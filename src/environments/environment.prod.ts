// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  //apiUrl: 'https://prolearn.com/api/lms'
  //apiUrl: 'http://127.0.0.1:8000/api/lms'
  //apiUrl: 'http://wgi-aws-php-1241812835.ap-south-1.elb.amazonaws.com/p2/API/public/api/lms'
  //apiUrl: 'https://multitudesolutions.in/api/lms'
  //apiUrl: 'http://139.59.89.219/api/lms'
  apiUrl: 'https://api.liveatclass.com/api/lms'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
