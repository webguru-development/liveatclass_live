$(document).on("click", "#nameSubmit", function () {

});

$.datetimepicker.setDateFormatter({
    parseDate: function (date, format) {
        var d = moment(date, format);
        return d.isValid() ? d.toDate() : false;
    },
    formatDate: function (date, format) {
        return moment(date).format(format);
    },
});

$("#datetimepicker").datetimepicker({
    changeYear: true,
    changeMonth: true,
    minDate: 0,
    timepicker: true,
    format: 'Y-M-D H:00:00',
});

$(document).on('click', "#meeting_submit", function () {

    var subject = $('#subject').val();
    var meeting_time = $('.meeting_time').val();
    var detail = $('#detail').val();
    if (isEmpty(subject) || isEmpty(meeting_time) || isEmpty(detail)) {
        alert('Please fill all details');
    } else {
        var videoConData = {
            'subject': subject,
            'meeting_time': meeting_time,
            'detail': detail,
        };
        var r = confirm("Are you sure?");
        if (r == true) {
            $.ajax({
                async: true,
                type: "POST",
                data: {
                    'data': videoConData
                },
                url: httppath + "/saveVideoConf",
                dataType: "json",
                beforeSend: function () { },
                success: function (res) {
                    alert(res.message);
                    window.location.reload();
                }
            });
        }
    }
})

$(document).on('click', ".cancelMeeting", function () {
    var idhost = $(this).data('id');
    var r = confirm("Are you sure?");
    if (r == true) {
        $.ajax({
            async: true,
            type: "POST",
            data: {
                'id': idhost
            },
            url: httppath + "/cancelMeet",
            dataType: "json",
            beforeSend: function () { },
            success: function (res) {
                alert(res.message);
                window.location.reload();
            }
        });
    }
})

$(document).on('click', ".copyText", function () {
    var parentDiv = $(this).parent();
    var copyText = parentDiv.find(".copyInput");
    copyToClipboard(copyText);

    copyText.addClass('success');
});

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

$(document).on('click', "#meetingJoin_submit", function () {

    var name = $('#name').val();
    var meeting_id = $('#meeting_id').val();
    if (isEmpty(name) || isEmpty(meeting_id)) {
        alert('Please fill all details');
    } else {
        var url_is = httppath+'/join/'+meeting_id+'/'+name+'?open=false&sessionid='+meeting_id+'&publicRoomIdentifier=dashboard&userFullName='+name;
        window.location.href = url_is;
    }
})