var hostVideo = function () {
    var mediaRecorder = '';
    var recordStreamObj = '';
    var recodedBolo = [];
    var buttonsCtr = [];
    var myStreamId='';
    (function () {
        var params = {},
            r = /([^&=]+)=?([^&]*)/g;

        function d(s) {
            return decodeURIComponent(s.replace(/\+/g, ' '));
        }
        var match, search = window.location.search;
        while (match = r.exec(search.substring(1)))
            params[d(match[1])] = d(match[2]);
        window.params = params;
        //$('#room-id').val(params.roomid)
    })();

    /*document.getElementById('open-room').onclick = function () {
        connection.open(document.getElementById('room-id').value, function () {
            console.log(connection.sessionid);
        });
    };*/

    var connection = new RTCMultiConnection();

    function hostOpenRoom(roomid, name = '') {
        connection.open(roomid);
        connection.extra.userFullName = name;
        connection.extra.roomOwner = true;
        buttonsCtr = ['mute-audio', 'mute-video', 'stop'];

    }

    function joinRoom(roomid, name = '') {
        connection.extra.userFullName = name;
        connection.extra.roomOwner = false;
        connection.join(roomid,function(status) {
            return status;
        });

    }





    connection.maxParticipantsAllowed = 7;

    // by default, socket.io server is assumed to be deployed on your own URL
    connection.socketURL = 'https://node.liveatclass.com:4000/';

    // comment-out below line if you do not have your own socket.io server
    // connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

    connection.socketMessageEvent = 'video-broadcast-demo';

    connection.session = {
        audio: true,
        video: true,
        // data: true,
        // screen: true,
        // data: true,
        // oneway: true
    };

    connection.sdpConstraints.mandatory = {
        OfferToReceiveAudio: false,
        OfferToReceiveVideo: false
    };


    // use your own TURN-server here!
    connection.iceServers = []
    connection.iceServers.push({
        'urls': [
            'stun:stun.l.google.com:19302',
            'stun:stun1.l.google.com:19302',
            'stun:stun2.l.google.com:19302',
            'stun:stun.l.google.com:19302?transport=udp',
        ]
    });

    connection.iceServers.push({
        'urls': ["turn:global.turn.twilio.com:3478?transport=udp", "turn:global.turn.twilio.com:3478?transport=tcp", "turn:global.turn.twilio.com:443?transport=tcp"],
        credential: 'tE2DajzSJwnsSbc123',
        username: 'dc2d2894d5a9023620c467b0e71cfa6a35457e6679785ed6ae9856fe5bdfa269'
    });



    connection.onUserStatusChanged = function (event) {
        // console.log(event);
        var infoBar = document.getElementById('onUserStatusChanged');
        var names = [];
        var NameStr = '';
        // connection.getAllParticipants().forEach(function (pid) {
        //     var name = getFullName(pid);
        //     var streamId = '';
        //     streamId = getStreamId(pid);
        //     NameStr += '<div class="userListItem">' + name + ' <span class="actionBtnUc"><i class="fa fa-microphone muteUc" action="unmute" data-id="' + streamId + '"></i> <i class="fa fa-video-camera cameraUc" action="video" data-id="' + streamId + '"></i> <i class="fa fa-trash removeUs" data-userid="' + pid + '" data-id="' + streamId + '"></i></span></div>';
        // });

        // if (!names.length) {
        //     names = ['Only You'];
        // } else {
        //     names = [connection.extra.userFullName || 'You'].concat(names);
        // }

        // infoBar.innerHTML = '<b>Active users: </b> ' + NameStr;
    };

    connection.onclose = connection.onerror = connection.onleave = function (event) {
        connection.onUserStatusChanged(event);
        // $("#"+event.userid).remove();
    };




    connection.onmessage = function (event) {

        if (event.data.checkmark === 'received') {
            var checkmarkElement = document.getElementById(event.data.checkmark_id);
            if (checkmarkElement) {
                checkmarkElement.style.display = 'inline';
            }
            return;
        }

    };

    connection.videosContainer = document.getElementById('videos-container');
    connection.onstream = function (event) {
        console.log(event);
        var existing = document.getElementById(event.streamid);
        if (existing && existing.parentNode) {
            existing.parentNode.removeChild(existing);
        }
        if (event.stream.isScreen && !event.stream.canvasStream) {
            console.log(event.stream);
            $('#screen-viewer').get(0).srcObject = event.stream;
            $('#screen-viewer').hide();
        } else {
            if (event.extra.roomOwner) {
                connection.videosContainer = document.getElementById('videos-host');
            } else {
                connection.videosContainer = document.getElementById('videos-container');
            }

            var existing = document.getElementById(event.streamid);
            if (existing && existing.parentNode) {
                existing.parentNode.removeChild(existing);
            }

            event.mediaElement.removeAttribute('src');
            event.mediaElement.removeAttribute('srcObject');
            event.mediaElement.muted = true;
            event.mediaElement.volume = 0;

            var video = document.createElement('video');

            try {
                video.setAttributeNode(document.createAttribute('autoplay'));
                video.setAttributeNode(document.createAttribute('playsinline'));
            } catch (e) {
                video.setAttribute('autoplay', true);
                video.setAttribute('playsinline', true);
            }

            if (event.type === 'local') {
                myStreamId = event.streamid;
                video.volume = 0;
                try {
                    video.setAttributeNode(document.createAttribute('muted'));
                } catch (e) {
                    video.setAttribute('muted', true);
                }
            }
            video.srcObject = event.stream;

            // var width ='';
            // if(event.extra.roomOwner){
            //     width = parseInt(connection.videosContainer.clientWidth / 2) - 20;
            // }else{
            //     width = parseInt(connection.videosContainer.clientWidth / 4) - 20;
            // }

            var mediaElement = getHTMLMediaElement(video, {
                title: event.extra.userFullName,
                buttons: [], //(event.type == 'local') ? [] : buttonsCtr,
                // width: width,
                showOnMouseEnter: false
            });


            connection.videosContainer.appendChild(mediaElement);

            setTimeout(function () {
                mediaElement.media.play();
            }, 5000);

            mediaElement.id = event.streamid;
        }


        connection.onUserStatusChanged(event);
    };

    connection.onstreamended = function (event) {
        var mediaElement = document.getElementById(event.streamid);
        if (mediaElement) {
            mediaElement.parentNode.removeChild(mediaElement);

            if (event.userid === connection.sessionid && !connection.isInitiator) {
                alert('Broadcast is ended. We will reload this page to clear the cache.');
                location.reload();
            }
        }
    };


    // code by haidar start
    // if local or remote stream is muted
    connection.onmute = function (e) {
        if (e.session.audio && !e.session.video) {
            e.mediaElement.muted = true;
            return;
        }

        // e.mediaElement.src = null;
        // e.mediaElement.pause();
        e.mediaElement.setAttribute('poster', '/img/photo.jpg');
    };

    // if local or remote stream is unmuted
    connection.onunmute = function (e) {
        if (e.session.audio && !e.session.video) {
            e.mediaElement.muted = false;
            return;
        }

        e.mediaElement.removeAttribute('poster');
        e.mediaElement.srcObject = e.stream;
        e.mediaElement.play();
    };

    // end by haidar



    connection.onMediaError = function (e) {
        if (e.message === 'Concurrent mic process limit.') {
            if (DetectRTC.audioInputDevices.length <= 1) {
                alert('Please select external microphone. Check github issue number 483.');
                return;
            }

            var secondaryMic = DetectRTC.audioInputDevices[1].deviceId;
            connection.mediaConstraints.audio = {
                deviceId: secondaryMic
            };

            connection.join(connection.sessionid);
        }
    };

    var roomid = params.roomid;

    if (roomid && roomid.length) {
        document.getElementById('room-id').value = roomid;

        // auto-join-room
        (function reCheckRoomPresence() {
            connection.checkPresence(roomid, function (isRoomExist) {
                if (isRoomExist) {
                    connection.join(roomid);
                    return;
                }
                setTimeout(reCheckRoomPresence, 5000);
            });
        })();
    }


    function getFullName(userid) {
        var _userFullName = userid;
        if (connection.peers[userid] && connection.peers[userid].extra.userFullName) {

            _userFullName = connection.peers[userid].extra.userFullName;
        }
        return _userFullName;
    }

    function getStreamId(userid) {
        var _userStreamId = userid;
        if (connection.peers[userid] && connection.peers[userid].streams.length > 0) {
            _userStreamId = connection.peers[userid].streams[0].streamid;
        }
        return _userStreamId;
    }

    function addStreamStopListener(stream, callback) {
        stream.addEventListener('ended', function() {
            callback();
            callback = function() {};
        }, false);

        stream.addEventListener('inactive', function() {
            callback();
            callback = function() {};
        }, false);

        stream.getTracks().forEach(function(track) {
            track.addEventListener('ended', function() {
                callback();
                callback = function() {};
            }, false);

            track.addEventListener('inactive', function() {
                callback();
                callback = function() {};
            }, false);
        });
    }

    function replaceTrack(videoTrack, screenTrackId) {
        if (!videoTrack) return;
        if (videoTrack.readyState === 'ended') {
            alert('Can not replace an "ended" track. track.readyState: ' + videoTrack.readyState);
            return;
        }
        connection.getAllParticipants().forEach(function (pid) {
            var peer = connection.peers[pid].peer;
            console.log('peer');
            console.log(peer);
            if (!peer.getSenders) return;
            var trackToReplace = videoTrack;
            peer.getSenders().forEach(function (sender) {
                if (!sender || !sender.track) return;
                if (screenTrackId) {
                    if (trackToReplace && sender.track.id === screenTrackId) {
                        sender.replaceTrack(trackToReplace);
                        trackToReplace = null;
                    }
                    return;
                }

                if (sender.track.id !== tempStream.getTracks()[0].id) return;
                if (sender.track.kind === 'video' && trackToReplace) {
                    sender.replaceTrack(trackToReplace);
                    trackToReplace = null;
                }
            });
        });
    }


    function replaceScreenTrack(stream) {
        stream.isScreen = true;
        stream.streamid = stream.id;
        stream.type = 'local';

        console.log(stream);

        connection.onstream({
            stream: stream,
            type: 'local',
            streamid: stream.id,
            // mediaElement: video
        });

        var screenTrackId = stream.getTracks()[0].id;
        addStreamStopListener(stream, function () {
            connection.send({
                hideMainVideo: true
            });

            // $('#main-video').hide();
            $('#screen-viewer').hide();
            // $('#btn-share-screen').show();
            replaceTrack(tempStream.getTracks()[0], screenTrackId);
        });

        stream.getTracks().forEach(function (track) {
            if (track.kind === 'video' && track.readyState === 'live') {
                replaceTrack(track);
            }
        });

        connection.send({
            showMainVideo: true
        });

        // $('#main-video').show();
        // $('#screen-viewer').css({
        //     top: $('#widget-container').offset().top,
        //     left: $('#widget-container').offset().left,
        //     width: $('#widget-container').width(),
        //     height: $('#widget-container').height()
        // });
        $('#screen-viewer').show();
    }

    // Audio Mute/unmute
    function selfAudioMute(connectionType) {
        console.log(connection.attachStreams);
        if (connectionType == 'host') {
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[1].id, 'mute', 'audio');
        } else {
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[0].id, 'mute', 'audio');
        }

    }

    function selfAudioUnmute(connectionType) {

        console.log(connection.attachStreams);

        if (connectionType == 'host') {
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[1].id, 'unmute', 'audio');
        } else {
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[0].id, 'unmute', 'audio');
        }

    }

    // video Mute/unmute
    function selfVideoMute(connectionType) {



        console.log('selfVideoMute');
        console.log(connectionType);
        console.log(connection.attachStreams);
        if (connectionType == 'host') {
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[1].id, 'mute', 'video');
        } else {
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[1].id, 'mute', 'video');
        }

    }

    function selfVideoUnmute(connectionType) {
        console.log('selfVideoUnmute');
        console.log(connectionType);
        console.log(connection.attachStreams);

        if (connectionType == 'host') {
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[1].id, 'unmute', 'video');
        } else {
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[1].id, 'unmute', 'video');
        }

    }

    function userAudioMute(stremId) {
        connection.StreamsHandler.onSyncNeeded(stremId, 'mute', 'audio');
    }
    function userAudioUnmute(stremId) {
        connection.StreamsHandler.onSyncNeeded(stremId, 'unmute', 'audio');
    }

    function userVideoMute(stremId) {
        connection.StreamsHandler.onSyncNeeded(stremId, 'mute', 'video');
    }
    function userVideoUnmute(stremId) {
        connection.StreamsHandler.onSyncNeeded(stremId, 'unmute', 'video');
    }

    function userRemove(stremId) {
        connection.removeStream(stremId)
        connection.StreamsHandler.onSyncNeeded(stremId, 'mute', 'both');
        connection.StreamsHandler.onSyncNeeded(stremId, 'ended');
    }

    function getMyVideoSteamId() {
        return myStreamId;
    }


    function shareMyScreen() {

        var tempStreamCanvas = document.getElementById('temp-stream-canvas');
        var tempStream = tempStreamCanvas.captureStream();
        tempStream.isScreen = true;
        tempStream.streamid = tempStream.id;
        tempStream.type = 'local';
        connection.attachStreams.push(tempStream);
        window.tempStream = tempStream;


        if (!window.tempStream) {
            alert('Screen sharing is not enabled.');
            return;
        }


        if (navigator.mediaDevices.getDisplayMedia) {
            navigator.mediaDevices.getDisplayMedia().then(stream => {
                replaceScreenTrack(stream);
            }, error => {
                alert('Please make sure to use Edge 17 or higher.');
            });
        }
        else if (navigator.getDisplayMedia) {
            navigator.getDisplayMedia(screen_constraints).then(stream => {
                replaceScreenTrack(stream);
            }, error => {
                alert('Please make sure to use Edge 17 or higher.');
            });
        }
        else {
            alert('getDisplayMedia API is not available in this browser.');
        }
    }


    // detect 2G
    if (navigator.connection &&
        navigator.connection.type === 'cellular' &&
        navigator.connection.downlinkMax <= 0.115) {
        alert('2G is not supported. Please use a better internet service.');
    }

    function endMeetingClick() {
        // alert("callll 2222");
        console.log("endMeetingClick");
        // connection.close();
        // connection.disconnect();
        // connection.attachStreams[0].stop();
    }

    function leaveMeetingClick() {
        console.log("leaveMeetingClick");
        connection.close();
        connection.disconnect();
        // connection.attachStreams[0].stop();
    }

    return {
        leaveMeetingClick: leaveMeetingClick,
        endMeetingClick: endMeetingClick,
        hostOpenRoom: hostOpenRoom,
        joinRoom: joinRoom,
        selfAudioMute: selfAudioMute,
        selfAudioUnmute: selfAudioUnmute,
        selfVideoMute: selfVideoMute,
        selfVideoUnmute: selfVideoUnmute,
        shareMyScreen: shareMyScreen,
        userAudioMute: userAudioMute,
        userAudioUnmute: userAudioUnmute,
        userVideoMute: userVideoMute,
        userVideoUnmute: userVideoUnmute,
        userRemove: userRemove,
        getMyVideoSteamId: getMyVideoSteamId,
    };

}
// export default hostVideo