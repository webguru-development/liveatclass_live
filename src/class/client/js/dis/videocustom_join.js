// ......................................................
// .......................UI Code........................
// ......................................................
var mediaRecorder = '';
var recordStreamObj = '';
var recodedBolo = [];

(function () {
    var params = {},
        r = /([^&=]+)=?([^&]*)/g;

    function d(s) {
        return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1)))
        params[d(match[1])] = d(match[2]);
    window.params = params;
    $('#room-id').val(params.roomid)
})();



document.getElementById('join-room').onclick = function () {

    connection.sdpConstraints.mandatory = {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: true
    };
    connection.join(document.getElementById('room-id').value);
};


// ......................................................
// ..................RTCMultiConnection Code.............
// ......................................................

var connection = new RTCMultiConnection();


connection.maxParticipantsAllowed = 7;

// STAR_FIX_VIDEO_AUTO_PAUSE_ISSUES
var bitrates = 512;
var resolutions = 'Ultra-HD';
var videoConstraints = {};

if (resolutions == 'HD') {
    videoConstraints = {
        width: {
            ideal: 1280
        },
        height: {
            ideal: 720
        },
        frameRate: 30
    };
}

if (resolutions == 'Ultra-HD') {
    videoConstraints = {
        width: {
            ideal: 1920
        },
        height: {
            ideal: 1080
        },
        frameRate: 30
    };
}

connection.mediaConstraints = {
    video: videoConstraints,
    audio: true
};

var CodecsHandler = connection.CodecsHandler;

connection.processSdp = function (sdp) {
    var codecs = 'vp8';

    if (codecs.length) {
        sdp = CodecsHandler.preferCodec(sdp, codecs.toLowerCase());
    }

    if (resolutions == 'HD') {
        sdp = CodecsHandler.setApplicationSpecificBandwidth(sdp, {
            audio: 128,
            video: bitrates,
            screen: bitrates
        });

        sdp = CodecsHandler.setVideoBitrates(sdp, {
            min: bitrates * 8 * 1024,
            max: bitrates * 8 * 1024,
        });
    }

    if (resolutions == 'Ultra-HD') {
        sdp = CodecsHandler.setApplicationSpecificBandwidth(sdp, {
            audio: 128,
            video: bitrates,
            screen: bitrates
        });

        sdp = CodecsHandler.setVideoBitrates(sdp, {
            min: bitrates * 8 * 1024,
            max: bitrates * 8 * 1024,
        });
    }

    return sdp;
};
// END_FIX_VIDEO_AUTO_PAUSE_ISSUES





// by default, socket.io server is assumed to be deployed on your own URL
connection.socketURL = 'https://livemeetups.herokuapp.com/';

// comment-out below line if you do not have your own socket.io server
// connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

connection.socketMessageEvent = 'video-broadcast-demo';

connection.session = {
    audio: true,
    video: true,
};

connection.sdpConstraints.mandatory = {
    OfferToReceiveAudio: false,
    OfferToReceiveVideo: false
};


// use your own TURN-server here!
connection.iceServers = []
connection.iceServers.push({
    'urls': [
        'stun:stun.l.google.com:19302',
        'stun:stun1.l.google.com:19302',
        'stun:stun2.l.google.com:19302',
        'stun:stun.l.google.com:19302?transport=udp',
    ]
});

connection.iceServers.push({
    'urls': ["turn:global.turn.twilio.com:3478?transport=udp","turn:global.turn.twilio.com:3478?transport=tcp","turn:global.turn.twilio.com:443?transport=tcp"],
    credential: 'tE2DajzSJwnsSbc123',
    username: 'dc2d2894d5a9023620c467b0e71cfa6a35457e6679785ed6ae9856fe5bdfa269'
});


connection.onUserStatusChanged = function (event) {
    var infoBar = document.getElementById('onUserStatusChanged');
    var names = [];
    connection.getAllParticipants().forEach(function (pid) {
        names.push(getFullName(pid));
    });

    if (!names.length) {
        names = ['Only You'];
    } else {
        names = [connection.extra.userFullName || 'You'].concat(names);
    }

    infoBar.innerHTML = '<b>Active users: </b> ' + names.join(', ');
};


connection.onclose = connection.onerror = connection.onleave = function (event) {
    connection.onUserStatusChanged(event);
    // $("#"+event.userid).remove();
};

connection.onmessage = function (event) {


    if (event.data.checkmark === 'received') {
        var checkmarkElement = document.getElementById(event.data.checkmark_id);
        if (checkmarkElement) {
            checkmarkElement.style.display = 'inline';
        }
        return;
    }

};



connection.videosContainer = document.getElementById('videos-container');
var videoIds = [];
connection.onstream = function (event) {
    // console.log(event);
    if(event.extra.roomOwner){
        connection.videosContainer = document.getElementById('videos-host');
    }else{
        connection.videosContainer = document.getElementById('videos-container');
    }

    var existing = document.getElementById(event.streamid);
    if (existing && existing.parentNode) {
        existing.parentNode.removeChild(existing);
    }

    event.mediaElement.removeAttribute('src');
    event.mediaElement.removeAttribute('srcObject');
    event.mediaElement.muted = true;
    event.mediaElement.volume = 0;

    var video = document.createElement('video');

    try {
        video.setAttributeNode(document.createAttribute('autoplay'));
        video.setAttributeNode(document.createAttribute('playsinline'));
    } catch (e) {
        video.setAttribute('autoplay', true);
        video.setAttribute('playsinline', true);
    }

    if (event.type === 'local') {
        video.volume = 0;
        try {
            video.setAttributeNode(document.createAttribute('muted'));
        } catch (e) {
            video.setAttribute('muted', true);
        }
    }

    video.srcObject = event.stream;

    // var width ='';
    // if(event.extra.roomOwner){
    //     width = parseInt(connection.videosContainer.clientWidth / 2) - 20;
    // }else{
    //     width = parseInt(connection.videosContainer.clientWidth / 4) - 20;
    // }
    var mediaElement = getHTMLMediaElement(video, {
        title: event.userid,
        buttons: [],
        // width: width,
        showOnMouseEnter: false
    });

    connection.videosContainer.appendChild(mediaElement);

    setTimeout(function () {
        mediaElement.media.play();
    }, 5000);

    mediaElement.id = event.streamid;

    // setTimeout(() => {
    //     $('.media-container').each(function (index, element) {
    //         if (index == 0) {
    //             $(this).addClass('VideoOne');
    //         } else {
    //             $(this).addClass('VideoTwo');
    //         }
    //     });
    // }, 5000);

    connection.onUserStatusChanged(event);
};

connection.onstreamended = function (event) {
    var mediaElement = document.getElementById(event.streamid);
    if (mediaElement) {
        mediaElement.parentNode.removeChild(mediaElement);

        if (event.userid === connection.sessionid && !connection.isInitiator) {
            alert('Broadcast is ended. We will reload this page to clear the cache.');
            location.reload();
        }
    }
};

connection.onMediaError = function (e) {
    console.log(e);
    if (e.message === 'Concurrent mic process limit.') {
        if (DetectRTC.audioInputDevices.length <= 1) {
            alert('Please select external microphone. Check github issue number 483.');
            return;
        }

        var secondaryMic = DetectRTC.audioInputDevices[1].deviceId;
        connection.mediaConstraints.audio = {
            deviceId: secondaryMic
        };

        connection.join(connection.sessionid);
    }
};


// code by haidar start
// if local or remote stream is muted
connection.onmute = function (e) {
    if (e.session.audio && !e.session.video) {
        e.mediaElement.muted = true;
        return;
    }

    // e.mediaElement.src = null;
    // e.mediaElement.pause();
    e.mediaElement.setAttribute('poster', '/img/photo.jpg');
};

// if local or remote stream is unmuted
connection.onunmute = function (e) {
    if (e.session.audio && !e.session.video) {
        e.mediaElement.muted = false;
        return;
    }

    e.mediaElement.removeAttribute('poster');
    e.mediaElement.srcObject = e.stream;
    e.mediaElement.play();
};

// end by haidar



var roomid = params.roomid;

if (roomid && roomid.length) {
    document.getElementById('room-id').value = roomid;

    // auto-join-room
    (function reCheckRoomPresence() {
        connection.checkPresence(roomid, function (isRoomExist) {
            if (isRoomExist) {
                connection.join(roomid);
                return;
            }
            setTimeout(reCheckRoomPresence, 5000);
        });
    })();

}


function getFullName(userid) {
    var _userFullName = userid;
    if (connection.peers[userid] && connection.peers[userid].extra.userFullName) {
        _userFullName = connection.peers[userid].extra.userFullName;
    }
    return _userFullName;
}


// Click Actions

var audioActive = true;
var cameraActive = true
$(document).on("click", ".witebord1", function () {
    // to manually remove stream from all remote users sides
    // connection.StreamsHandler.onSyncNeeded(streamid, 'ended');

    // last argument is "both" or "audio" or "video" or NULL
    if (params.open === true || params.open === 'true') {
        if (cameraActive) {
            $(this).html('<i class="fa fa-video-camera" aria-hidden="true"></i><span class="disableVIco">/</span>')
            cameraActive = false;
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[1].id, 'mute', 'video');
        } else {
            $(this).html('<i class="fa fa-video-camera" aria-hidden="true"></i>')
            cameraActive = true;
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[1].id, 'unmute', 'video');
        }
    } else {

        if (cameraActive) {
            $(this).html('<i class="fa fa-video-camera" aria-hidden="true"></i><span class="disableVIco">/</span>')
            cameraActive = false;
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[0].id, 'mute', 'video');
        } else {
            $(this).html('<i class="fa fa-video-camera" aria-hidden="true"></i>')
            cameraActive = true;
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[0].id, 'unmute', 'video');
        }
    }
});

$(document).on("click", ".witebord2", function () {
    // last argument is "both" or "audio" or "video" or NULL
    if (params.open === true || params.open === 'true') {
        if (audioActive) {
            $(this).html('<i class="fa fa-microphone-slash" aria-hidden="true"></i>')
            audioActive = false;
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[1].id, 'mute', 'audio');
        } else {
            $(this).html('<i class="fa fa-microphone" aria-hidden="true"></i>')
            audioActive = true;
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[1].id, 'unmute', 'audio');
        }

    } else {
        if (audioActive) {
            $(this).html('<i class="fa fa-microphone-slash" aria-hidden="true"></i>')
            audioActive = false;
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[0].id, 'mute', 'audio');
        } else {
            $(this).html('<i class="fa fa-microphone" aria-hidden="true"></i>')
            audioActive = true;
            connection.StreamsHandler.onSyncNeeded(connection.attachStreams[0].id, 'unmute', 'audio');
        }
    }
});




// detect 2G
if (navigator.connection &&
    navigator.connection.type === 'cellular' &&
    navigator.connection.downlinkMax <= 0.115) {
    alert('2G is not supported. Please use a better internet service.');
}
