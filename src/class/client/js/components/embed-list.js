module.exports={
    props: ['embedList','folderList','selectedEmbed','embedOpts'],
    data: function (){
      return {
        sortBy: 'name',
        sortOrder: true
      }
    },
    template: '#embed-list',
    methods: {
      loadVideo: function (embed) {
        this.$emit('load-embed', embed)
      },
      sortTable: function (field){
        this.sortOrder=(this.sortOrder==field ? !this.sortOrder : true);
        this.sortBy=field;
        this.embedOpts.order=field;
        this.embedOpts.dir=(this.sortOrder ? 'asc' : 'desc');
        this.changeOpts();
      },
      selectEmbed: function (embed) {
        this.$emit('select-embed', embed)
      },
      changeOpts: function(){
        this.$emit('embed-opts-change', this.embedOpts, 'embed');
      },
      changeFolder: function (folder) {
        this.$emit('load-folder', folder, 'embed')
      }
    }
  }