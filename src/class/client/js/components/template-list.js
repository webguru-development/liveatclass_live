module.exports={
    props: ['templateList','folderList','selectedTemplate','templateOpts'],
    data: function (){
      return {
        sortBy: 'name',
        sortOrder: true
      }
    },
    template: '#template-list',
    methods: {
      loadTemplate: function (template) {
        this.$emit('load-template', template)
      },
      sortTable: function (field){
        this.sortOrder=(this.sortOrder==field ? !this.sortOrder : true);
        this.sortBy=field;
        this.templateOpts.order=field;
        this.templateOpts.dir=(this.sortOrder ? 'asc' : 'desc');
        this.changeOpts();
      },
      selectTemplate: function (template) {
        this.$emit('select-template', template)
      },
      changeOpts: function(){
        this.$emit('template-opts-change', this.templateOpts, 'import_from_library');
      },
      changeFolder: function (folder) {
        this.$emit('load-folder', folder, 'template')
      }
    }
  }