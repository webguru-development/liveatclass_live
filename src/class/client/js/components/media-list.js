module.exports={
    props: ['mediaList','folderList','selectedMedia','mediaOpts'],
    data: function (){
      return {
        sortBy: 'name',
        sortOrder: true
      }
    },
    template: '#media-list',
    methods: {
      loadVideo: function (media) {
        this.$emit('load-media', media)
      },
      sortTable: function (field){
        this.sortOrder=(this.sortOrder==field ? !this.sortOrder : true);
        this.sortBy=field;
        this.mediaOpts.order=field;
        this.mediaOpts.dir=(this.sortOrder ? 'asc' : 'desc');
        this.changeOpts();
      },
      selectMedia: function (media) {
        this.$emit('select-media', media)
      },
      changeOpts: function(){
        this.$emit('media-opts-change', this.mediaOpts, 'media');
      },
      changeFolder: function (folder) {
        this.$emit('load-folder', folder, 'media')
      }
    }
  }