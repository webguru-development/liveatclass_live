"use strict"
module.exports={
  HOST        :   process.env.VD_HOST || 'localhost',
  PORT        :   process.env.VD_PORT || 3000,
  DEBUG       :   process.env.VD_DEBUG || true,
  REDIS_HOST  :   process.env.VD_REDIS_HOST || '127.0.0.1',
  REDIS_PORT  :   process.env.VD_REDIS_PORT || 6379,
  REDIS_PWD   :   process.env.VD_REDIS_PWD || '',
  MYSQL_HOST  :   process.env.VD_MYSQL_HOST || 'localhost',
  MYSQL_PORT  :   process.env.VD_MYSQL_PORT || 3306,
  MYSQL_USER  :   process.env.VD_MYSQL_USER || 'root',
  MYSQL_PWD   :   process.env.VD_MYSQL_PWD || '',
  MYSQL_DB    :   process.env.VD_MYSQL_DB || 'test',
  FILE_STORAGE:   process.env.VD_FILE_STORAGE || '/var/www/html/prolearn/APPS/src/class/client/uploads/',
  FILE_URL    :   process.env.VD_FILE_URL || 'http://localhost/class/client/uploads/',
  //FILE_URL    :   process.env.VD_FILE_URL || 'https://multitudesolutions.in/class/client/uploads/',
  API_URL     :   process.env.VD_API_URL || 'https://multitudesolutions.in/api/lms',
  SSL_KEY     :   process.env.SSL_KEY || '/var/www/html/liveatclass.dev/public_html/APPS/src/class/server/certificate/localhost.key',
  SSL_CERT    :   process.env.SSL_CERT || '/var/www/html/liveatclass.dev/public_html/APPS/src/class/server/certificate/localhost.crt',
}


/*
"use strict"
module.exports={
  HOST        :   process.env.VD_HOST || 'localhost',
  PORT        :   process.env.VD_PORT || 3000,
  DEBUG       :   process.env.VD_DEBUG || true,
  REDIS_HOST  :   process.env.VD_REDIS_HOST || '127.0.0.1',
  REDIS_PORT  :   process.env.VD_REDIS_PORT || 6379,
  REDIS_PWD   :   process.env.VD_REDIS_PWD || '',
  MYSQL_HOST  :   process.env.VD_MYSQL_HOST || 'localhost',
  MYSQL_PORT  :   process.env.VD_MYSQL_PORT || 3306,
  MYSQL_USER  :   process.env.VD_MYSQL_USER || 'root',
  MYSQL_PWD   :   process.env.VD_MYSQL_PWD || '',
  MYSQL_DB    :   process.env.VD_MYSQL_DB || 'test',
  FILE_STORAGE:   process.env.VD_FILE_STORAGE || '/var/www/html/prolearn/APPS/src/class/client/uploads/',
  FILE_URL    :   process.env.VD_FILE_URL || 'http://localhost/class/client/uploads/',
  API_URL     :   process.env.VD_API_URL || 'http://localhost/api/lms',
  SSL_KEY     :   process.env.SSL_KEY || 'E:/wgi-office-work/LMS-hiring/node/virtual_discussion/server/certificate/localhost.key',
  SSL_CERT    :   process.env.SSL_CERT || 'E:/wgi-office-work/LMS-hiring/node/virtual_discussion/server/certificate/localhost.crt',
}
*/