'use strict'
const CONFIG=require('./config')
const MOMENT=require('moment')
const REQUEST = require('request-promise')

var Discussion = function () {
  function getInfo (discussionId, userToken, redis) {
    if( CONFIG.DEBUG ) { console.info( 'Checking discussion by id: ' + discussionId ) ; }
    if(discussionId==-1){
      return {title: 'Generate template',startTime: new Date(), duration: 300, live: false};
    }
    var classOpts= {
      uri: CONFIG.API_URL+"/live_classes/live/"+discussionId,
      qs: {
          token: userToken
      },
      json: true
    },
    attendOpts= {
      uri: CONFIG.API_URL+"/live_classes/attend/"+discussionId,
      qs: {
          token: userToken
      },
      json: true
    }
    // mark attended or live based on who is signed in
    REQUEST(attendOpts).then(()=>{}).catch(console.log);
    // get class data
    return REQUEST(classOpts).then(function(resp){
      if(!resp.status){
        throw new Error('Something is wrong');
      }
      var discussion=resp.data;
      discussion.startTime=MOMENT.utc(resp.data.start_time).toDate();
      discussion.duration=MOMENT(resp.data.end_time).diff(MOMENT(resp.data.start_time),'minutes');
      discussion.live=true;
      return discussion;
    })
  }
  function initDiscussion (redis, socket, mapSocketToDiscussion, user) {
    socket.on('editorchange', function (delta, tabId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('editorchange ' + discussionId + ' ' + delta) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (workplace) {
          socket.to(discussionId).emit('editorchange', delta, tabId)
          workplace = JSON.parse(workplace)
          if (!workplace['settings'][tabId]) workplace['settings'][tabId] = { 'ops': [] }
          workplace['settings'][tabId]['ops'].push(['editorchange', delta, Date.now()])
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
    })
    socket.on('editorchange_language', function (lang, tabId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('editorchange_language ' + discussionId) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (workplace) {
          socket.to(discussionId).emit('editorchange_language', lang, tabId)
          workplace = JSON.parse(workplace)
          if (!workplace['tabs']) workplace['tabs'] = []
          for (let i = 0; i < workplace['tabs'].length; i++) {
            if (workplace['tabs'][i]['id'] === tabId) {
              workplace['tabs'][i]['language'] = lang
              break
            }
          }
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
    })

    socket.on('editorchange_selection', function (selPos, tabId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('editorchange_selection ' + discussionId + '  : ' + selPos) }
      socket.to(discussionId).emit('editorchange_selection', selPos, tabId)
    })

    socket.on('editorchange_cursor', function (curPos, tabId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('editorchange_cursor ' + discussionId + '  : ' + curPos) }
      socket.to(discussionId).emit('editorchange_cursor', curPos, tabId)
    })

    socket.on('editorclear_buffer', function (tabId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('editorclear_buffer ' + discussionId) }
      socket.to(discussionId).emit('editorclear_buffer', tabId)
    })
  }
  return {
    init: initDiscussion,
    getInfo: getInfo
  }
}
module.exports = Discussion
