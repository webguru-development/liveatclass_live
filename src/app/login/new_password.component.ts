import { Component, OnInit, DoCheck, Injectable } from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router, CanActivate,ActivatedRouteSnapshot,RouterStateSnapshot} from '@angular/router';
import { AppComponent } from '../app.component';



@Injectable()
@Component({
  templateUrl: './new_password.component.html'
})

export class NewpasswordComponent implements OnInit,DoCheck,CanActivate {
  message = '';
  newmessage='';
   data:any;
   model: any = {};
   email='';
   token= '';

    constructor(private UserService: UserService, private router: Router,private appComponent: AppComponent) {}
 
    ngOnInit() {
       		this.data=JSON.parse(localStorage.getItem("language"));
       		this.router.routerState.root.queryParams.forEach((params: Params) => {
			this.message=params['message'];
			if(params['email']!== undefined){
			this.email=params['email'];
			this.token=params['token'];
			}
			});
    }
	canActivate(route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot) {
	  if(route.queryParams['email']!==undefined){
	   return true;
	  }else{
		this.router.navigate(['']);
		return false;
	}
  }	

    ngDoCheck(){
    if(this.data!=JSON.parse(localStorage.getItem("language"))){
		this.data=JSON.parse(localStorage.getItem("language")); 
		}
		if(this.newmessage!=""){
			this.message=this.newmessage;
		}
	}
	
	setPassword(){
	if(this.model.password==this.model.confirmpassword)
	{
	this.appComponent.isLoading = true;
	      this.UserService.setPassword(this.model.password, this.email, this.token)
            .subscribe(
                data => {
                   if(data['code'] == 200){
				   this.message =data['message'];
                   this.appComponent.isLoading = false;
                   this.router.navigate(['']);
					}else{
						this.message =data['message'];
						this.appComponent.isLoading = false;
					}
                },
                error => {
                this.message ="Something went wrong! Pleae try again";
				this.appComponent.isLoading = false;
                });
       }else{
		this.message="Password not match"
       }
	}

 
 }

    

			

																																																																																																																								

