import { Component, OnInit, DoCheck } from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router, ActivatedRoute} from '@angular/router';
import { AppComponent } from '../app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Title, Meta }     from '@angular/platform-browser';

import * as $ from "jquery";
declare var jQuery:any;

@Component({
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit, DoCheck {
	  message = '';
	  newmessage='';
	  model: any = {};
	  data:any;
	  animal: string;
	  name: string;
	  check=true;
	  currentUser: {};
	  loginError=false;
	  forgotError=false;
	  loginSetting:any={institute: {title: 'Live @ Class'}};
	  public file_srcs: string= '';
    constructor(private title: Title,private meta: Meta, private UserService: UserService, private router: Router,private appComponent: AppComponent) { }

	ngOnInit(){
		this.getLoginSettings();
		this.data=JSON.parse(localStorage.getItem("language"));
			this.router.routerState.root.queryParams.forEach((params: Params) => {
				if(params['message'] != 'undefined')this.message=params['message'];
				if(params['token']!== undefined){
				//~ this.confirmation(params['token']);
				}
			});	
		if(JSON.parse(localStorage.getItem("user_data")) != null){
		  this.currentUser= JSON.parse(localStorage.getItem("user_data"));
		}
		if(this.currentUser != null || this.currentUser != undefined){
			if(document.referrer.indexOf('/class/') != -1 && document.referrer.indexOf('&token=') == -1){
				window.location.href=document.referrer+'&token='+localStorage.getItem("token");
				return;
			}
			switch(this.currentUser['user_type']){
				case 'superadmin':
				case 'admin':
					this.router.navigate(['/dashboard']);		
				break;
				case 'teacher':
					this.router.navigate(['/teacher_dashboard']);
				break;
				case 'student':
					this.router.navigate(['/student_dashboard']);
				break;
			}
		}
	}
     ngDoCheck(){
		//~ if(this.newmessage!=""){
			//~ this.message=this.newmessage;
		//~ }
	}


    login(form) {
      var that=this;
      that.loginError=false;
      if(!this.data) this.data=JSON.parse(localStorage.getItem("language"));
		
     // this.appComponent.isLoading = true;
        this.UserService.login(this.model.email, this.model.password)
            .subscribe(
                loginData => {
					//this.appComponent.isLoading = false;
                   if(loginData['code'] == 200){
						localStorage.setItem("token",loginData['token']);
						this.getUserDetail()
						.subscribe(
							data => {            
								that.loginError=false;
      					localStorage.setItem("user_data",JSON.stringify(data['user']));
								localStorage.setItem("permissions",JSON.stringify(data['permission']));
								that.currentUser= JSON.parse(localStorage.getItem("user_data"));
								if(document.referrer.indexOf('/class/') != -1){
									window.location.href=document.referrer+'&token='+loginData['token'];
									return;
								}
								if(that.currentUser['user_type_id'] == 4){
									if(Object.keys(this.data).length) this.message =this.data[loginData['message']];
									window.location.href = '/student_dashboard';
								}else if(that.currentUser['user_type_id'] == 3){
									if(Object.keys(this.data).length) this.message =this.data[loginData['message']];
									window.location.href = '/teacher_dashboard';
								}else{
									if(Object.keys(this.data).length) this.message =this.data[loginData['message']];
									window.location.href = '/dashboard';
								}
								setTimeout(_=>{
									form.submitted=false;
								},3000);
							},
							error => {
								
							});
					}
                },
			error => {
				console.log(error);
					that.loginError=true;
      					
					this.message ='Invalid email and password';
					setTimeout(()=>{    //<<<---    using ()=> syntax
						  this.message = "";
						  form.submitted=false;
					 }, 3000)
					//~ //this.appComponent.isLoading = false;
                    //~ 
                });
    }
  
	//~ confirmation(token: string){
	//~ if(!this.data) this.data=JSON.parse(localStorage.getItem("language"));
		//~ 
	//~ this.UserService.confirm(token)
		//~ .subscribe(
			//~ data => {
			  //~ if(data['code'] == 200){
					//~ if(Object.keys(this.data).length) this.message =this.data[data['message']];
				//~ }else{
					//~ if(Object.keys(this.data).length) this.message =this.data[data['message']];
			  //~ }
			//~ },
			//~ error => {
				//~ 
			//~ });
//~ 
	//~ }
	getUserDetail(){
		return this.UserService.getUserDetail();
	}
	
	getLoginSettings(){
		if(!this.data) this.data=JSON.parse(localStorage.getItem("language"));
		
		this.UserService.getLoginSettings()
		 .subscribe(
				data => {
					this.loginSetting = data['getLoginSetting'];
					this.title.setTitle( 'Home | '+this.loginSetting.institute.title);
					if(data['getLoginSetting']) this.file_srcs = data['getLoginSetting']["banner_image"];
				},
				error => {
				}
			);
		
	}
	
	//~ hide(){
		//~ 
	//~ }
	
	resetPassword(form){ 
		if(!this.data) this.data=JSON.parse(localStorage.getItem("language"));
		this.forgotError=false;
	//this.appComponent.isLoading = true;
	      this.UserService.forgotPassword(this.model.forgotEmail)
            .subscribe(
                data => {
                   if(data['code'] == 200){
            this.forgotError=false;
            this.model={};
            form.submitted=false;
            this.appComponent.isLoading = false;
						this.newmessage =data['message'];
					}else{
						this.forgotError=true;
						this.appComponent.isLoading = false;
						this.newmessage =data['message'];
						setTimeout(()=>{    //<<<---    using ()=> syntax
							  this.newmessage = "";
						 }, 3000)
						
					}
                },
                error => {
                	this.forgotError=true;
					this.newmessage ='Something went wrong! Please try again';
					this.appComponent.isLoading = false;
                });
	}
    
    
    
	
}
