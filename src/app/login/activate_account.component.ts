import { Component, OnInit, DoCheck, Injectable } from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router, CanActivate,ActivatedRouteSnapshot,RouterStateSnapshot} from '@angular/router';



@Injectable()
@Component({
  templateUrl: './activate_account.component.html'
})

export class ActivateAccountComponent implements OnInit,DoCheck,CanActivate {
	token= '';
	operation={lifecycle: 'running',status: false,message: ''};

    constructor(private UserService: UserService, private router: Router) {}
 
    ngOnInit() {
       		this.router.routerState.root.queryParams.forEach((params: Params) => {
				if(params['token']!== undefined){
					this.token=params['token'];
					this.activateAccount();
				}
				
			});
    }
	canActivate(route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot) {
	  if(route.queryParams['token']!==undefined){
	   return true;
	  }else{
		this.router.navigate(['']);
		return false;
	}
  }	

    ngDoCheck(){
		
	}
	
	activateAccount(){
		
	  this.UserService.activateAccount(this.token)
		.subscribe(
			data => {
			   this.operation={lifecycle: 'stopped',status: true,message: 'Congrats! your account has been activated'};
			},
			error => {
			   this.operation={lifecycle: 'stopped',status: false,message: error.message};
			});
	}

 
 }

    

			

																																																																																																																								

