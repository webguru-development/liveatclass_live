import { Deserializable } from './deserializable';

export class Group  implements Deserializable{
  id: number;
  group_name: string;
  description: string;
  
  created_at: Date;
  updated_at: Date;
  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }
}