import { Deserializable } from './deserializable';

export class EmailTemplate  implements Deserializable{
  id: number;
  type: string='';
  subject: string='';
  content: string='';
  
  status: boolean=false;
  
  created_at: Date;
  updated_at: Date;
  deserialize(input: any): this {
    Object.assign(this, input);
    this.status=(input.status==1);
    return this;
  }
  
}