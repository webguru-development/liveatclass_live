import { User } from './user';
import { Deserializable } from './deserializable';

export class FileLibrary  implements Deserializable{
  id: number;
  name: string;
  description: string;
  type: boolean;
  filename: string;
  size: number;
  content_type: string;
  images: [];

  is_shared: boolean;
  is_read: boolean;
  is_write: boolean;
  is_delete: boolean;

  user: User;
  
  created_at: Date;
  updated_at: Date;
  deserialize(input: any): this {
    Object.assign(this, input);
    this.user = new User().deserialize(input.user);
    return this;
  }
  
}