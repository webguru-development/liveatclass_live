export class Course {categoryId: number;
    title:string;
    subtitle:string;
    keywords:string;
    category_id:number;
    course_image:any;
    course_summary:any;
    arr: Array<{start: Date, end: Date,enroll_student:number}>; 
	price:number;
  
    enroll_student:number;
    enrollment: number;
 }
