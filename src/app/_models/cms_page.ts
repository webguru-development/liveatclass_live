import { User } from './user';
import { Deserializable } from './deserializable';

export class CmsPage  implements Deserializable{
  id: number;
  title: string='';
  description: string='';
  seo_url: string='';
  meta_title: string='';
  meta_description: string='';
  meta_keywords: string='';
  
  active: boolean=false;
  page_order: number=0;
  
  user: User;
  
  created_at: Date;
  updated_at: Date;
  deserialize(input: any): this {
    Object.assign(this, input);
    this.active=(input.active==1);
    this.user = new User().deserialize(input.user);
    return this;
  }
  
}