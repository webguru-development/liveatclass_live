import { Component, OnInit, DoCheck,ViewChild ,ViewChildren,QueryList, Output, EventEmitter } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import {FileLibraryService} from '../_services/file-library.service'; 
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {Params, Router, ActivatedRoute} from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'angular-bootstrap-md';
@Component({
    templateUrl: 'curriculum_play.component.html'
})

export class CurriculumPlayComponent implements OnInit, DoCheck {
	@ViewChildren(DataTableDirective)
	dtElements: QueryList<DataTableDirective>;
	public Editor = ClassicEditor;
	public isCollapsed :any ={};
	//~ @Output() courseTitleEvent = new EventEmitter<string>();
	@ViewChild('embeddedContentModal', { static: true }) embeddedContentModal: ModalDirective;
	@ViewChild('curriculamclassModal', { static: true }) curriculamclassModal: ModalDirective;
	@ViewChild('contentModal', { static: true }) contentModal: ModalDirective;
	@ViewChild('saveChapter', { static: true }) saveChapter: ModalDirective;
	@ViewChild('deleteChapter', { static: true }) deleteChapter: ModalDirective;
	@ViewChild('addUnitModal', { static: true }) addUnitModal: ModalDirective;
	@ViewChild('addResourceModal', { static: true }) addResourceModal: ModalDirective;
	@ViewChild('liveclassModal', { static: true }) liveclassModal: ModalDirective;
	@ViewChild('deleteUnitModal', { static: true }) deleteUnitModal: ModalDirective;
	@ViewChild('deleteResourceModal', { static: true }) deleteResourceModal: ModalDirective;
	@ViewChild('confirmedLearnModal', { static: true }) confirmedLearnModal: ModalDirective;
	dtOptions: any = {};
	showMore={highlights: false,about:false};
	courseChapters:any={id:'',name:'',description:'',message:'',unit:{id:'',name:'',message:'',resource:{id:'',name:'',resource_type:'',message:''}}};
   	resourceId:any;
	currentUser:any;
	
	resource:any={};
	course:any={};
	completedCourses=[];
	enrolledCourseCount=0;
	
	showChapterContainer=false;
	totalResources=0;
	currentResourceIndex=0;
	prevResourceId=0;
	nextResourceId=0;
	currentChapterIndex=0;
	currentUnitIndex=0;
	token='';
	markAsLearnChecked = false;
    constructor(private FileLibraryService: FileLibraryService, private UserService: UserService, private router: Router, private route: ActivatedRoute , public sanitizer: DomSanitizer) {

   
    }

   
	ngOnInit() {
		const that = this;
		that.currentUser= JSON.parse(localStorage.getItem("user_data"));
		that.token=localStorage.getItem("token");
		that.route.params.subscribe(params=>{ 
			that.resourceId=params['id'] 
			this.getResources();
		});
	}

	ngDoCheck(){


	}
	getYoutubeId(url) {
		const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
		const match = url.match(regExp);

		return (match && match[2].length === 11)
		  ? match[2]
		  : null;
	}
	isPassed(startTime){
		return new Date(startTime) < new Date();
	}
	getFileSrc(file){
		let filename=file.filename;
		if(file.content_type=='youtube'){
			let youtubeId=this.getYoutubeId(filename);
			filename='//www.youtube.com/embed/'+ youtubeId;
		}
		return this.sanitizer.bypassSecurityTrustResourceUrl(filename);
	}
	getResources(){
		this.UserService.getResources(this.resourceId).subscribe(resp => {
		  this.resource=resp.data.resource;
		  if(this.resource.file){
			 this.resource.file.filename= this.getFileSrc(this.resource.file);
			 console.log(this.resource.file.filename); 
		  }
		  this.course=resp.data.course;
		  this.completedCourses=resp.data.completed;
		  this.enrolledCourseCount=resp.data.currentUserEnrolledCourseCount;
		  
		  this.UserService.changeCourseTitle(this.course.course_title)
		  this.curriculumResource(this.course,this.resource);
		  if(this.completedCourses.indexOf(this.resource.id) !=-1){
		  	this.markAsLearnChecked = true;
		  }
		  console.log(this.markAsLearnChecked);
		},error=>{
		  //ToDo: do something with error;
		});
	  }

	markAsLearnRevert(){
		if(this.markAsLearnChecked){
			this.markAsLearnChecked = false;
		}else{
			this.markAsLearnChecked = true;
		}
		console.log(this.markAsLearnChecked);
	}

	markAsLearn(event){
		this.markAsLearnChecked = event.target.checked;
		console.log(this.markAsLearnChecked);
	}
	  
	completedResource(resourceId){
		console.log(this.markAsLearnChecked);
		let resourceIndex=this.completedCourses.indexOf(resourceId);
		
		this.UserService.completedResource(resourceId,this.markAsLearnChecked).subscribe(resp => {
			if(this.markAsLearnChecked){
				if(resourceIndex == -1){
					this.completedCourses.push(resourceId);
				}
			}else{
				if(resourceIndex != -1){
					this.completedCourses.splice(resourceIndex,1);
				}
			}
		});
		
	}  
	
	curriculumResource(course,currentResource){
		this.currentResourceIndex=0;
		this.nextResourceId=0;
		this.prevResourceId=0;
		this.totalResources=0;
		this.currentChapterIndex=0;
		this.currentUnitIndex=0;
							
		course.chapters.forEach((chapter,chapterIndex)=>{
			chapter.units.forEach((unit,unitIndex)=>{
				unit.resources.forEach((resource,resourceIndex)=>{
					if(this.nextResourceId > 0) return;
					
					if(this.currentResourceIndex > 0){
						this.nextResourceId=resource.id;
					}else{
						if(resource.id == currentResource.id ){
							this.totalResources=unit.resources.length;
							this.currentResourceIndex=resourceIndex+1;
							this.currentChapterIndex=chapterIndex+1;
							this.currentUnitIndex=unitIndex+1;
						}else{
							this.prevResourceId=resource.id;
						}
					}
				});
			});
		});
	}
	
}
