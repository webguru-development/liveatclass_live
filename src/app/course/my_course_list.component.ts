import { Component, OnInit, DoCheck,ViewChild } from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router} from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { AppComponent } from '../app.component';
import { ModalDirective } from 'angular-bootstrap-md';
import { DataTableDirective } from 'angular-datatables';

@Component({
    templateUrl: 'my_course_list.component.html'
})

export class MyCourselistComponent implements OnInit, DoCheck {
	@ViewChild(DataTableDirective, {static: false})
	private datatableElement: DataTableDirective;
	dtOptions: any = {};
    message = '';
    newmessage = '';
	data:any;
    enrolledCoursesList = [];
    currentUser:any;
    permissions:any;
    language:number;
    courseId:any;
    loadingComplete = false;
    constructor(private UserService: UserService, private router: Router,private appComponent: AppComponent,public sanitizer: DomSanitizer) {}

	ngOnInit() {
		const that = this;
		that.data=JSON.parse(localStorage.getItem("language"));
		that.currentUser= JSON.parse(localStorage.getItem("user_data"));
		that.permissions= JSON.parse(localStorage.getItem("permissions"));
		that.language = JSON.parse(localStorage.getItem("activeLanguage"));
		let options={
				pagingType: 'simple_numbers',
				pageLength: 10,
				dom: 'fltipr',
				processing: true,
				serverSide: true,
				language: {
					search: "_INPUT_",
					searchPlaceholder: "Search by Name"
				},
				order: [[0, 'asc']],
		};
		let tableCols=[];
		let tableOrder=[ 0, "asc" ];
		if(that.currentUser.user_type=='superadmin' || that.currentUser.user_type=='admin'){
			tableCols.push({ 'searchable': false,'orderable': false });
			tableOrder=[ 1, "asc" ];
		}
		tableCols.push({ data: 'course_uid' });
		tableCols.push({ data: 'course_title' });
		tableCols.push({ 'orderable': false,data:'assigned_teacher' });
		tableCols.push({ 'searchable': false,'orderable': false});
		tableCols.push({ 'searchable': false,'orderable': false });
		if(that.currentUser.user_type=='superadmin' || that.currentUser.user_type=='admin'){
			tableCols.push({ data: 'is_active' });
		}
		that.dtOptions['courseEnroll'] = JSON.parse(JSON.stringify(options));
		that.dtOptions['courseEnroll']['language']['searchPlaceholder']='Search by Course Name';
		that.dtOptions['courseEnroll']['ajax'] =  (dataTablesParameters: any, callback) => {
			that.UserService.getMyEnrolledCourses(dataTablesParameters).subscribe(resp => {
				//console.log(resp.data);
				let c_url_temp = window.location.href.split('/');
				let c_url = c_url_temp[0]+'//'+c_url_temp[2];
				let response = [];
				resp.data.forEach(function(temp , key){
					let teachers = '';
					temp.assigned_teacher.forEach(function(teacher, index){
						if(index>0){
							teachers += ', <a href="'+c_url+'/profile/'+temp.assigned_teacher_id[index]+'" target="_blank">'+teacher+'</a>';
						}else{
							teachers = '<a href="'+c_url+'/profile/'+temp.assigned_teacher_id[index]+'" target="_blank">'+teacher+'</a>';
						}
					})
					temp.assigned_teacher = teachers;
					response[key] = temp;
				});
				//console.log(response);
				that.enrolledCoursesList = response;
				callback({
				  recordsTotal: resp.total,
				  recordsFiltered: resp.total,
				  data: []
				});
			});
		}	
		that.dtOptions['courseEnroll']['columns'] = tableCols,
		that.dtOptions['courseEnroll']['order'] =  [tableOrder]
		
    }
  	editCourse(something){

  	}
  	removeSelectedCourses(){

  	}
  	courseClone(){

  	}
  	getWidthPercent(percent){
    	return this.sanitizer.bypassSecurityTrustStyle('width: '+percent+'%');
  	}
  	coursePublish(something){

  	}
    ngDoCheck(){
    if(this.data!=JSON.parse(localStorage.getItem("language"))){
		this.data=JSON.parse(localStorage.getItem("language")); 
		}
		if(this.newmessage!=""){
			this.message=this.newmessage;
		}
	}
}
