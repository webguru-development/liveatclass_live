import { Component, OnInit, DoCheck} from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router, ActivatedRoute} from '@angular/router';

@Component({
	selector: 'common_course_left',
    templateUrl: 'common_course_left.component.html'
})

export class CommoncourseComponent implements OnInit, DoCheck {
    message = '';
    newmessage = '';
	data:any;
	model: any = {};
    CourseID:number;
    currentUser:any;
    confirmBox = false;
    currentCourse=null;
    permissions:any;
    constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) {}

	ngOnInit() {
       		this.data=JSON.parse(localStorage.getItem("language"));
       		this.currentUser= JSON.parse(localStorage.getItem("user_data"));
       		this.permissions= JSON.parse(localStorage.getItem("permissions"));
       		this.route.params.subscribe(params => {this.CourseID=params['id']});

			
    }
  
    ngDoCheck(){
    if(this.data!=JSON.parse(localStorage.getItem("language"))){
		this.data=JSON.parse(localStorage.getItem("language")); 
		}
		if(this.newmessage!=""){
			this.message=this.newmessage;
		}
	}
	
  deleteMyCourse(id:number){
	this.userService.deleteMyCourse(id)
			.subscribe(
				data => {  
				this.router.navigate(['/mycourse']);
				this.closePopUp();	
				},
				error => {

				}
			);   
	}
	closePopUp(){		
		this.currentCourse=null;
		this.confirmBox=false;
	}
	
	 public openConfirmationDialog(id:number) {
		this.currentCourse=id;
		this.confirmBox=true;
	  }
 
}
