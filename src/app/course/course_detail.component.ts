import { Component, OnInit, DoCheck,ViewChild ,ViewChildren,QueryList } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import {FileLibraryService} from '../_services/file-library.service'; 
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {Params, Router, ActivatedRoute} from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'angular-bootstrap-md';
@Component({
    templateUrl: 'course_detail.component.html'
})

export class CourseDetailComponent implements OnInit, DoCheck {
	@ViewChildren(DataTableDirective)
	dtElements: QueryList<DataTableDirective>;
	
	
	public isCollapsed :any ={};
	@ViewChild('embeddedContentModal', { static: true }) embeddedContentModal: ModalDirective;
	@ViewChild('curriculamclassModal', { static: true }) curriculamclassModal: ModalDirective;
	@ViewChild('contentModal', { static: true }) contentModal: ModalDirective;
	@ViewChild('saveChapter', { static: true }) saveChapter: ModalDirective;
	@ViewChild('deleteChapter', { static: true }) deleteChapter: ModalDirective;
	@ViewChild('addUnitModal', { static: true }) addUnitModal: ModalDirective;
	@ViewChild('addResourceModal', { static: true }) addResourceModal: ModalDirective;
	@ViewChild('editResourceModal', { static: true }) editResourceModal: ModalDirective;
	@ViewChild('liveclassModal', { static: true }) liveclassModal: ModalDirective;
	@ViewChild('deleteUnitModal', { static: true }) deleteUnitModal: ModalDirective;
	@ViewChild('deleteResourceModal', { static: true }) deleteResourceModal: ModalDirective;
	@ViewChild('confirmedAllowedModal', { static: true }) confirmedAllowedModal: ModalDirective;
	
	dtOptions: any = {};
	model: any = {};
	showMore={highlights: false,about:false};
	courseChapters:any={id:'',name:'',description:'',message:'',unit:{id:'',name:'',message:'',resource:{id:'',name:'',resource_type:'',message:''}}};
   	CourseID:any;
	currentUser:any;
	classes: [];
	ClassesList:[];
	embedList=[];
	fileList=[];
    promoVideo:any= '';
	completedResources=[];
	totalResources=0;
	completionStatus=0;
	assignedTeachers: [];
	enrolledCourseCount=0;
	showFileType:number=-1;
	currentFolderId=0;
	selectedClasses={};
	selectedFile:any={};
	selectedClass:any={};
	isFile:boolean=true;
	isFolder:boolean=false;
	isEmbeded:boolean=false;
	folderList=[];
	unsanitizedVideoUrl='';
	resource:any={id: '', name: '', message:  ''};
 	operation={lifecycle: 'started',status: false,message: ''};
 	add_resourcename:any;
	
    constructor(private FileLibraryService: FileLibraryService, private UserService: UserService, private router: Router, private route: ActivatedRoute , public sanitizer: DomSanitizer) {

   
    }

   
	ngOnInit() {
		const that = this;
		that.currentUser= JSON.parse(localStorage.getItem("user_data"));
		that.route.params.subscribe(params=>{
			that.CourseID=params['id']
			  let options={
				pagingType: 'simple_numbers',
				pageLength: 10,
				dom: 'fltipr',
				processing:true,
				serverSide: true,
				language: {
				  search: "_INPUT_",
				  searchPlaceholder: "Search by Name"
				},
				order: [[0, 'asc']],
			  };
			  let detailTypes={
				assignedClasses: {
				  placeholder: 'Search by Live Class',
				  func: 'getClasses',
				  classParam: 'ClassesList',
				  params: ['CourseID'], 
				  service: 'UserService',
				  columns: [
					{"name": 'created_at',"orderable": true},
					{"name": 'liveclass.title',"orderable": true},
					{"name": 'id',"orderable": true},
					{"name": 'id',"orderable": false},
				  ]
				},
				classes: {
				  placeholder: 'Search by Class Name',
				  func: 'getMyClassList',
				  classParam: 'classes', 
				  params: [],
				  service: 'UserService',
				  columns: [
					{"name": 'title',"orderable": true},
					{"name": 'teacher.firstname',"orderable": false},
					{"name": 'id',"orderable": false},
				  ]
				},
				files: {
					placeholder: 'Search by File/Folder',
					func: 'getFiles',
					classParam: 'fileList',
				    service: 'FileLibraryService',
					params: ['currentFolderId','showFileType'], 
					columns: [
						{"name": 'name',"orderable": true},
						{"name": 'type',"orderable": true},
						{"name": 'id',"orderable": false},
					]
				},
				embedded_content: {
					placeholder: 'Search by File Name',
					func: 'getEmbeds',
					classParam: 'embedList',
					params: [],
				    service: 'FileLibraryService',
					columns: [
						{"name": 'name',"orderable": true},
						{"name": 'contentType',"orderable": true},
						{"name": 'id',"orderable": false}
					]
				},
			  };
			  Object.keys(detailTypes).forEach(name=>{
				this.dtOptions[name] = JSON.parse(JSON.stringify(options));
				this.dtOptions[name]['language']['searchPlaceholder']=detailTypes[name]['placeholder'];
				this.dtOptions[name]['columns']=detailTypes[name]['columns'];
				this.dtOptions[name]['ajax'] =  (dataTablesParameters: any, callback) => {
				  let params=[dataTablesParameters];
				  detailTypes[name]['params'].forEach(param=>{ params.push(this[param])});
				  this[detailTypes[name]['service']][ detailTypes[name]['func'] ](...params).subscribe(resp => {
					this[detailTypes[name]['classParam']] = resp.data;
					callback({
					  recordsTotal: resp.total,
					  recordsFiltered: resp.total,
					  data: []
					});
				  });
				}
			  });
		});
		that.getCourseDetails();
		
    }
  
    ngDoCheck(){
    
		
	}
	
	//Courses details, add curriculum.
	
	getYoutubeId(url) {
		const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
		const match = url.match(regExp);
		return (match && match[2].length === 11)
		  ? match[2]
		  : null;
	}
	
	getFileSrc(file){
		let filename=file;
		let youtubeId=this.getYoutubeId(filename);
		filename='//www.youtube.com/embed/'+ youtubeId;
		return this.sanitizer.bypassSecurityTrustResourceUrl(filename);
	}
	videoUrl(videoUrl){
		if(this.videoType(videoUrl)=='youtube'){
			return this.getFileSrc(videoUrl);
		}else{
			return this.sanitizer.bypassSecurityTrustResourceUrl(videoUrl);
		}
	}
	videoType(videoUrl){
		if(videoUrl){
			const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
			if(videoUrl.match(regExp)){
				return 'youtube';
			}else if(videoUrl.indexOf('vimeo') !== -1){
				return 'vimeo';
			}else{
				return 'normal';
			}	
		}
		return 'none';
	}
	checkVideoFormat(videoUrl){
		this.isEmbeded=false;
		const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
		if(videoUrl && (videoUrl.match(regExp) || videoUrl.indexOf('vimeo') !== -1) ){
			this.isEmbeded = true;
		}
		return this.isEmbeded;
	}
	
	resetChapter(){
		this.courseChapters={id:'',name: '',description: '',message:'',unit:{id:'',name:'',message:'',resource:{id:'',name:'',resource_type:'',message:''}}};
	}
	
	getWidthPercent(percent){
		return this.sanitizer.bypassSecurityTrustStyle('width: '+percent+'%');
	}
	
	getCourseDetails(){
		 this.UserService.getCourseDetail(this.CourseID)
			.subscribe(
				data => { 
					this.model = data['getCourseDetail'];
					this.checkVideoFormat(this.model.promotion_video);
					if(this.isEmbeded==true){
						this.promoVideo=this.getFileSrc(this.model.promotion_video); 
					}
					this.unsanitizedVideoUrl=this.model.promotion_video;
        	this.model.promotion_video=this.videoUrl(this.model.promotion_video);
					
					this.assignedTeachers = data['teachers'];
					this.enrolledCourseCount = data['currentUserEnrolledCourseCount'];
					this.completedResources= data.completed;
					this.totalResources=this.courseCompletionStatus(this.model);
					this.completionStatus = this.completedResources.length*100/this.totalResources
					//~ console.log(this.completedResources.length*100/this.totalResources);
				},
				error => {

				}
			); 
	}
	
	courseCompletionStatus(course){
		let totalResources=0;
		course.chapters.forEach(chapter=>{
			chapter.units.forEach(unit=>{
				totalResources +=unit.resources.length;
			});
		});
		return totalResources;
	}
	
	reloadFiles(){
		// get file/folder list according to [currentFolderId] [showFileType]
	  this.dtElements.forEach((dtElement: DataTableDirective) => {
      //@ts-ignore
      if(!$(dtElement.el.nativeElement).is(':visible')) return;
      dtElement.dtInstance.then((dtInstance: DataTables.Api) => {dtInstance.ajax.reload();});
    });
	}
	
	traverseTo(folderId){
		this.currentFolderId=folderId;
		if(folderId != 0) this.folderList.push(folderId);
		this.reloadFiles();
	}
	
	goBack(){
		let folderId=0;
		if(this.folderList.length){
			this.folderList.splice(this.folderList.length - 1, 1);
		}
		if(this.folderList.length){
			folderId= this.folderList[this.folderList.length - 1];
		}
		this.traverseTo(folderId);
	}
	
	showDeleteChapterPopup(chapter:any){
		this.courseChapters=chapter;
		this.courseChapters.unit={};
		this.courseChapters.unit.resource={};
		this.courseChapters.message='';
		this.deleteChapter.show();
	}
	
	showDeleteUnitPopup(chapter:any, unit:any){
		this.courseChapters=chapter;
		this.courseChapters.unit=unit;
		this.courseChapters.unit.resource={};
		this.courseChapters.unit.message='';
		this.deleteUnitModal.show();
	}
	
	showDeleteResourcePopup(chapter:any, unit:any,resource:any){
		this.courseChapters=chapter;
		this.courseChapters.unit=unit;
		this.courseChapters.unit.resource=resource;
		this.courseChapters.unit.resource.message='';
		this.deleteResourceModal.show();
	}
	
	editChapter(index:number){
		this.courseChapters=this.model.chapters[index];
		this.courseChapters.message='';
		this.courseChapters.unit={};
		this.courseChapters.unit.resource={};
		this.saveChapter.show();
	}
	
	editUnit(chapter:any , index:number){
		this.courseChapters=chapter;
		this.courseChapters.unit=chapter.units[index];
		this.courseChapters.unit.message='';
		this.courseChapters.unit.resource={};
		this.addUnitModal.show();
	}
	editResource(resource:any){
		this.resource=resource;
		this.editResourceModal.show();
	}
	
	addUnit(chapter:any){
		this.courseChapters=chapter;
		this.courseChapters.unit={id:'',name:'',message:'',resource:{id:'',name:'',resource_type:'',message:''}};
		this.addUnitModal.show();
	}
	
	addResource(chapter:any,unit:any){
		this.courseChapters=chapter;
		this.courseChapters.unit=unit;
		this.courseChapters.unit.resource={id:'',name:'',resource_type:'',message:''};
		this.selectedFile = {};
		this.selectedClass = {};
		this.addResourceModal.show();
	}
	
	addCourseChapter(form:any){
	
		//~ console.log(this.courseChapters);
		this.UserService.addChapter(this.CourseID,this.courseChapters)
		.subscribe(
					data => {
						if(this.courseChapters.id > 0){
							let index=this.model.chapters.findIndex(x => x.id ===data.chapter.id);
							this.model.chapters[index]=data.chapter;
						}else{
							this.model.chapters.push(data.chapter);
						}
						this.courseChapters.message = data.message;
						form.resetForm();
						setTimeout(()=>{
							  this.saveChapter.hide();
						}, 2000);
					},
					error => {

					}
				); 
		
	}
	removeChapter(){
		if(this.courseChapters.id=='') return;
		//~ console.log(this.courseChapters);
		this.UserService.removeChapter(this.courseChapters.id)
		.subscribe(
					data => {
						let index=this.model.chapters.findIndex(x => x.id ===this.courseChapters.id);
						this.model.chapters.splice(index,1);
						this.courseChapters.message = data.message;
						setTimeout(()=>{
							  this.deleteChapter.hide();
						 }, 2000)
					},
					error => {

					}
				); 
		
	}
	
	
	removeUnit(){
		if(this.courseChapters.unit.id=='') return;
		//~ console.log(this.courseChapters);
		this.UserService.removeUnit(this.courseChapters.unit.id)
		.subscribe(
					data => {
						let courseIndex=this.model.chapters.findIndex(x => x.id ===this.courseChapters.id);
						let unitIndex=this.model.chapters[courseIndex].units.findIndex(x => x.id ===this.courseChapters.unit.id);
						this.model.chapters[courseIndex].units.splice(unitIndex,1);
						this.courseChapters.unit.message = data.message;
						setTimeout(()=>{
							  this.deleteUnitModal.hide();
						 }, 2000)
					},
					error => {

					}
				); 
		
	}
	editUnitResource(f){
		if(this.resource.id=='') return;
		
		this.UserService.editResource(this.resource)
		.subscribe(
					data => {
						this.resource.message = data.message;
						setTimeout(()=>{
							  this.editResourceModal.hide();
							  this.resource={id: '',name: '',message: ''}
							  f.submitted=false;
						 }, 2000)
					},
					error => {
						this.resource.message = error.message;
					}
				); 
		
	}
	removeResource(){
		if(this.courseChapters.unit.resource.id=='') return;
		this.UserService.removeResource(this.courseChapters.unit.resource.id)
		.subscribe(
					data => {
						let courseIndex=this.model.chapters.findIndex(x => x.id ===this.courseChapters.id);
						let unitIndex=this.model.chapters[courseIndex].units.findIndex(x => x.id ===this.courseChapters.unit.id);
						let resourceIndex=this.model.chapters[courseIndex].units[unitIndex].resources.findIndex(x => x.id ===this.courseChapters.unit.resource.id);
						this.model.chapters[courseIndex].units[unitIndex].resources.splice(resourceIndex,1);
						this.courseChapters.unit.resource.message = data.message;
						setTimeout(()=>{
							  this.deleteResourceModal.hide();
						 }, 2000)
					},
					error => {

					}
				); 
		
	}
	
	selectResource(){
		if(this.courseChapters.unit.resource.resource_type == "embedded"){
			this.embeddedContentModal.show();
			this.selectedFile = {};
			this.addResourceModal.hide();
		}
		if(this.courseChapters.unit.resource.resource_type == "content"){
			this.contentModal.show();
			this.selectedFile = {};
			this.addResourceModal.hide();
		}
		if(this.courseChapters.unit.resource.resource_type == "classes"){
			this.curriculamclassModal.show();
			this.selectedClass = {};
			this.addResourceModal.hide();
		}
		//~ if(this.courseChapters.unit.resource.resource_type == "content") this.contentModal.show();
	}
	
	selectContent(){
		if(this.courseChapters.unit.resource.resource_type == "embedded") this.embeddedContentModal.hide();
		if(this.courseChapters.unit.resource.resource_type == "content") this.contentModal.hide();
		this.addResourceModal.show();
	}
	
	selectClass(){
		if(this.courseChapters.unit.resource.resource_type == "classes") this.curriculamclassModal.hide();
		this.addResourceModal.show();
	}
	
	
	addUnitResource(form:any){
		this.UserService.addUnitResource(this.CourseID,this.courseChapters.id,this.courseChapters.unit.id,this.courseChapters.unit.resource,this.selectedFile.id,this.selectedClass.id)
		.subscribe(
					data => {
						let courseIndex=this.model.chapters.findIndex(x => x.id ===this.courseChapters.id);
						let unitIndex=this.model.chapters[courseIndex].units.findIndex(x => x.id ===this.courseChapters.unit.id);
						if(this.courseChapters.unit.resource.id > 0){
							let resourceIndex=this.model.chapters[courseIndex].units[unitIndex].findIndex(x => x.id ===this.courseChapters.unit.resource.id);
							this.model.chapters[courseIndex].units[unitIndex].resources[resourceIndex]=data.unitResource;
						}else{
							this.model.chapters[courseIndex].units[unitIndex].resources.push(data.unitResource);
						}
						this.courseChapters.unit.resource.message = data.message;
						form.resetForm();
						setTimeout(()=>{
						  this.addResourceModal.hide();
						}, 2000);
					},
					error => {

					}
				); 
		
	}
	
	addChapterUnit(form:any){
	
		this.UserService.addChapterUnit(this.CourseID,this.courseChapters.id,this.courseChapters.unit)
		.subscribe(
					data => {
						let courseIndex=this.model.chapters.findIndex(x => x.id ===this.courseChapters.id);
						if(this.courseChapters.unit.id > 0){
							let unitIndex=this.model.chapters[courseIndex].units.findIndex(x => x.id ===this.courseChapters.unit.id);
							this.model.chapters[courseIndex].units[unitIndex]=data.chapterUnit;
						}else{
							this.model.chapters[courseIndex].units.push(data.chapterUnit);
						}
						this.courseChapters.unit.message = data.message;
						form.resetForm();
						setTimeout(()=>{
							  this.addUnitModal.hide();
						 }, 2000)
					},
					error => {

					}
				); 
		
	}//End Courses details and add curriculum.
	
	//add live classes
		 reloadTables(){
			this.dtElements.forEach((dtElement: DataTableDirective) => {
			  //@ts-ignore
			  if(!$(dtElement.el.nativeElement).is(':visible')) return;
			  dtElement.dtInstance.then((dtInstance: DataTables.Api) => {dtInstance.ajax.reload();});
			});
		  }
		  
		 assignClassesToCourse(){
			this.operation={lifecycle: 'running',status: false,message: ''};
			
			let classIds=[];
			Object.keys(this.selectedClasses).forEach(key=>{
			  if(this.selectedClasses[key]) classIds.push(key);
			});
			
			if(classIds.length <= 0) return;
			
			this.UserService.assignClassesToCourse(this.CourseID,classIds).subscribe(resp=>{
			  //@ts-ignore
			  this.operation={lifecycle: 'stopped',status: resp.status,message: resp.message.join('<br>')};
			  //@ts-ignore
			  setTimeout(_=>{
				this.operation={lifecycle: 'started',status: false,message: ''};
				this.liveclassModal.hide();
				this.reloadTables();
			  },1000)
			},error=>{
			  this.operation={lifecycle: 'stopped',status: false,message: error.message};
			});
		  }
		  
	//End live classes.
	
	public Editor = ClassicEditor;
}
