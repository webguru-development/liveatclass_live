import { Component, ElementRef, OnInit, DoCheck,ViewChild , Input ,ChangeDetectorRef,AfterViewInit,OnDestroy ,Inject } from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router,ActivatedRoute} from '@angular/router';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { AppComponent } from '../app.component';
declare var jQuery: any;
import * as types from 'gijgo';
import {IMyDpOptions, IMyDateModel} from 'mydatepicker';	
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import {ModalDirective} from 'angular-bootstrap-md';
@Component({
   //moduleId: module.id,
  templateUrl: 'user_profile.component.html'
})

export class UserProfileCompnent implements OnInit,DoCheck {
@ViewChild('exampleModal9', { static: true }) exampleModal9: ModalDirective;
@ViewChild('dob', { static: false }) dob: ElementRef;

@Input() dob_instance: types.DatePicker;
@Input() dobConfiguration: types.DatePickerSettings;

currentUser: {};
model:any= {};
public file_srcs;
user_id:number=null;
id:number;
profile_image;
countries:any;
operation={lifecycle: 'started',status: false,message: ''};
resetoperation={lifecycle: 'started',status: false,message: ''};
resetModel:any={};
public Editor = ClassicEditor;

	constructor(private userService: UserService, private router: Router,private route: ActivatedRoute, private sanitized: DomSanitizer) {
			let that=this;
			that.dobConfiguration = {
				format: 'yyyy-mm-dd',
				maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
				change: function(e){
						that.model.dob=that.dob_instance.value();
				}
			};
	}

	public myDatePickerOptions: IMyDpOptions = {
			// other options...
			dateFormat: 'yyyy-mm-dd'
		};

	 onDateChanged(event: IMyDateModel) {
			// event properties are: event.date, event.jsdate, event.formatted and event.epoc
		}

	ngOnInit() {
		if(JSON.parse(localStorage.getItem("user_data")) != null){
			  this.currentUser= JSON.parse(localStorage.getItem("user_data"));
			}
		if(this.route.params !=null) this.route.params.subscribe(params => {this.user_id=params['user_id']});
		this.userProfile();
		//~ this.model = this.currentUser;
		this.userService.countries().subscribe(
      data => {  
        this.countries=data;
      },
      error =>{
      }
    );
	}
		

	ngDoCheck(){
	}

	ngAfterViewInit() {
		
		setTimeout(_=>{
			// giving datepicker not a function error, so delaying it a bit
			this.dob_instance = jQuery(this.dob.nativeElement).datepicker(this.dobConfiguration);
			
		},100)
	  }  
  
  ngOnDestroy() {
    this.dob_instance.destroy();
  }
	fileChange(input) {  
      this.profile_image=input.target.files[0];
      this.readFiles(input.target.files);      
  } 
  readFile(file, reader, callback) {  
      reader.onload = () => {  
          callback(reader.result);         
      }  
      reader.readAsDataURL(file);  
  }
    
  readFiles(files) {
    let index=0;  
    // Create the file reader  
    let reader = new FileReader();  
    // If there is a file  
    if (index in files) {  
      // Start reading this file  
      this.readFile(files[index], reader, (result) => {  
          this.file_srcs=this.sanitized.bypassSecurityTrustUrl(result);  
      });  
    }  
  }    

	userProfile(){
		if(this.user_id == undefined || this.user_id == null){
			this.id = this.currentUser['id'];
		}else{
			this.id = this.user_id;
		}
		
		this.userService.userProfile(this.id)
				.subscribe(
					data => {  
						this.model = data['user_data'];
            this.userService.updateCurrentUser(data['user_data']);
						if(this.model['profile_image'] == '' || this.model['profile_image'] == null){
							this.file_srcs=("/assets/images/avatar.png");
						}else{
							this.file_srcs= this.model['profile_image'];
						}
						this.dob_instance.value(this.model.dob);
						
						//~ localStorage.setItem("userProfileData",JSON.stringify(data['user_data']));
					},
					error => {

					}
				);          
	}
  resetPassword(f){
    this.resetoperation={lifecycle: 'running',status: false,message: ''};
    this.userService.resetPassword(this.resetModel.old_password, this.resetModel.new_password)
      .subscribe(
        data => { 
          f.submitted=false;
          this.resetModel={};
          this.resetoperation={lifecycle: 'stopped',status: true,message: 'Password reset successfully'};
          setTimeout(_=>{
            this.exampleModal9.hide();
          },3000);
        },
        error => {
          this.resetoperation={lifecycle: 'stopped',status: false,message: error.message};
        }
      );
  }
	
	savePersonalInstituteUser(){
		this.operation={lifecycle: 'running',status: false,message: ''};
 		let gotProfile=false;
    if(this.profile_image && this.profile_image.name){
			this.userService.setProfilePic(this.profile_image)
				.subscribe(
					data => {
            if(!gotProfile){
              gotProfile=true;
              this.userProfile();  
            }
					},
					error => {

					}
				);   
		}
		this.userService.savePersonalInstituteUser(this.model)
				.subscribe(
					data => { 
						if(data['code'] == '200'){
              if(!gotProfile){
                gotProfile=true;
                this.userProfile();  
              }
							this.operation={lifecycle: 'stopped',status: true,message: 'Profile updated successfully'};
 						}
						
					},
					error => {
						this.operation={lifecycle: 'stopped',status: false,message: error.message};
					}
				);   
	}


	}
