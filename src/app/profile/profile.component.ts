﻿import { Component, OnInit } from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router, ActivatedRoute} from '@angular/router';

@Component({
    templateUrl: 'profile.component.html'
})

export class ProfileComponent implements OnInit {
  	user:User=new User();
    user_id;
    liveClassHours=0;
    inCourses=0;
    trainedStudents=0;
    showMore={'description': false};
    constructor(private UserService: UserService, private router: Router, private route: ActivatedRoute) {}
    ngOnInit() {
        this.route.params.subscribe(params => {
          this.user_id=params['user_id']
          if(typeof this.user_id == 'undefined' || this.user_id==''){
            this.router.navigate(['/dashboard']);
            return;
          }
          this.UserService.userProfile(this.user_id).subscribe(data => {  
            this.user = data['user_data']; 
            this.liveClassHours=data['total_class_hours'];
            this.inCourses=data['total_assigned_courses'];
            this.trainedStudents=data['total_students'];
            if(this.user['profile_image'] == '' || this.user['profile_image'] == null){
              this.user['profile_image']="/assets/images/avatar.png";
            }
          },error => {
            //ToDo: Do something on error;
          });
        });
    }
}
