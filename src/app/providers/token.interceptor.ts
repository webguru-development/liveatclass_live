import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token=localStorage.getItem('token');
    if(token) request = request.clone({
      setHeaders: {
        Authorization: 'Bearer '+token
      }
    });
    return next.handle(request).pipe(
		tap(
			event => {
				if (event instanceof HttpResponse) {
					let contentType=event.headers.get("content-type");
				}
				return event;
			}
		)
    );
  }
}
