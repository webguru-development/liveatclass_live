import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable,throwError} from 'rxjs';
import {tap,catchError} from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

// custom error for response
function ResponseError(message,extraData) {
  this.name = 'ResponseError';
  this.message = message;
  this.data=extraData;
  this.stack = (new Error()).stack;
}
ResponseError.prototype = Object.create(Error.prototype);
ResponseError.prototype.constructor = ResponseError;

@Injectable()
export class SuccessInterceptor implements HttpInterceptor {
  constructor(private toastr: ToastrService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
        tap(
          event => {
			if (event instanceof HttpResponse) {
				let contentType=event.headers.get("content-type");
				if (contentType && contentType.indexOf("application/json") !== -1 && event.body.code  && event.body.code != 200 && event.body.code != 202) { 
					throw new ResponseError(event.body.message,event.body);
					
				}
            }
            return event;
          }
        ),
        catchError(err=>{
			if(err.name=='ResponseError') this.toastr.error(err.message,'Error!',{timeOut: 2000})
			return throwError(err);
		})
      );
  }
}
