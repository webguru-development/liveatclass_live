import { Component, OnInit, DoCheck, Inject, ViewChild } from '@angular/core';
import {CmsPageService} from '../_services/cms-page.service';
import { CmsPage } from '../_models/cms_page';
import {Params, Router,ActivatedRoute} from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';
import { DataTableDirective } from 'angular-datatables';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';


@Component({
   //moduleId: module.id,
  templateUrl: './cms_page_form.component.html'
})

export class CmsPageFormComponent implements OnInit,DoCheck {
	operation={lifecycle: 'started',status: false,message: ''};
 	page:CmsPage=new CmsPage();
  Editor = ClassicEditor;
     
 	constructor(private router: Router, private CmsPageService: CmsPageService,private route: ActivatedRoute) {    		
  }
  ngOnInit() {
  	let that=this;
    that.route.params.subscribe(params => {
  		if(params['id']){
  			this.CmsPageService.getPage(params['id']).subscribe(resp=>{
	  			this.page=resp.data
	  		},error=>{
	  			//Todo: do something with error
	  		})	
  		}
  	});
	}
  onReady(e){
    e.plugins.get('FileRepository').createUploadAdapter = function (loader) {
      let uploader=function(loader){
        this.loader=loader;
      };
      uploader.prototype.upload = function() {
        return this.loader.file.then( file => new Promise( ( resolve, reject ) => {
          var myReader= new FileReader();
          myReader.onloadend = (e) => {
             resolve({ default: myReader.result });
          }
          myReader.readAsDataURL(file);
        } ) );
      };
      return new uploader(loader);
    };
  }
  slugify(){
    if(this.page.seo_url == '') this.page.seo_url=this.page.title;
    this.page.seo_url=this.page.seo_url.toString().toLowerCase()
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '');
  }
	ngDoCheck(){
		
	}
	savePage(){
		this.operation={lifecycle: 'running',status: false,message: ''};
 		this.CmsPageService.savePage(this.page)
			.subscribe(
				data => {
					this.operation={lifecycle: 'stopped',status: true,message: 'Cms Page saved successfully'};
 					this.router.navigate(['/cms_pages']); 
				},
				error => {
          this.operation={lifecycle: 'stopped',status: false,message: error.message};
        }
			);
	}
	
 }