import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {User} from './_models/user';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { MenuBarComponent } from './menu-bar.component';
import { FooterBarComponent } from './footer-bar.component';
import { AddcourseComponent } from './course/add_course.component';
import { LoginSettingComponent } from './accounts_settings/login-settings.component';
import { LogoSettingComponent } from './accounts_settings/logo-settings.component';
import { InterfaceComponent } from './accounts_settings/interface.component';
import { DomainComponent } from './accounts_settings/domain.component';
import { StorageComponent } from './accounts_settings/storage.component';
import { EditPersonalDetailComponent } from './accounts_settings/edit-personal-detail.component';
import { UserListComponent } from './accounts_settings/user-lists.component';
import { NewpasswordComponent } from './login/new_password.component';
import { ActivateAccountComponent } from './login/activate_account.component';
import { ProfileComponent } from './profile/profile.component';
import { UserProfileCompnent } from './profile/user_profile.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import {UserService} from './_services/user.service';
import { routing }        from './app.routing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { LoginRouteGuard } from './login-route-guard';
import { RecaptchaModule } from 'angular-google-recaptcha';
import { ManageCourselistComponent } from './course/manage_course_list.component';
import { CoursePreviewComponent } from './course/course_preview.component';
import { CommoncourseComponent } from './course/common_course_left.component';

import { CourseCatalogueComponent } from './course/course_catalogue.component';
import { CourseDetailComponent } from './course/course_detail.component';
import { CurriculumPlayComponent } from './course/curriculum_play.component';
import { MyCourselistComponent } from './course/my_course_list.component';

import { Ng2CarouselamosModule } from 'ng2-carouselamos';

import { DashboardComponent } from './institute/dashboard.component';
import { DashboardLeftBarComponent } from './institute/commonleftbar.component';
import { TokenInterceptor } from './providers/token.interceptor';
import { SuccessInterceptor } from './providers/success.interceptor';
import { ToastrModule } from 'ngx-toastr';
import {Ng2PaginationModule} from 'ng2-pagination';
import { NgDatepickerModule } from 'ng2-datepicker';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AngularDraggableModule } from 'angular2-draggable';
import { MatTabsModule } from '@angular/material';
import { FileLibraryComponent } from './library/file_library.component';
import { RecordingComponent } from './library/recording.component';
import { CmsPageListComponent } from './cms_pages/cms_page_list.component';
import { CmsPageFormComponent } from './cms_pages/cms_page_form.component';
import { EnrolledLiveClassListComponent } from './live_classes/enrolled_live_class_list.component';
import { LiveClassListComponent } from './live_classes/live_class_list.component';
import { LiveClassFormComponent } from './live_classes/live_class_form.component';
import { GroupComponent } from './groups/group.component';
import { GroupDetailComponent } from './groups/group_detail.component';
import { TeacherDashoardComponent } from './institute/teacher_dashboard.component';
import { StudentDashoardComponent } from './institute/student_dashboard.component';
import { PlanDetailComponent } from './plan/plan_detail.component';
import { PlanUpgradeComponent } from './plan/plan_upgrade.component';

import { MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { MyDatePickerModule } from 'mydatepicker';
import { LoginComponent } from './login/login.component';

import { PageComponent } from './page.component';
import { ComingSoonComponent } from './coming_soon.component';

import {EmailTemplateComponent} from './email_templates/email_templates.component';
//datepicker

//bootstrap
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

//ckeditor
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

//charts
import { ChartsModule } from 'ng2-charts';
//moment

import { MomentModule } from 'angular2-moment';

//Sortable
import { SortablejsModule } from 'ngx-sortablejs';
import { DragdropDirective } from './dragdrop.directive'
import { CarouselModule } from 'ngx-owl-carousel-o';
import { CertificateComponent } from './library/certificate.component';
import { JwPaginationModule } from 'jw-angular-pagination';

@NgModule({
  declarations: [
    AppComponent,
    PlanDetailComponent,
    PlanUpgradeComponent,
    LoginSettingComponent,
    LogoSettingComponent,
    EmailTemplateComponent,
    InterfaceComponent,
    DomainComponent,
    StorageComponent,
    UserListComponent,
    MenuBarComponent,
    FooterBarComponent,
    ProfileComponent, 
    NewpasswordComponent,
    ActivateAccountComponent,
    
    AddcourseComponent,
    ManageCourselistComponent,
    CourseCatalogueComponent,
    CourseDetailComponent,
    CoursePreviewComponent,
    CurriculumPlayComponent,
    MyCourselistComponent,
    
    CommoncourseComponent,
    DashboardComponent,
    DashboardLeftBarComponent,
    EditPersonalDetailComponent,
    UserProfileCompnent,
    FileLibraryComponent,
    RecordingComponent,
    CmsPageListComponent,
    CmsPageFormComponent,
    EnrolledLiveClassListComponent,
    LiveClassListComponent,
    LiveClassFormComponent,
    GroupComponent,
    GroupDetailComponent,
    TeacherDashoardComponent,
    StudentDashoardComponent,
	  LoginComponent,
    PageComponent,
    ComingSoonComponent,
    DragdropDirective,
    CertificateComponent
    
  ],
  imports: [
  JwPaginationModule,
  CarouselModule,
	SortablejsModule.forRoot({ animation: 150 }),
    NgbModule,
	BrowserAnimationsModule,
	BrowserModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    routing,
    RecaptchaModule,
    Ng2CarouselamosModule,
    Ng2PaginationModule,
    CKEditorModule,
    ChartsModule,
    NgDatepickerModule,
    FontAwesomeModule,
    AngularFontAwesomeModule,
    DataTablesModule,
    ToastrModule.forRoot({maxOpened: 1}),
    MatTabsModule,
    MyDatePickerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
	MomentModule,
	AngularDraggableModule
    
    
  ],
   
  providers: [
  Title,
  UserService,
  LoginRouteGuard,
  NewpasswordComponent,
  ActivateAccountComponent,
  AppComponent,
  MenuBarComponent,
  User,
  {
	  provide: HTTP_INTERCEPTORS,
	  useClass: TokenInterceptor,
	  multi: true
  },
  {
	  provide: HTTP_INTERCEPTORS,
	  useClass: SuccessInterceptor,
	  multi: true
  },
],
  bootstrap: [AppComponent],

})
export class AppModule { 
 constructor() {
    // Add an icon to the library for convenient access in other components
    library.add(faCoffee,fas);
  }
	
}
