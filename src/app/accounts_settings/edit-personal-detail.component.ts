import { Component, ElementRef, OnInit, DoCheck,ViewChild , Input ,ChangeDetectorRef,AfterViewInit,OnDestroy ,Inject,ViewChildren,QueryList } from '@angular/core';
import { GroupService } from '../_services/group.service'; 
import { DomSanitizer } from '@angular/platform-browser';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router,ActivatedRoute,RoutesRecognized} from '@angular/router';
import { AppComponent } from '../app.component';
import { ModalDirective } from 'angular-bootstrap-md';
import { DataTableDirective } from 'angular-datatables';
declare var jQuery: any;
import * as types from 'gijgo';
import * as moment from 'moment';

@Component({
   //moduleId: module.id,
  templateUrl: './edit-personal-detail.component.html'
})

export class EditPersonalDetailComponent implements OnInit,DoCheck {
  @ViewChild('dob', { static: false }) dob: ElementRef;
  @ViewChild('enrollCourseModal', { static: true }) enrollCourseModal: ModalDirective;
  @ViewChild('deleteEnrollCourseModal', { static: true }) deleteEnrollCourseModal: ModalDirective;
  @ViewChild('assignGroupModal', { static: true }) assignGroupModal: ModalDirective;
  @ViewChild('deleteAssignGroupModal', { static: true }) deleteAssignGroupModal: ModalDirective;
  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;
  @Input() dob_instance: types.DatePicker;
  @Input() dobConfiguration: types.DatePickerSettings;
  //~ dtTriggerCourses: Subject<any> = new Subject();
  //~ dtTriggerGroups: Subject<any> = new Subject();
  operation={lifecycle: 'started',status: false,message: ''};
  dtOptions: any = {};
  loadTable:any={'assignedGroups': true};
  userId:any;
  message:any = {};
  courseslist:[];
  teacherCoursesCounts:number=0;
  studentCoursesCounts:number=0;
  groupsCounts:number=0;
  ClassesCounts:number=0;
  selectedCourses={};
  assignedCourse:any={};
  selectedGroups={};
  assignedGroup:any={};
  groupslist:[];
  userProfile:any={};
  assignedGroupsList = [];
  enrolledCoursesList = [];
  assignedCoursesList = [];
  liveClassesList = [];
  userTypeList=[];
  model: any = {user:{id:'',email:'',user_type_id:'',user_type:{id:'',name:''}}};
  currentTab:string='group';
  tab = '';
  countries:any;
  edit:boolean=false;
  isActive:number=1;
  selectedUser:any=-1;

  constructor(private UserService: UserService,private GroupService: GroupService, private router: Router,private route: ActivatedRoute, private appComponent: AppComponent, public sanitizer: DomSanitizer) {
	this.route.queryParams.subscribe(params => {
		if(typeof params.tab != 'undefined'){
			this.currentTab=params.tab;
		}
		if(typeof params.edit != 'undefined'){
			this.edit=params.edit=='1';
		}
    });
    let that=this;
	that.dobConfiguration = {
		format: 'yyyy-mm-dd',
		maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
		change: function(e){
      that.model.dob=that.dob_instance.value();
    }
	};
   }
     
   ngOnInit() {
	  this.UserService.countries().subscribe(data=>{
      this.countries=data;
    },error=>{

    });
    this.route.params.subscribe(params=>{
	  this.userId=params['id']
	  this.editInstituteUser(this.userId);
	  this.getUserType();
	   
      let options={
        pagingType: 'simple_numbers',
        pageLength: 10,
        dom: 'fltipr',
        processing: true,
        serverSide: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search by Name"
        },
        order: [[0, 'asc']],
        user_id : this.selectedUser
      };
      let detailTypes={
        assignedGroups: {
          placeholder: 'Search by Group Name',
          func: 'getAssignedGroups',
          classParam: 'assignedGroupsList',
          params: ['userId'],
          service: 'UserService',
          columns: [
            {"name": 'group.group_name',"orderable": true},
            {"name": 'id',"orderable": false},
          ]
        },
        courseAssign: {
          placeholder: 'Search by Course Title',
          func: 'getAssignedCourses',
          //func: 'getEnrolledCoursesFromAdmin',
          classParam: 'assignedCoursesList',
          params: ['userId'],
          service: 'UserService',
          columns: [
            {"name": 'course.course_title',"orderable": true},
            {"name": 'created_at',"orderable": true},
            {"name": 'completed',"orderable": true},
            {"name": 'id',"orderable": false}
          ]
        },
        courseEnroll: {
          placeholder: 'Search by Course Title',
          func: 'getEnrolledCourses',
          classParam: 'enrolledCoursesList',
          params: ['userId'],
          service: 'UserService',
          columns: [
            {"name": 'course.course_title',"orderable": true},
            {"name": 'created_at',"orderable": true},
            {"name": 'completed',"orderable": true},
            {"name": 'id',"orderable": false}
          ]
        },
        assignedClasses: {
          placeholder: 'Search by Live Class',
          func: 'getAssignedLiveClasses',
          classParam: 'liveClassesList', 
          params: ['userId'],
          service: 'UserService',
          columns: [
            {"name": 'liveclass.title',"orderable": true},
            {"name": 'liveclass.start_time',"orderable": true},
            {"name": 'id',"orderable": true},
            {"name": 'is_attended',"orderable": false},
          ]
        },
        groups: {
          placeholder: 'Search by Name',
          func: 'getGroups',
          classParam: 'groupslist', 
          params: [],
          service: 'GroupService',
          columns: [
            {"name": 'group_name',"orderable": true},
            {"name": 'id',"orderable": false},
          ]
        },
        courses: {
          placeholder: 'Search by Title',
          //func: 'getMyCourseList',
          func: 'getMyCourseListByUser',
          classParam: 'courseslist', 
          params: ['userId','isActive'],
          service: 'UserService',
          columns: [
            {"name": 'course_title',"orderable": true},
            {"name": 'id',"orderable": false},
          ]
        },
        classes: {
          placeholder: 'Search by Class Name',
          func: 'getMyClassList',
          classParam: 'classes', 
          params: [],
          service: 'UserService',
          columns: [
            {"name": 'title',"orderable": true},
            {"name": 'id',"orderable": false},
          ]
        }
      };
      Object.keys(detailTypes).forEach(name=>{
        this.dtOptions[name] = JSON.parse(JSON.stringify(options));
        this.dtOptions[name]['language']['searchPlaceholder']=detailTypes[name]['placeholder'];
        this.dtOptions[name]['columns']=detailTypes[name]['columns'];
        this.dtOptions[name]['ajax'] =  (dataTablesParameters: any, callback) => {
		let params=[dataTablesParameters];
		  detailTypes[name]['params'].forEach(param=>{ params.push(this[param])});
          this[detailTypes[name]['service']][ detailTypes[name]['func'] ](...params).subscribe(resp => {
            this[detailTypes[name]['classParam']] = resp.data;
            callback({
              recordsTotal: resp.total,
              recordsFiltered: resp.total,
              data: []
            });
          });
        }
      });
    });
	   
	   
	   
	   //~ this.route.params.subscribe(params => {this.userId=params['id']});
	   //~ this.editInstituteUser(this.userId);
	   //~ this.getUserType();
	   //~ this.getCoursesList();
	   //~ 
	   //~ let options={
				//~ dom: 'ftp',
				//~ serverSide: true,
				//~ language: {
					//~ search: "_INPUT_",
					//~ searchPlaceholder: "Search by Name"
				//~ }
		//~ };

       //group
       //~ this.dtOptions['group'] = JSON.parse(JSON.stringify(options));
       //~ this.dtOptions['group']['language']['searchPlaceholder']='Search by Group Name';
       //~ this.dtOptions['group']['serverSide']=false;
       //~ this.UserService.getAssignedGroups({length: -1,start: 0,order:[],search:{value: ''}},this.userId).subscribe(resp => {
			//~ this.assignedGroupsList = resp.data;
			//~ this.dtTriggerGroups.next();
		//~ });
	   //~ this.dtOptions['group']['ajax'] =  (dataTablesParameters: any, callback) => {
			//~ this.UserService.getAssignedGroups(dataTablesParameters,this.userId).subscribe(resp => {
				//~ this.assignedGroupsList = resp.data;
				//~ callback({
				  //~ recordsTotal: resp.total,
				  //~ recordsFiltered: resp.total,
				  //~ data: []
				//~ });
			//~ });
		//~ }
	   //~ this.dtOptions['group']['columns'] =  [{ data: 'group.group_name' }, {'searchable': false,'orderable': false }],
	   //~ this.dtOptions['group']['order']=[[ 0, "asc" ]]
       //~ 
       //~ //Tecaher Assign courses
       //~ this.dtOptions['courseAssign'] = JSON.parse(JSON.stringify(options));
       //~ this.dtOptions['courseAssign']['language']['searchPlaceholder']='Search by Course Name';
	   //~ this.dtOptions['courseAssign']['ajax'] =  (dataTablesParameters: any, callback) => {
			//~ this.UserService.getAssignedCourses(dataTablesParameters,this.userId).subscribe(resp => {
				//~ this.assignedCoursesList = resp.data;
				//~ callback({
				  //~ recordsTotal: resp.total,
				  //~ recordsFiltered: resp.total,
				  //~ data: []
				//~ });
			//~ });
		//~ }	
	   //~ this.dtOptions['courseAssign']['columns'] =  [{ data: 'course.course_title' }, { data: 'created_at' }, { data: 'completed' }, {'searchable': false,'orderable': false }],
	   //~ this.dtOptions['courseAssign']['order'] =  [[ 0, "asc" ]]
	   //~ 
       //~ //Student Enrolled courses
       //~ this.dtOptions['courseEnroll'] = JSON.parse(JSON.stringify(options));
       //~ this.dtOptions['courseEnroll']['language']['searchPlaceholder']='Search by Course Name';
	   //~ this.dtOptions['courseEnroll']['ajax'] =  (dataTablesParameters: any, callback) => {
			//~ this.UserService.getEnrolledCourses(dataTablesParameters,this.userId).subscribe(resp => {
				//~ this.enrolledCoursesList = resp.data;
				//~ callback({
				  //~ recordsTotal: resp.total,
				  //~ recordsFiltered: resp.total,
				  //~ data: []
				//~ });
			//~ });
		//~ }	
	   //~ this.dtOptions['courseEnroll']['columns'] =  [{ data: 'course.course_title' }, { data: 'created_at' }, { data: 'completed' }, {'searchable': false,'orderable': false }],
	   //~ this.dtOptions['courseEnroll']['order'] =  [[ 0, "asc" ]]
       
       //~ this.dtOptions['quizzes'] = JSON.parse(JSON.stringify(options));
       //~ this.dtOptions['quizzes']['language']['searchPlaceholder']='Search by Test Name';
       //~ this.dtOptions['quizzes']['columns'] =  [{'searchable': false,'orderable': false  }, { 'searchable': false,'orderable': false }, { 'searchable': false,'orderable': false  }, {'searchable': false,'orderable': false }],
	   //~ this.dtOptions['quizzes']['order'] =  [[ 0, "asc" ]]
       //~ 
       //~ this.dtOptions['assignments'] = JSON.parse(JSON.stringify(options));
       //~ this.dtOptions['assignments']['language']['searchPlaceholder']='Search by Assignment Name';
       
       //~ this.dtOptions['liveclass'] = JSON.parse(JSON.stringify(options));
       //~ this.dtOptions['liveclass']['language']['searchPlaceholder']='Search by Live Classes';
       //~ this.dtOptions['liveclass']['ajax'] =  (dataTablesParameters: any, callback) => {
				//~ this.UserService.getAssignedLiveClasses(dataTablesParameters,this.userId).subscribe(resp => {
					//~ this.liveClassesList = resp.data;
					//~ callback({
					  //~ recordsTotal: resp.total,
					  //~ recordsFiltered: resp.total,
					  //~ data: []
					//~ });
				//~ });
			//~ }
	   //~ this.dtOptions['liveclass']['columns'] =  [{ data: 'liveclass.title' }, { data: 'liveclass.start_time' }, { data: 'id' }, {data: 'is_attended'}]
	   //~ this.dtOptions['liveclass']['order'] = [[ 0, "asc" ]]
       
    
    
}
  
    ngDoCheck(){}
	
	ngAfterViewInit() {
		this.initDatePicker();
	  }  
  
  initDatePicker(){
		setTimeout(_=>{
			// giving datepicker not a function error, so delaying it a bit
			if(this.dob && this.model.dob){
				this.dob_instance = jQuery(this.dob.nativeElement).datepicker(this.dobConfiguration);
				this.dob_instance.value(this.model.dob);
			}else if(this.dob && !this.model.dob){
				this.dob_instance = jQuery(this.dob.nativeElement).datepicker(this.dobConfiguration);	
			}
		},100)
  }
  
  ngOnDestroy() {
    if(typeof this.dob_instance != 'undefined') this.dob_instance.destroy();
  }
  getWidthPercent(percent){
    return this.sanitizer.bypassSecurityTrustStyle('width: '+percent+'%');
  }
	isClassLive(liveclass:any){
		//console.log(MomentModule);
		//return MomentModule().isBetween(MomentModule(liveclass.start_time),MomentModule(liveclass.start_time).add(liveclass.duration,'minutes'));
	}
	isClassPending(liveclass:any){
		//return MomentModule().isBefore(MomentModule(liveclass.start_time));
	}
	isClassComplete(liveclass:any){
		//return MomentModule(liveclass.start_time).add(liveclass.duration,'minutes').isBefore(MomentModule());
	}
  loginAs(){
    if(this.selectedUser <= 0) return;
    
    let userId=this.selectedUser;
    this.UserService.loginAs(userId)
    .subscribe(
      resp => {
        if(resp.status){
          localStorage.setItem("token",resp.data.token);
          localStorage.setItem("user_data",JSON.stringify(resp.data.user));
          localStorage.setItem("permissions",JSON.stringify(resp.data.permissions));
          this.UserService.updateCurrentUser(resp.data.user);
          this.router.navigate(['/']);
        }else{

        }
      },
      error => {

      }
    );
  }
	editInstituteUser(userId:number) {
		this.UserService.editInstituteUser(userId)
		.subscribe(
				data => { 			
					this.model = data['getInstituteUser'];
					this.teacherCoursesCounts = data['assignCoursesCounts'];
					this.studentCoursesCounts = data['enrollCoursesCounts'];
					this.groupsCounts = data['assignGroupsCounts'];
					this.initDatePicker();
				},
				error => {

				}
			);
    }
    
    getUserType(){
		this.UserService.getUserType()
				.subscribe(
					data => { 
						this.userTypeList = data;
						
					},
					error => {

					}
				);   

	}
	
	
	reloadDatatablesData(){
	  this.dtElements.forEach((dtElement: DataTableDirective) => {
      //@ts-ignore
      if(!$(dtElement.el.nativeElement).is(':visible')) return;
			dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
				dtInstance.ajax.reload();
			});
		});
	}
	
	//for both course and group list for assign group and enroll course to user
	getCoursesList(){
		this.UserService.getCourseList()
			.subscribe(
				data => {  
					this.courseslist = data['getCourseList'];
					this.groupslist = data['getGroupList'];
				},
				error => {

				}
			);
	
	}
	showAssignedGroups(){
		this.selectedGroups={};
		//~ this.assignedGroupsList.forEach((group)=>{
			//~ this.selectedGroups[group.group_id]=true;
		//~ });
		//~ this.loadTable['groups']=true;
		this.message['assignGroup'] = '';
		this.assignGroupModal.show();
	}
	showEnrolledCourses(){
		this.selectedCourses={}
		//~ this.assignedCoursesList.forEach((course)=>{
			//~ this.selectedCourses[course.course_id]=true;
		//~ });
		 this.message['assignCourse'] = '';
		this.enrollCourseModal.show();
	}
	
	deleteEnrolledCourse(course:any,index:number){
		this.assignedCourse = course;
		this.deleteEnrollCourseModal.show();
	}
	
	deleteAssignedGroup(group:any){
		this.assignedGroup = group;
		this.deleteAssignGroupModal.show();
	}
	
	removeEnrolledCourse(){
		if(this.model.user.user_type.name =='student'){
			this.UserService.removeEnrolledCourse(this.assignedCourse.id)
				.subscribe(
							data => {
								this.message['removeAssignCourse'] = data.message;
								this.reloadDatatablesData();
								this.editInstituteUser(this.userId);
								this.deleteEnrollCourseModal.hide();
								//~ setTimeout(()=>{
									  //~ this.message['removeAssignCourse']='';
									  //~ this.deleteEnrollCourseModal.hide();
								 //~ }, 1000)
							},
							error => {

							}
						); 
		}else{
			this.UserService.removeAssignedCourse(this.assignedCourse.id)
				.subscribe(
							data => {
								this.message['removeAssignCourse'] = data.message;
								this.reloadDatatablesData();
								this.editInstituteUser(this.userId);
								this.deleteEnrollCourseModal.hide();
								//~ setTimeout(()=>{
									  //~ this.message['removeAssignCourse']='';
									  //~ this.deleteEnrollCourseModal.hide();
								 //~ }, 1000)
							},
							error => {

							}
						);
		}
	}
	
	removeAssignedGroup(){
		this.UserService.removeAssignedGroup(this.assignedGroup.id)
			.subscribe(
						data => {
							this.message['removeAssignGroup'] = data.message;
							this.reloadDatatablesData();
							this.editInstituteUser(this.userId);
							this.deleteAssignGroupModal.hide();
							//~ setTimeout(()=>{
								  //~ this.message['removeAssignGroup']='';
								  //~ this.deleteAssignGroupModal.hide();
							 //~ }, 1000)
						},
						error => {

						}
					); 
	}
	
	saveEnrollCourses(){
		let enrollCourses=[];
		let that=this;
		Object.keys(that.selectedCourses).forEach(function(key){
			if(that.selectedCourses[key]) enrollCourses.push(key);
		})
		if(enrollCourses.length <=0) return;
    this.UserService.saveEnrollCourses(this.userId,enrollCourses.join(','))
      .subscribe(
        data => {
          this.reloadDatatablesData();
          this.editInstituteUser(this.userId);
          this.message['assignCourse']=data.message;
          setTimeout(()=>{
              this.enrollCourseModal.hide();
              this.message['assignCourse'] = '';
           }, 1000)
        },
        error => {

        }
      );
		/*if(this.model.user.user_type.name =='student'){
			this.UserService.saveEnrollCourses(this.userId,enrollCourses.join(','))
					.subscribe(
						data => {
							this.reloadDatatablesData();
							this.editInstituteUser(this.userId);
							this.message['assignCourse']=data.message;
							setTimeout(()=>{
								  this.enrollCourseModal.hide();
								  this.message['assignCourse'] = '';
							 }, 1000)
						},
						error => {

						}
					);
		}else{
			this.UserService.saveAssignCourses(this.userId,enrollCourses.join(','))
					.subscribe(
						data => {
							this.reloadDatatablesData();
							this.editInstituteUser(this.userId);
							this.message['assignCourse']=data.message;
							setTimeout(()=>{
								  this.enrollCourseModal.hide();
								  this.message['assignCourse'] = '';
							 }, 1000)
						},
						error => {

						}
					);
		} */ 
	}
	
	saveAssignGroups(){
		let assignGroups=[];
		let that=this;
		Object.keys(that.selectedGroups).forEach(function(key){
			if(that.selectedGroups[key]) assignGroups.push(key);
		})
		if(assignGroups.length <=0) return;
		
		this.UserService.saveAssignGroups(this.userId,assignGroups.join(','))
				.subscribe(
					data => {
						this.reloadDatatablesData();
						this.editInstituteUser(this.userId);
						this.message['assignGroup']=data.message;
						setTimeout(()=>{
							  this.assignGroupModal.hide();
							  this.message['assignGroup'] = '';
						 }, 1000)
					},
					error => {
						this.message['assignGroup']=error.message;
					}
				);  
	}
	sendResetInstruction(user){
    this.UserService.sendResetInstruction(user)
        .subscribe(
          data => {
          },
          error => {
          }
        );
  }
	savePersonalInstituteUser(){
    this.operation={lifecycle: 'running',status: false,message: ''};
  	//~ this.model.user_type_id = $('#user_type :selected').attr('id');
		this.UserService.savePersonalInstituteUser(this.model)
				.subscribe(
					data => {
						if(data['code'] == 200){
							this.router.navigate(['user-lists']);
						  this.operation={lifecycle: 'stopped',status: true,message: 'Profile saved successfully'};
            }else{
              this.operation={lifecycle: 'stopped',status: false,message: 'Problem updating profile'};
            }
					},
					error => {
            this.operation={lifecycle: 'stopped',status: false,message: error.message};
					}
				);   
	}
	
 }

     
 
 
