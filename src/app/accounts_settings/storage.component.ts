import { Component, OnInit, DoCheck, Inject, ViewChild } from '@angular/core';
import {FileLibraryService} from '../_services/file-library.service';
import {Params, Router} from '@angular/router';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
   //moduleId: module.id,
  templateUrl: './storage.component.html'
})

export class StorageComponent implements OnInit,DoCheck {
	@ViewChild('assignSpaceModal', { static: true }) assignSpaceModal: ModalDirective;
  
  operation={lifecycle: 'started',status: false,message: ''};
  teachers=[];
  usedSpace=0;
  totalAssignedSpace=0;
  totalSpace=0;
  remainingSpace = 0;
  instituteStoragePercent = 0;
  selectedTeacher={teacher:{storage:0}};
  constructor(private FileLibrary: FileLibraryService, private router: Router,private sanitized: DomSanitizer) { }
	
  ngOnInit() {
	   this.getStorage();
  }
  
  ngDoCheck(){
    
	}
  showModal(detail){
    this.operation={lifecycle: 'started',status: false,message: ''};
    this.selectedTeacher=detail;
    this.selectedTeacher['existing_storage']=detail.teacher.storage;
    this.assignSpaceModal.show();
  }
  getWidthPercent(percent){
    return this.sanitized.bypassSecurityTrustStyle('width: '+percent+'%');
  }
	getStorage(){
    this.totalAssignedSpace=0;
    this.FileLibrary.getAllSpaceUsage().subscribe(
      resp => {
        this.teachers=resp.data.teachers;
        this.teachers.forEach(detail=>{
          this.totalAssignedSpace +=detail.teacher.storage;
        })
        //console.log(this.totalAssignedSpace);
        this.usedSpace=resp.data.used_space;
        this.totalSpace=resp.data.total_space;
        this.instituteStoragePercent = ((this.usedSpace*100)/this.totalSpace);
        //console.log(this.totalSpace);
        this.remainingSpace = this.totalSpace - this.totalAssignedSpace;
        //console.log(this.remainingSpace);
      }
      );
  }
  updateStorage(e){
    this.selectedTeacher.teacher.storage=parseInt(e.target.value);
  }
  saveStorage(teacher){
    this.operation={lifecycle: 'running',status: false,message: ''};
  	this.FileLibrary.saveStorage(teacher.user_id,teacher.storage)
		 .subscribe(
				data => {
          this.getStorage();
					this.operation={lifecycle: 'stopped',status: true,message: 'Storage updated successfully'};
          setTimeout(()=>{
            this.assignSpaceModal.hide();
          },1500)
  			},
				error => {
          this.operation={lifecycle: 'stopped',status: false,message: 'Problem updating storage'};
          setTimeout(()=>{
            this.assignSpaceModal.hide();
          },1500)
				}
			);
	}
}

    

			

																																																																																																																								

