import { Component, OnInit, DoCheck, Inject} from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router} from '@angular/router';  




@Component({
   //moduleId: module.id,
  templateUrl: './interface.component.html'
})

export class InterfaceComponent implements OnInit,DoCheck {
  
  settings:any={class_language:'en-uk',other_language:'en-uk'};
  operation={lifecycle: 'started',status: false,message: ''};
  
  constructor(private UserService: UserService, private router: Router) { }
  
  ngOnInit() {
    this.getLoginSettings();
  }
  
  ngDoCheck(){
  }
    
  saveSettings(){
    this.operation={lifecycle: 'running',status: false,message: ''};
  
    this.UserService.saveSettings(this.settings)
     .subscribe(
        data => {
          this.operation={lifecycle: 'stopped',status: true,message: 'Settings saved successfully'};
        },
        error => {
          this.operation={lifecycle: 'stopped',status: false,message: 'Problem saved settings'};
        }
      );
  }
   
  getLoginSettings(){
    this.UserService.getLoginSettings()
      .subscribe(
        data => { 
          if(data['getLoginSetting']){
            this.settings.class_language=data['getLoginSetting']['class_language'];
            this.settings.other_language=data['getLoginSetting']['other_language'];
          }
        },
        error => {

        }
      );
    
  }
 }