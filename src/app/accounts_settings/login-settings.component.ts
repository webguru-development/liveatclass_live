import { Component, OnInit, DoCheck, Inject,ChangeDetectorRef } from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router} from '@angular/router';
import { AppComponent } from '../app.component';
import * as $ from "jquery";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {IMyDpOptions, IMyDateModel} from 'mydatepicker';	




@Component({
   //moduleId: module.id,
  templateUrl: './login-settings.component.html'
})

export class LoginSettingComponent implements OnInit,DoCheck {
	
  model:any={left_title:'',left_description:'',right_title:'',right_description:'',copyright_title:''};
  public file_srcs: any= '';
  banner_image:any= '';
  currentUser:any;
  operation={lifecycle: 'started',status: false,message: ''};
  
  constructor(private UserService: UserService, private router: Router,private appComponent: AppComponent,private changeDetectorRef:ChangeDetectorRef) { }
	
   ngOnInit() {
	   
       	this.currentUser= JSON.parse(localStorage.getItem("user_data"));
       	this.getLoginSettings()
       	
			
    }
  
    ngDoCheck(){
    
	}
	
	
	
	fileChange(input) {  
        this.banner_image=input.target.files[0];
        this.readFiles(input.target.files);      
    } 
	readFile(file, reader, callback) {  
        reader.onload = () => {  
            callback(reader.result);         
        }  
        reader.readAsDataURL(file);  
    }
    
     readFiles(files, index = 0) {  
        // Create the file reader  
        let reader = new FileReader();  
        // If there is a file  
        if (index in files) {  
            // Start reading this file  
            this.readFile(files[index], reader, (result) => {  
                // Create an img element and add the image file data to it  
                var img = document.createElement("img");  
                
                img.src = result; 
                this.file_srcs=(result);  
            });  
        } else {  
            // When all files are done This forces a change detection  
            this.changeDetectorRef.detectChanges();  
        }  
    }    
    
    resetSetting(){
      this.operation={lifecycle: 'running',status: false,message: ''};
  
      this.UserService.resetLoginSettings('setting')
     .subscribe(
        resp => {
          this.operation={lifecycle: 'stopped',status: true,message: 'Settings reset successfully'};
          if(resp['data']) this.model = resp['data'];
          if(resp['data'] && !!resp['data']['banner_image']){
            this.file_srcs=resp['data']['banner_image'];
          }
        },
        error => {
          this.operation={lifecycle: 'stopped',status: false,message: 'Problem resetting settings'};
        }
      );
    }
    saveLoginSettings(){
      this.operation={lifecycle: 'running',status: false,message: ''};
  
		this.UserService.saveLoginSettings(this.currentUser.id,this.banner_image, this.model.left_title,this.model.right_title,this.model.left_description,this.model.right_description, this.model.copyright_title, this.currentUser.institute_id)
		 .subscribe(
				data => {
					this.operation={lifecycle: 'stopped',status: true,message: 'Settings saved successfully'};
  			},
				error => {
          this.operation={lifecycle: 'stopped',status: false,message: 'Problem saved settings'};
				}
			);
	}
   
    getLoginSettings(){
		this.UserService.getLoginSettings()
			.subscribe(
				data => { 
					if(data['getLoginSetting']) this.model = data['getLoginSetting'];
          if(data['getLoginSetting'] && !!data['getLoginSetting']['banner_image']){
            this.file_srcs=data['getLoginSetting']['banner_image'];
          }
				},
				error => {

				}
			);
    
    }
    
	public Editor = ClassicEditor;
     
     
     
 
 }

    

			

																																																																																																																								

