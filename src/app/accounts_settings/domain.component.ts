import { Component, OnInit, DoCheck, Inject} from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router} from '@angular/router';  




@Component({
   //moduleId: module.id,
  templateUrl: './domain.component.html'
})

export class DomainComponent implements OnInit,DoCheck {
  
  settings:any={is_domain:false,domain:'',subdomain:''};
  operation={lifecycle: 'started',status: false,message: ''};
  
  constructor(private UserService: UserService, private router: Router) { }
  
  ngOnInit() {
    this.getInstitute();
  }
  
  ngDoCheck(){
  }
    
  saveSettings(){
    this.operation={lifecycle: 'running',status: false,message: ''};
  
    this.UserService.setInstitute(this.settings)
     .subscribe(
        data => {
          this.operation={lifecycle: 'stopped',status: true,message: 'Settings saved successfully'};
        },
        error => {
          this.operation={lifecycle: 'stopped',status: false,message: 'Problem saved settings'};
        }
      );
  }
   
  getInstitute(){
    this.UserService.getInstitute()
      .subscribe(
        data => { 
          if(data['institute']){
            this.settings.is_domain=!!data['institute']['is_domain'];
            this.settings.domain=data['institute']['domain'];
            this.settings.subdomain=data['institute']['subdomain'];
          }
        },
        error => {

        }
      );
    
  }
 }