import { Component, OnInit, DoCheck, ViewChild } from '@angular/core';
import { GroupService } from '../_services/group.service'; 
import { UserService } from '../_services/user.service'; 
import {Params, Router} from '@angular/router';
import { MomentModule } from 'angular2-moment';
import * as $ from "jquery";
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
    templateUrl: 'group.component.html'
})

export class GroupComponent implements OnInit, DoCheck {
  @ViewChild(DataTableDirective, {static: false}) dtElement: DataTableDirective;
  
  @ViewChild('deleteGroupModal', { static: true }) deleteGroupModal: ModalDirective;
  @ViewChild('addGroupModal', { static: true }) addGroupModal: ModalDirective;
  @ViewChild('addUserModal', { static: true }) addUserModal: ModalDirective;
  
  group:any={};
  operation={lifecycle: 'started',status: false,message: ''};
  groups=[];
  users=[];
  selectedUsers={};
  dtOptions: DataTables.Settings = {};
  constructor(private GroupService: GroupService,private UserService: UserService) {}
  
  ngOnInit() {
    this.dtOptions['groups']={
      pagingType: 'simple_numbers',
      pageLength: 10,
      dom: 'fltip',
      serverSide: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search by Group Name"
      },
      order: [[0, 'asc']],
      columns: [
        {"name": 'group_name',"orderable": true},
        {"name": 'description',"orderable": false},
        {"name": 'id',"orderable": false},
      ],
      ajax: (dataTablesParameters: any, callback) => {
        this.GroupService.getGroups(dataTablesParameters).subscribe(resp => {
          this.groups = resp.data;
          callback({
            recordsTotal: resp.total,
            recordsFiltered: resp.total,
            data: []
          });
        });
      }
    };
    this.dtOptions['users']={
      pagingType: 'simple_numbers',
      pageLength: 10,
      dom: 'fltip',
      serverSide: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search by User Name"
      },
      order: [[0, 'asc']],
      columns: [
        {"name": 'firstname'},
        {"name": 'email'}
      ],
      ajax: (dataTablesParameters: any, callback) => {
        this.UserService.getAllUsers(dataTablesParameters).subscribe(resp => {
          this.users = resp.user_data.data;
          callback({
            recordsTotal: resp.user_data.total,
            recordsFiltered: resp.user_data.total,
            data: []
          });
        }, error =>{
          //ToDo: do something on error
        });
      }
    };
  }
  reloadGroups(){
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {dtInstance.ajax.reload();});
  }
  addGroup(){
    this.operation={lifecycle: 'running',status: false,message: ''};
    this.GroupService.addGroup(this.group).subscribe(resp=>{
      //@ts-ignore
      this.operation={lifecycle: 'stopped',status: resp.status,message: resp.message.join('<br>')};
      this.reloadGroups();
      //@ts-ignore
      setTimeout(_=>{
        this.addGroupModal.hide();
      },1000)
    },resp=>{
      let msg=resp.message;
      if(resp.error.errors){
        msg=Object.values(resp.error.errors).join('<br>');
      }else if(resp.error.message){
        msg=resp.error.message;
      }
      this.operation={lifecycle: 'stopped',status: false,message: msg};
    });
  }
  showAddUserModal(group){
    this.operation={lifecycle: 'started',status: false,message: ''};
    this.selectedUsers={};
    this.group=group;
    this.addUserModal.show()
  }
  showDeleteGroupModal(group){
    this.group=group;
    this.operation={lifecycle: 'started',status: false,message: ''};
    this.deleteGroupModal.show();
  }
  assignUsersToGroup(){
    this.operation={lifecycle: 'running',status: false,message: ''};
    
    let userIds=[];
    Object.keys(this.selectedUsers).forEach(key=>{
      if(this.selectedUsers[key]) userIds.push(key);
    });
    
    if(userIds.length <= 0) return;
    
    this.GroupService.assignToGroup(this.group.id,userIds).subscribe(resp=>{
      //@ts-ignore
      this.operation={lifecycle: 'stopped',status: resp.status,message: resp.message.join('<br>')};
      //@ts-ignore
      setTimeout(_=>{
        this.addUserModal.hide();
      },1000)
    },resp=>{
      let msg=resp.message;
      if(resp.error.errors){
        msg=Object.values(resp.error.errors).join('<br>');
      }else if(resp.error.message){
        msg=resp.error.message;
      }
      this.operation={lifecycle: 'stopped',status: false,message: msg};
    });
  }
  deleteSelected(){
    this.GroupService.deleteGroup(this.group.id).subscribe(_=>{
      this.reloadGroups();
      this.deleteGroupModal.hide();
      this.group={};
    },error=>{
      // ToDo: do something with error
    });
  }
  ngDoCheck(){
  }
}
