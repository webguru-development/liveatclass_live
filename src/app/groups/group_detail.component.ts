import { Component, OnInit, QueryList, ViewChild, ViewChildren, Pipe } from '@angular/core';
import { UserService } from '../_services/user.service'; 
import { GroupService } from '../_services/group.service'; 
import { Group } from '../_models/group'; 
import { Params, Router , ActivatedRoute } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
    templateUrl: 'group_detail.component.html'
})

export class GroupDetailComponent implements OnInit {
  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;
  
  @ViewChild('deleteUserModal', { static: true }) deleteUserModal: ModalDirective;
  @ViewChild('deleteCourseModal', { static: true }) deleteCourseModal: ModalDirective;
  @ViewChild('deleteClassModal', { static: true }) deleteClassModal: ModalDirective;
  @ViewChild('assignUserModal', { static: true }) assignUserModal: ModalDirective;
  @ViewChild('assignCourseModal', { static: true }) assignCourseModal: ModalDirective;
  @ViewChild('assignClassModal', { static: true }) assignClassModal: ModalDirective;
  
  currentTab:string='details';
  groupId=0;
  group:Group= new Group();
  
  totalUsers=0;
  totalCourses=0;
  totalClasses= 0;

  assignedUsers=[];
  assignedCourses=[];
  assignedClasses=[];

  users=[];
  courses=[];
  classes=[];
  
  selectedUsers={};
  selectedCourses={};
  selectedClasses={};

  operation={lifecycle: 'started',status: false,message: ''};
  loadTable={};
  dtOptions={};
  tempRec={};
  
  constructor(private GroupService: GroupService,private UserService: UserService,private router: Router,private route: ActivatedRoute) {}
  
  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.groupId=params['id'];
      this.getGroupDetail();  
      let options={
        pagingType: 'simple_numbers',
        pageLength: 10,
        dom: 'fltip',
        serverSide: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search by Name"
        },
        order: [[0, 'asc']],
      };
      let detailTypes={
        assignedUsers: {
          placeholder: 'Search by User Name',
          func: 'getUsers',
          classParam: 'assignedUsers',
          service: 'GroupService',
          columns: [
            {"name": 'firstname',"orderable": true},
            {"name": 'email',"orderable": true},
            {"name": 'id',"orderable": false},
          ]
        },
        assignedCourses: {
          placeholder: 'Search by Course Title',
          func: 'getCourses',
          classParam: 'assignedCourses',
          service: 'GroupService',
          columns: [
            {"name": 'id',"orderable": false},
            {"name": 'course_title',"orderable": true},
            {"name": 'teachers',"orderable": false},
            {"name": 'id',"orderable": false}
          ]
        },
        assignedClasses: {
          placeholder: 'Search by Live Class',
          func: 'getClasses',
          classParam: 'assignedClasses', 
          service: 'GroupService',
          columns: [
            {"name": 'title',"orderable": true},
            {"name": 'created_at',"orderable": true},
            {"name": 'firstname',"orderable": true},
            {"name": 'id',"orderable": false},
            {"name": 'id',"orderable": false},
          ]
        },
        users: {
          placeholder: 'Search by Name',
          func: 'getAllUsers',
          classParam: 'users', 
          service: 'UserService',
          columns: [
            {"name": 'firstname',"orderable": true},
            {"name": 'id',"orderable": false},
          ]
        },
        courses: {
          placeholder: 'Search by Title',
          func: 'getMyCourseList',
          classParam: 'courses', 
          service: 'UserService',
          columns: [
            {"name": 'course_title',"orderable": true},
            {"name": 'id',"orderable": false},
          ]
        },
        classes: {
          placeholder: 'Search by Class Name',
          func: 'getMyClassList',
          classParam: 'classes', 
          service: 'UserService',
          columns: [
            {"name": 'title',"orderable": true},
            {"name": 'id',"orderable": false},
          ]
        }
      };
      Object.keys(detailTypes).forEach(name=>{
        this.dtOptions[name] = JSON.parse(JSON.stringify(options));
        this.dtOptions[name]['language']['searchPlaceholder']=detailTypes[name]['placeholder'];
        this.dtOptions[name]['columns']=detailTypes[name]['columns'];
        this.dtOptions[name]['ajax'] =  (dataTablesParameters: any, callback) => {
          this[detailTypes[name]['service']][ detailTypes[name]['func'] ](dataTablesParameters,this.groupId).subscribe(resp => {
            this[detailTypes[name]['classParam']] = resp.data;
            callback({
              recordsTotal: resp.total,
              recordsFiltered: resp.total,
              data: []
            });
          });
        }
      });
    });
  }
  getGroupDetail(){
    this.GroupService.getDetail(this.groupId).subscribe(resp => {
      this.group=resp.data.group;
      this.totalUsers=resp.data.totalUsers;
      this.totalCourses=resp.data.totalCourses;
      this.totalClasses=resp.data.totalClasses;
    },error=>{
      //ToDo: do something with error;
    });
  }
  reloadTables(){
    this.dtElements.forEach((dtElement: DataTableDirective) => {
      //@ts-ignore
      if(!$(dtElement.el.nativeElement).is(':visible')) return;
      dtElement.dtInstance.then((dtInstance: DataTables.Api) => {dtInstance.ajax.reload();});
    });
  }
  saveDetail(){
    this.operation={lifecycle: 'running',status: false,message: ''};
    this.GroupService.updateGroup(this.group).subscribe(resp => {
      //@ts-ignore
      this.operation={lifecycle: 'stopped',status: resp.status,message: resp.message.join('<br>')};
      //@ts-ignore
      setTimeout(_=>{
        this.router.navigate(['/groups']);
      },1000);
    },error=>{
      //@ts-ignore
      this.operation={lifecycle: 'stopped',status: resp.status,message: error.message.join('<br>')};
    });
  }
  assignUsersToGroup(){
    this.operation={lifecycle: 'running',status: false,message: ''};
    
    let userIds=[];
    Object.keys(this.selectedUsers).forEach(key=>{
      if(this.selectedUsers[key]) userIds.push(key);
    });
    
    if(userIds.length <= 0) return;
    
    this.GroupService.assignToGroup(this.group.id,userIds).subscribe(resp=>{
      //@ts-ignore
      this.operation={lifecycle: 'stopped',status: resp.status,message: resp.message.join('<br>')};
      //@ts-ignore
      setTimeout(_=>{
        this.operation={lifecycle: 'started',status: false,message: ''};
        this.assignUserModal.hide();
        this.reloadTables();
      },1000)
    },error=>{
      this.operation={lifecycle: 'stopped',status: false,message: error.message};
    });
  }
  assignCoursesToGroup(){
    this.operation={lifecycle: 'running',status: false,message: ''};
    
    let courseIds=[];
    Object.keys(this.selectedCourses).forEach(key=>{
      if(this.selectedCourses[key]) courseIds.push(key);
    });
    
    if(courseIds.length <= 0) return;
    
    this.GroupService.assignCoursesToGroup(this.group.id,courseIds).subscribe(resp=>{
      //@ts-ignore
      this.operation={lifecycle: 'stopped',status: resp.status,message: resp.message.join('<br>')};
      //@ts-ignore
      setTimeout(_=>{
        this.operation={lifecycle: 'started',status: false,message: ''};
        this.assignCourseModal.hide();
        this.reloadTables();
      },1000)
    },error=>{
      this.operation={lifecycle: 'stopped',status: false,message: error.message};
    });
  }
  assignClassesToGroup(){
    this.operation={lifecycle: 'running',status: false,message: ''};
    
    let classIds=[];
    Object.keys(this.selectedClasses).forEach(key=>{
      if(this.selectedClasses[key]) classIds.push(key);
    });
    
    if(classIds.length <= 0) return;
    
    this.GroupService.assignClassesToGroup(this.group.id,classIds).subscribe(resp=>{
      //@ts-ignore
      this.operation={lifecycle: 'stopped',status: resp.status,message: resp.message.join('<br>')};
      //@ts-ignore
      setTimeout(_=>{
        this.operation={lifecycle: 'started',status: false,message: ''};
        this.assignClassModal.hide();
        this.reloadTables();
      },1000)
    },error=>{
      this.operation={lifecycle: 'stopped',status: false,message: error.message};
    });
  }
  deleteUser(assignedUser){
    this.GroupService.deleteUser(assignedUser.id,this.groupId).subscribe(resp => {
      // Reload tables
      this.reloadTables();
    },error=>{
      //ToDo: do something with error
    });
  }
  deleteCourse(assignedCourse){
    this.GroupService.deleteCourse(assignedCourse.id,this.groupId).subscribe(resp => {
      // Reload tables
      this.reloadTables();
    },error=>{
      //ToDo: do something with error
    });
  }
  deleteClass(assignedClass){
    this.GroupService.deleteClass(assignedClass.id,this.groupId).subscribe(resp => {
      // Reload tables
      this.reloadTables();
    },error=>{
      //ToDo: do something with error
    });
  }

}
