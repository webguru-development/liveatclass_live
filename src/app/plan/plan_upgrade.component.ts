import { Component, OnInit, DoCheck, Inject, ViewChild, OnDestroy } from '@angular/core';
import {Params, Router,ActivatedRoute} from '@angular/router';
import {UserService} from '../_services/user.service';
import {ModalDirective} from 'angular-bootstrap-md';
import { OwlOptions } from 'ngx-owl-carousel-o';
@Component({
   //moduleId: module.id,
  templateUrl: './plan_upgrade.component.html'
})

export class PlanUpgradeComponent implements OnInit,DoCheck {
  @ViewChild('enquireModal', { static: true }) enquireModal: ModalDirective;
  operation={lifecycle: 'started',status: false,message: ''};
  plans:any;
  countries:any=[{phone_code: '+91',name: 'India'}];
  selectedPlan={};
  enrollCourse={firstname: '',lastname: '',email: '',phone_code: '+91',phone: '',message: ''};
  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 2
      },
      940: {
        items: 3
      }
    },
    nav: true
  };
  constructor(private router: Router,private UserService: UserService,private route: ActivatedRoute) {       
  }

  ngOnInit() {
    this.UserService.countries().subscribe(
      data => {  
        this.countries=data;
      },
      error =>{
      }
    );
    this.UserService.plans().subscribe(
      data => {  
        this.plans=data;
      },
      error =>{
      }
    );

    this.UserService.getPlanInfo().subscribe(resp=>{
      this.selectedPlan=resp.id;
    },err=>{
      //ToDo: do something with error      
    });
  }
  enquireRequest(form){
    this.operation={lifecycle: 'running',status: false,message: ''};
    this.UserService.enquirePlanRequest(this.enrollCourse,this.selectedPlan).subscribe(resp=>{
      form.submitted=false;
      this.enrollCourse={firstname: '',lastname: '',email: '',phone_code: '+91',phone: '',message: ''};
      this.enquireModal.hide();
      this.operation={lifecycle: 'stopped',status: true,message: 'Request sent successfully'};
      setTimeout(_=>{
        this.operation={lifecycle: 'started',status: false,message: ''};
      },2000);
    },
    error => {
      this.operation={lifecycle: 'stopped',status: false,message: error.message};
      setTimeout(_=>{
        this.operation={lifecycle: 'started',status: false,message: ''};
      },2000);
    });
  }
  ngDoCheck(){
    
  }
  
 }