import { Component, OnInit, DoCheck, Inject, ViewChild, OnDestroy } from '@angular/core';
import {Params, Router,ActivatedRoute} from '@angular/router';
import {UserService} from '../_services/user.service';
import {FileLibraryService} from '../_services/file-library.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
   //moduleId: module.id,
  templateUrl: './plan_detail.component.html'
})

export class PlanDetailComponent implements OnInit,DoCheck {
  plan = {name: '',no_of_users: 0,total_space: 0,max_class_duration:0,no_of_users_per_class:0,no_of_concurrent_classes:0,white_labeling: false, validity_of_plan: 1};
  plans:any = [];
  institute={active_users: 0, used_space: 0, created_at: new Date()};
  constructor(private sanitized: DomSanitizer, private router: Router,private UserService: UserService, private FileLibrary: FileLibraryService,private route: ActivatedRoute) {       
  } 

  ngOnInit() {
    this.UserService.getLoginSettings()
    .subscribe(
      data => { 
        this.institute.created_at = data.getLoginSetting.institute.created_at; 
      },
      error => {

      }
    );
    this.UserService.siteStats()
    .subscribe(
      data => { 
        this.institute.active_users = data.total_users; 
      },
      error => {

      }
    );
    this.FileLibrary.getAllSpaceUsage().subscribe(
      resp => {
        this.institute.used_space=resp.data.used_space;
      }
      );
    this.UserService.getPlanInfo().subscribe(resp=>{
      this.plan=resp;
    },err=>{
      //ToDo: do something with error      
    });
    this.UserService.plans().subscribe(
      data => {  
        this.plans=data;
      },
      error =>{
      }
    );

  }
  getWidthPercent(percent){
    return this.sanitized.bypassSecurityTrustStyle('width: '+percent.toFixed(2)+'%');
  }
  ngDoCheck(){
    
  }
  
 }