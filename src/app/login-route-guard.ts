import { CanActivate,Router,ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class LoginRouteGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate() {
  if(localStorage.getItem("token")!=undefined)
  {
   return true;
  }
  else 
  {
  this.router.navigate(['/']);
  return false;
  }
  }
  
}
