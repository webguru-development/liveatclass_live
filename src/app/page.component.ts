import { Component, OnInit, OnDestroy } from '@angular/core';
import {CmsPageService} from './_services/cms-page.service';
import {Params, ActivatedRoute} from '@angular/router';
import { Title, Meta }     from '@angular/platform-browser';

@Component({
    templateUrl: 'page.component.html'
})
export class PageComponent{
  operation={lifecycle: 'started',status: false,message: ''};
  page;
  constructor(private meta: Meta, private titleService: Title, private CmsPageService: CmsPageService,private route: ActivatedRoute) {       
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.loadPage(params.slug);
    });
  }
  loadPage(slug){
    let urlSlug=slug.replace(/^\/|\/$/g, '');
    this.operation={lifecycle: 'running',status: false,message: ''};
    this.CmsPageService.getPageBySlug(urlSlug).subscribe(resp=>{
      this.page=resp.data;
      
      this.titleService.setTitle(this.page.title+' | Live @ Class');
      this.meta.addTag({ name: 'description', content: this.page.meta_description });
      this.meta.addTag({ name: 'keywords', content: this.page.meta_keywords });

      this.operation={lifecycle: 'stopped',status: true,message: ''};
    },error=>{
      this.operation={lifecycle: 'stopped',status: false,message: ''};
    });
  }
}