import { Component, OnInit, DoCheck, OnDestroy, QueryList, ViewChild, ElementRef, ViewChildren, Pipe } from '@angular/core';
import {FileLibraryService} from '../_services/file-library.service'; 
import {User} from '../_models/user';
import {Params, Router} from '@angular/router';
import { MomentModule } from 'angular2-moment';
import * as $ from "jquery";
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'angular-bootstrap-md';
import 'jstree';
import * as fileSaver from 'file-saver';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import {HttpEventType, HttpResponse} from '@angular/common/http';

@Component({
  templateUrl: './certificate.component.html'
})
export class CertificateComponent implements OnInit  {
	@ViewChild('certificateMsg', { static: false }) certificateMsg: ModalDirective;
	operation={
    	'certificate_img': {lifecycle: 'started',status: false,message: '',progress: 0},
  	};
  	public certificate_img = '';
  	public certificate_img_upload = {name: ''};

	certificates = [];
	certificatesResult = false;	
	pageOfItems: Array<any>;
	pageSize = 6;
	error = false;
	message = '';
  	constructor(private FileLibraryService: FileLibraryService,private sanitized: DomSanitizer) { }
	ngOnInit() {
		this.getCertificates();
	}

	readFile(file, reader, callback) {  
		reader.onload = () => {  
		  callback(reader.result);         
		}  
		reader.readAsDataURL(file);  
  	}
    
	readFiles(files,logo) {
		let index=0;  
		// Create the file reader  
		let reader = new FileReader();  
		// If there is a file  
		if (index in files) {  
			// Start reading this file  
			this.readFile(files[index], reader, (result) => {  
			  // Create an img element and add the image file data to it  
			  var img = document.createElement("img");  
			  
			  img.src = result;
			  this[logo]=this.sanitized.bypassSecurityTrustUrl(result);  
			});  
		}  
	} 

	getCertificates(){
		this.FileLibraryService.getCertificates().subscribe(res => {
			//console.log(res.data)
			if(res.success){
				this.certificatesResult = true;
				this.certificates = res.data;
			}else{
				this.certificatesResult = false;
				this.certificates = [];
			}
		});
	}

	onChangePage(pageOfItems: Array<any>) {
        // update current page of items
        this.pageOfItems = pageOfItems;
    }

    fileChange(input,c_file) {  
    	this[c_file+'_upload']=input.target.files[0];
    	this.readFiles(input.target.files,c_file);      
  	}

  	saveCertificate(c_file){
  		if(this[c_file+'_upload'].name != ""){
  			this.FileLibraryService.saveLogoSettings(c_file,this[c_file+'_upload'])
	     	.subscribe(
		        event => {
					/*if(event.type === HttpEventType.UploadProgress){
						let percentDone = Math.round(100 * event.loaded / event.total);
						this.operation[logo]['progress']=percentDone;
					}else if(event instanceof HttpResponse){
						this.getLoginSettings();
						this.operation[logo]={lifecycle: 'stopped',status: true,message: 'Logo updated successfully',progress: 100};
						setTimeout(_=>{
							this.operation[logo]={lifecycle: 'started',status: false,message: '',progress: 0};
						},2000)
					}*/
		        },
		        error => {
					
		        }
	      	);
  		}else{
  			console.log('error');
  			this.error = true;
  			this.message = "Select an image first";
  			this.certificateMsg.show();
  		}
  	}
}
