import { Component, OnInit, DoCheck, Inject, ViewChild, OnDestroy } from '@angular/core';
import {FileLibraryService} from '../_services/file-library.service';
import {Params, Router,ActivatedRoute} from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';
import { DataTableDirective } from 'angular-datatables';
import * as moment from 'moment';
@Component({
  templateUrl: './recording.component.html'
})

export class RecordingComponent implements OnInit,DoCheck {
	@ViewChild(DataTableDirective, {static: false})
	private datatableElement: DataTableDirective;
	dtOptions: any = {};
	recordings=[];
 	constructor(private router: Router,private FileLibraryService: FileLibraryService,private route: ActivatedRoute) {    		
  }

  ngOnInit() {
		const that = this;
    this.dtOptions={
      pagingType: 'simple_numbers',
      pageLength: 10,
      dom: 'fltip',
      serverSide: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search by Class Name"
      },
      order: [[1, 'asc']],
      columns: [
        {"name": 'created_at',"orderable": true},
        {"name": 'description',"orderable": true},
        {"name": 'id',"orderable": false}
      ],
      ajax: (dataTablesParameters: any, callback) => {
        that.FileLibraryService.getMyRecordings(dataTablesParameters).subscribe(resp => {
          that.recordings = resp.data;
          callback({
            recordsTotal: resp.total,
            recordsFiltered: resp.total,
            data: []
          });
        });
      }
    };
	}
  
	ngDoCheck(){
		
	}
	
 }
