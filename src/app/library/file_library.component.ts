import { Component, OnInit, DoCheck, OnDestroy, QueryList, ViewChild, ElementRef, ViewChildren, Pipe } from '@angular/core';
import {FileLibraryService} from '../_services/file-library.service'; 
import {User} from '../_models/user';
import {Params, Router} from '@angular/router';
import { MomentModule } from 'angular2-moment';
import * as $ from "jquery";
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'angular-bootstrap-md';
import 'jstree';
import * as fileSaver from 'file-saver';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import {HttpEventType, HttpResponse} from '@angular/common/http';


@Component({
    templateUrl: 'file_library.component.html'
})

export class FileLibraryComponent implements OnInit, DoCheck {
	@ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;
	
	@ViewChild('deleteFileModal', { static: true }) deleteFileModal: ModalDirective;
	@ViewChild('addFolderModal', { static: true }) addFolderModal: ModalDirective;
	@ViewChild('addFileModal', { static: true }) addFileModal: ModalDirective;
	@ViewChild('addMultipleFileModal', { static: true }) addMultipleFileModal: ModalDirective;
	@ViewChild('addEmbedModal', { static: true }) addEmbedModal: ModalDirective;
	@ViewChild('folderModal', { static: true }) folderModal: ModalDirective;

	@ViewChild('folderTree', { static: true }) folderTree: ElementRef<any>;
	@ViewChild('maintainCopy', { static: true }) maintainCopy: ElementRef<any>;
	
  currentTab:string='files';
	showFileType:number=-1;
	currentFolderId=0;
	total_space=0;
	total_allocated=0;
	personal_space=0;
	personal_allocated=0;
	tempFile:any={};
	tempFiles=[];
	operation={lifecycle: 'started',status: false,message: '',progress: 0};
	folderList=[];
	folderTreeList=[];
	fileList=[];
	embedList=[];
	recordingList=[];
	templateList=[];
	selectedFiles={};
	selectedAll={files: false,embeds: false,recordings: false,templates: false};
	dtOptions: DataTables.Settings = {};
	currentUser:any={};
	isFile:boolean=true;
	isFolder:boolean=false;
	token='';
	personalStoragePercent = 0;
	instituteStoragePercent = 0;
	embededSuccessMsg = '';
	constructor(private FileLibraryService: FileLibraryService,private sanitized: DomSanitizer) {}
	
	ngOnInit() {
		this.currentUser=JSON.parse(localStorage.getItem('user_data'));
		this.token=localStorage.getItem('token');
		this.getAllFolders();
		this.getSpaceUsage();
		let options={
			pagingType: 'simple_numbers',
			pageLength: 10,
			dom: 'fltipr',
			processing: true,
			serverSide: true,
			language: {
				search: "_INPUT_",
				searchPlaceholder: "Search by Name"
			},
			order: [[1, 'asc']],
		};
		let fileTypes={
			files: {
				placeholder: 'Search by File / Folder name',
				func: 'getFiles',
				classParam: 'fileList',
				columns: [
					{"name": 'id',"orderable": false},
					{"name": 'name',"orderable": true},
					{"name": 'type',"orderable": true},
					{"name": 'user_id',"orderable": true},
					// {"name": 'is_shared',"orderable": true},
					{"name": 'id',"orderable": false},
					{"name": 'id_read',"orderable": false},
				]
			},
			embedded_content: {
				placeholder: 'Search by Title name',
				func: 'getEmbeds',
				classParam: 'embedList',
				columns: [
					{"name": 'id',"orderable": false},
					{"name": 'name',"orderable": true},
					{"name": 'contentType',"orderable": true},
					{"name": 'description',"orderable": false},
					{"name": 'id',"orderable": false}
				]
			},
			recording: {
				placeholder: 'Search by Recording name',
				func: 'getRecordings',
				classParam: 'recordingList', 
				columns: [
					{"name": 'id',"orderable": false},
					{"name": 'name',"orderable": true},
					{"name": 'description',"orderable": true},
					{"name": 'size',"orderable": true},
					{"name": 'id',"orderable": false},
				]
			},
			template: {
				placeholder: 'Search by Template name',
				func: 'getTemplates',
				classParam: 'templateList', 
				columns: [
					{"name": 'id',"orderable": false},
					{"name": 'name',"orderable": true},
					{"name": 'created_at',"orderable": true},
					{"name": 'id',"orderable": false}
				]
			}
		};
		Object.keys(fileTypes).forEach(name=>{
			this.dtOptions[name] = JSON.parse(JSON.stringify(options));
			this.dtOptions[name]['language']['searchPlaceholder']=fileTypes[name]['placeholder'];
			this.dtOptions[name]['columns']=fileTypes[name]['columns'];
			this.dtOptions[name]['ajax'] =  (dataTablesParameters: any, callback) => {
				this.FileLibraryService[ fileTypes[name]['func'] ](dataTablesParameters,this.currentFolderId,this.showFileType).subscribe(resp => {
					this[fileTypes[name]['classParam']] = resp.data;
					callback({
					  recordsTotal: resp.total,
					  recordsFiltered: resp.total,
					  data: []
					});
				});
			}
		});
	}
	amIOwner(file){
		return file.user_id == this.currentUser.id;
	}
	editableFolder(){
		return	this.folderList.length == 0 || 
						this.folderList[ (this.folderList.length - 1) ].is_write || 
						this.folderList[ (this.folderList.length - 1) ].user_id == this.currentUser.id;
	}
	canEdit(file){
		return ( (file.user_id == this.currentUser.id) || file.is_write);
	}
	canRead(file){
		return ( (file.user_id == this.currentUser.id) || file.is_read);
	}
	canDelete(file){
		return ( (file.user_id == this.currentUser.id) || file.is_delete);
	}
	bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Byte';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)).toFixed(2));
	   return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i];
	}
	reloadFiles(){
		// get file/folder list according to [currentFolderId] [showFileType]
	  this.dtElements.forEach((dtElement: DataTableDirective) => {
      //@ts-ignore
      if(!$(dtElement.el.nativeElement).is(':visible')) return;
      dtElement.dtInstance.then((dtInstance: DataTables.Api) => {dtInstance.ajax.reload();});
    });
	}
	reloadEmbeds(){
		this.reloadFiles();
	}
	reloadRecordings(){
		this.reloadFiles();
	}
	reloadTemplates(){
		this.reloadFiles();
	}
	traverseTo(folderId){
		this.selectedFiles={};
		this.currentFolderId=folderId;
		// remove folders from list until find selected folder
		for(let i=this.folderList.length;i>0;i--){
			if(this.folderList[ (i -1) ].id == folderId){
				break;
			}
			this.folderList.splice((i -1),1);
		}
		this.reloadFiles();
	}
	handleFileInput(files) {
	    this.tempFile.filename = files.item(0);
	    this.tempFile.name=this.tempFile.filename.name.split('.')[0];
	}
	updateFile(file,whichChanged){
		switch(whichChanged){
			case 'read':
				file.is_shared=file.is_read;
				file.is_write=file.is_write && file.is_read;
				file.is_delete=file.is_delete && file.is_read;
			break;
			case 'write':
				file.is_read=file.is_read || file.is_write;
				file.is_shared=file.is_read || file.is_write;
				file.is_delete=file.is_delete && file.is_write;
			break;
			case 'delete':
				file.is_read=file.is_read || file.is_delete;
				file.is_shared=file.is_read || file.is_delete;
				file.is_write=file.is_write || file.is_delete;
			break;
		}
		this.FileLibraryService.updateFile(file).subscribe(res=>{
			//ToDo: If we gonna show a message
		},error=>{
			// ToDo: do something with error
		});
	}
	getWidthPercent(percent){
		return this.sanitized.bypassSecurityTrustStyle('width: '+percent+'%');
	}
	getSpaceUsage(){
		this.FileLibraryService.getSpaceUsage().subscribe(res=>{
			this.total_space=res.data.total_size;
			this.total_allocated=res.data.total_allocated;
			this.personal_space=res.data.personal_size;
			this.personal_allocated=res.data.personal_allocated;
			this.personalStoragePercent = ((this.personal_space*100)/this.personal_allocated);
			this.instituteStoragePercent = ((this.total_space*100)/this.total_allocated);
		},error=>{
			// ToDo: do something with error
		});
	}
	getAllFolders(){
		this.FileLibraryService.getFolders().subscribe(res=>{
			this.folderTreeList=res.data;
		},error=>{
			// ToDo: do something with error
		});
	}
	addFiles(event){
		if(event.length <= 0) return;
		Array.from(event).forEach(e=>{
			let file={
				filename: e,
				//@ts-ignore
				name: e.name.split('.')[0],
				is_read: false,
				is_write: false,
				is_delete: false,

				operation: {lifecycle: 'running',status: false,message: '',progress: 0}
			};
			this.addFile(file,'bulkUpload');
			this.tempFiles.push(file);
		});
	}
	addFile(file,type){
		file.parent_id=this.currentFolderId;
		file.type=this.isFile;
		file.is_shared=file.is_read;
		file.operation={lifecycle: 'running',status: false,message: '',progress: 0};
		this.FileLibraryService.addFile(file,{reportProgress: true, observe: 'events' }).subscribe((event)=>{
			if (event.type === HttpEventType.UploadProgress) {
        let percentDone = Math.round(100 * event.loaded / event.total);
        file.operation['progress']=percentDone;
      } else if (event instanceof HttpResponse) {
        if(file.operation['progress'] != 100){
        	file.operation['progress']=100;
	      }
	      setTimeout(()=>{
        	//@ts-ignore
					file.operation={lifecycle: 'stopped',status: event.body.status,message: event.body.message.join('<br>'),progress: 100};
				},100);

        if(type=='file'){
					this.reloadFiles();
					//@ts-ignore
					window.setTimeout(()=>{
						this.addFileModal.hide();
					},1000);
					(<HTMLInputElement>document.getElementById('fileControl')).value=null;
				}else if(type=='embed'){
					this.embededSuccessMsg = event.body.message[0];
					this.reloadEmbeds();
					//@ts-ignore
					window.setTimeout(_=>{
						this.embededSuccessMsg = '';
						this.addEmbedModal.hide();
					},2000)
				}
      }
		},resp=>{
			let msg=resp.message;
			if(resp.error.errors){
				msg=Object.values(resp.error.errors).join('<br>');
			}else if(resp.error.message){
				msg=resp.error.message;
			}
			file.operation={lifecycle: 'stopped',status: false,message: msg,progress: 0};
		});
	}
	addFolder(file){
		file.parent_id=this.currentFolderId;
		file.type=this.isFolder;
		file.is_shared=file.is_read;
		this.operation={lifecycle: 'running',status: false,message: '',progress: 0};
		this.FileLibraryService.addFile(file,{}).subscribe(resp=>{
			//@ts-ignore
			this.operation={lifecycle: 'stopped',status: resp.status,message: resp.message.join('<br>'),progress: 0};
			this.getAllFolders();
			this.reloadFiles();
			//@ts-ignore
			window.setTimeout(_=>{
				this.addFolderModal.hide();
			},1000)
		},resp=>{
			let msg=resp.message;
			if(resp.error.errors){
				msg=Object.values(resp.error.errors).join('<br>');
			}else if(resp.error.message){
				msg=resp.error.message;
			}
			this.operation={lifecycle: 'stopped',status: false,message: msg,progress: 0};
		});
	}
	selectAllFiles(e){
		this.selectedFiles={};
		this.fileList.forEach(file=>{
			this.selectedFiles[file.id]=e.target.checked;
		});
	}
	selectAllEmbeds(e){
		this.selectedFiles={};
		this.embedList.forEach(file=>{
			this.selectedFiles[file.id]=e.target.checked;
		});	
	}
	selectAllRecordings(e){
		this.selectedFiles={};
		this.recordingList.forEach(file=>{
			this.selectedFiles[file.id]=e.target.checked;
		});	
	}
	selectAllTemplates(e){
		this.selectedFiles={};
		this.templateList.forEach(file=>{
			this.selectedFiles[file.id]=e.target.checked;
		});	
	}
	anySelected(){
		return Object.values(this.selectedFiles).indexOf(true) !== -1;
	}
	disableChildren(children, selectedFolders, disable){
		return children.map(child=>{ 
			child.state.disabled=disable ? disable : (selectedFolders.indexOf(child.id.toString()) !== -1);
			if(child.children.length){
				child.children=this.disableChildren(child.children,selectedFolders,child.state.disabled);
			}
			return child;
		})
	}
	showFolderModal(){
		let selectedFolder=[];
		Object.keys(this.selectedFiles).forEach(key=>{
			if(this.selectedFiles[key]) selectedFolder.push(key);
		});
		this.folderTreeList=this.disableChildren(this.folderTreeList, selectedFolder, false);
		
		let prevObj=$(this.folderTree.nativeElement).jstree(true);
		if(prevObj) prevObj.destroy();
		
		$(this.folderTree.nativeElement).jstree({'core': {'multiple': false,'data': [{'id': 'root','state':{'opened': true},'text': 'Root','children': this.folderTreeList}]}});
		
		this.operation={lifecycle: 'started',status: false,message: '',progress: 0};
		this.folderModal.show();
	}
	moveToFolder(){
		let folderId='0';
		let prevObj=$(this.folderTree.nativeElement).jstree(true);
		let keepCopy=$(this.maintainCopy.nativeElement).is(':checked');
		
		if(!prevObj && prevObj.get_selected(true).length <=0) return;
		
		folderId=prevObj.get_selected(true)[0].id;
		folderId=folderId=='root' ? '0' : folderId;
		
		let selectedFiles=[];
		Object.keys(this.selectedFiles).forEach(key=>{
			if(this.selectedFiles[key]) selectedFiles.push(key);
		});

		this.operation={lifecycle: 'running',status: false,message: '',progress: 0};
		this.FileLibraryService.moveToFolder(selectedFiles, folderId, keepCopy).subscribe(resp=>{
			//@ts-ignore
			this.operation={lifecycle: 'stopped',status: resp.status,message: resp.message.join('<br>'),progress: 0};
			this.selectedFiles={};
			this.resetSelectAll();
			this.getAllFolders();
			this.reloadFiles();
			//@ts-ignore
			setTimeout(_=>{
				this.folderModal.hide();	
			},1000)
		},resp=>{
			let msg=resp.message;
			if(resp.error.errors){
				msg=Object.values(resp.error.errors).join('<br>');
			}else if(resp.error.message){
				msg=resp.error.message;
			}
			this.operation={lifecycle: 'stopped',status: false,message: msg,progress: 0};
		})
	}
	resetSelectAll(){
		this.selectedAll={files: false,embeds: false,recordings: false,templates: false};
	}
	deleteSelected(){
		let fileIds=[];
		Object.keys(this.selectedFiles).forEach(key=>{
			if(this.selectedFiles[key]) fileIds.push(key);
		});
		this.FileLibraryService.deleteFiles(fileIds).subscribe(_=>{
			switch(this.currentTab){
				case 'files':
					this.getAllFolders();
					this.reloadFiles();
				break;
				case 'embed':
					this.reloadEmbeds();
				break;
				case 'recording':
					this.reloadRecordings();
				break;
				case 'template':
					this.reloadTemplates();
				break;
				
			}
			this.deleteFileModal.hide();
			this.resetSelectAll();
			this.selectedFiles={};
		},error=>{
			// ToDo: do something with error
		});
	}
	downloadFile(url, filename){
		this.FileLibraryService.downloadFile(url).subscribe(response => {
			console.log(response);
			return false;
			let blob:any = new Blob([response.blob()], { type: 'text/json; charset=utf-8' });
			const url= window.URL.createObjectURL(blob);
			window.open(url);
			fileSaver.saveAs(blob, filename);
			window.location.href = response.url
		});
		/*var URL = window.URL;
		var file = new Blob(
    		[url],
    		{"type" : "video\/webm"});
		var value = URL.createObjectURL(file);
		//window.open(value);
		fileSaver.saveAs(value, filename);*/
		//window.location.href = response.url
	}
	ngDoCheck(){
	}
	ngOnDestroy(){
		let prevObj=$(this.folderTree.nativeElement).jstree(true);
		if(prevObj) prevObj.destroy();
	}

}
