import { Component, OnInit, DoCheck, Inject, ViewChild, OnDestroy } from '@angular/core';
import {LiveClassService} from '../_services/live-class.service';
import {UserService} from '../_services/user.service';
import {Params, Router,ActivatedRoute} from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';
import { DataTableDirective } from 'angular-datatables';
import * as moment from 'moment';
@Component({
   //moduleId: module.id,
  templateUrl: './enrolled_live_class_list.component.html'
})

export class EnrolledLiveClassListComponent implements OnInit,DoCheck {
	@ViewChild(DataTableDirective, {static: false}) private datatableElement: DataTableDirective;
  dtOptions: any = {};
	operation={lifecycle: 'started',status: false,message: ''};
 	
 	classes=[];
  currentUser:any;
  token='';
 	constructor(private router: Router,private UserService: UserService, private LiveClassService: LiveClassService,private route: ActivatedRoute) {    		
  }

  ngOnInit() {
		const that = this;
    that.currentUser=JSON.parse(localStorage.getItem('user_data'));
		that.token=localStorage.getItem('token');
    this.dtOptions={
      pagingType: 'simple_numbers',
      pageLength: 10,
      dom: 'fltip',
      serverSide: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search by Class Name"
      },
      order: [[1, 'desc']],
      columns: [
        {"name": 'start_time',"orderable": true},
        {"name": 'start_time',"orderable": true},
        {"name": 'id',"orderable": false}
      ],
      ajax: (dataTablesParameters: any, callback) => {
        this.LiveClassService.getEnrolledClasses(dataTablesParameters).subscribe(resp => {
          this.classes = resp.data;
          callback({
            recordsTotal: resp.total,
            recordsFiltered: resp.total,
            data: []
          });
        });
      }
    };
	}
  
	ngDoCheck(){
		
	}
  toTime(time,offset){
    return moment.utc(time).utcOffset(offset)
  }
	isBefore(startTime,endTime){
		if(this.isBetween(startTime,endTime)) return false;
		return moment.utc(startTime).isBefore();
	}
	isAfter(startTime,endTime){
		if(this.isBetween(startTime,endTime)) return false;
		return moment.utc(startTime).isAfter();
	}
	isBetween(startTime,endTime){
    return moment.utc(startTime).isBefore() && moment.utc(endTime).isAfter();
	}
}
