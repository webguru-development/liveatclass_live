import { Component, OnInit, DoCheck, Inject, ViewChild, OnDestroy } from '@angular/core';
import {LiveClassService} from '../_services/live-class.service';
import {UserService} from '../_services/user.service';
import {Params, Router,ActivatedRoute} from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';
import { DataTableDirective } from 'angular-datatables';
import { SortablejsOptions } from 'ngx-sortablejs';
import * as moment from 'moment';
@Component({
   //moduleId: module.id,
  templateUrl: './live_class_list.component.html'
})

export class LiveClassListComponent implements OnInit,DoCheck {
	@ViewChild(DataTableDirective, {static: false}) private datatableElement: DataTableDirective;
  @ViewChild('deleteClassModal', { static: true }) deleteClassModal: ModalDirective;
  @ViewChild('deleteClassMessageModal', { static: true }) deleteClassMessageModal: ModalDirective;
	dtOptions: any = {};
	operation={lifecycle: 'started',status: false,message: ''};
 	
 	classes=[];
 	selectedClasses={};
  classStudents=[];
  plan={no_of_users_per_class: 20};
  currentUser:any;
  token='';
 	constructor(private router: Router,private UserService: UserService, private LiveClassService: LiveClassService,private route: ActivatedRoute) {    		
  }

  ngOnInit() {
		const that = this;
    that.currentUser=JSON.parse(localStorage.getItem('user_data'));
		that.token=localStorage.getItem('token');
    this.dtOptions={
      pagingType: 'simple_numbers',
      pageLength: 10,
      dom: 'fltip',
      serverSide: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search by Class Name"
      },
      order: [[1, 'desc']],
      columns: [
        {"name": 'id',"orderable": false},
        {"name": 'start_time',"orderable": true},
        {"name": 'title',"orderable": true},
        {"name": 'id',"orderable": false},
        {"name": 'id',"orderable": false},
      ],
      ajax: (dataTablesParameters: any, callback) => {
        this.LiveClassService.getClasses(dataTablesParameters).subscribe(resp => {
          this.classes = resp.data;
          callback({
            recordsTotal: resp.total,
            recordsFiltered: resp.total,
            data: []
          });
        });
      }
    };
    this.UserService.getPlanInfo().subscribe(resp=>{
      this.plan=resp;
    },err=>{

    })
	}
  
	ngDoCheck(){
		
	}
  toTime(time,offset){
    return moment.utc(time).utcOffset(offset)
  }
	isBefore(startTime,endTime){
		if(this.isBetween(startTime,endTime)) return false;
		return moment.utc(startTime).isBefore();
	}
	isAfter(startTime,endTime){
		if(this.isBetween(startTime,endTime)) return false;
		return moment.utc(startTime).isAfter();
	}
	isBetween(startTime,endTime){
    return moment.utc(startTime).isBefore() && moment.utc(endTime).isAfter();
	}
	selectAllClasses(e){
		this.selectedClasses={};
		this.classes.forEach((classObj)=>{
			this.selectedClasses[classObj.id]=e.target.checked;
		});
	}
	anySelected(){
		return Object.values(this.selectedClasses).indexOf(true) !== -1;
	}
	deleteSelected(){
		let classIds=[];
		Object.keys(this.selectedClasses).forEach(key=>{
			if(this.selectedClasses[key]) classIds.push(key);
		});
		this.LiveClassService.deleteClasses(classIds)
				.subscribe(
					data => { 
						this.selectedClasses={};
            this.classes=[];
            console.log(data.status);
            if(data.status){
              this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.ajax.reload();
                this.deleteClassModal.hide();
              });  
            }else{
              this.deleteClassMessageModal.show();
              this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.ajax.reload();
                //this.deleteClassModal.hide();
              });  
            }
					},
					error => {

					}
				);   
	} 
	
 }
