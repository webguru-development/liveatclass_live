import {Component,Input,DoCheck,OnInit} from '@angular/core';
import {UserService} from './_services/user.service';
import {User} from './_models/user';
import { Router } from '@angular/router';



@Component({

  selector: 'footer-page',
  templateUrl: './footer-bar.component.html'

})

export class FooterBarComponent{
data:any;
slug= '';
language = '';
pageContent= '';
static="";
currentUser:any;
model:any;
constructor(private userService: UserService, private router: Router) {
 }
 ngOnInit(){
	
		if(JSON.parse(localStorage.getItem("user_data")) != null){
			  this.currentUser= JSON.parse(localStorage.getItem("user_data"));
		  }
			this.data = JSON.parse(localStorage.getItem("language"));
			this.language = JSON.parse(localStorage.getItem("activeLanguage"));
			this.getslug();
		}
     ngDoCheck(){
		if(this.language!=JSON.parse(localStorage.getItem("activeLanguage"))){
			this.language = JSON.parse(localStorage.getItem("activeLanguage"));
			this.getslug();
		 }
		 
	}
 
 getslug(){
	 this.userService.getslug(this.language)
			.subscribe(
				data => {  
					this.slug = data;
					
				},
				error => {

				}
			);   
 }
 	 
}
