import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams} from '@angular/common/http';
import { RequestOptions, Response} from '@angular/http';
import { EmailTemplate } from '../_models/email_template';
import {environment} from '../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class EmailTemplateService {

  constructor(private http: HttpClient) { }
  saveTemplate(template){
    template.is_default = 0; //do not remove this line or template will be default forever
    return this.http.put<any>(environment.apiUrl+'/email_templates/'+template.id, template);
  }
  getTemplate(templateId){
    return this.http.get<any>(environment.apiUrl+'/email_templates/'+templateId).map((res) => {
            res.data = new EmailTemplate().deserialize(res.data); 
            return res;
          });
  }
  getAllTemplates(){
    return this.http.get<any>(environment.apiUrl+'/email_templates')
          .map((res) => {
            res = res.map((emailTemplate: EmailTemplate) => new EmailTemplate().deserialize(emailTemplate)); 
            return res;
          });
  }
  resetTemplate(template){
    return this.http.get<any>(environment.apiUrl+'/email_templates_reset/'+template.id);
  }
}
