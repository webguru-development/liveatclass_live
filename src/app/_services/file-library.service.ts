import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams} from '@angular/common/http';
import { Http, RequestOptions, Response, ResponseContentType} from '@angular/http';
import { FileLibrary } from '../_models/file_library';
import {environment} from '../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class FileLibraryService { 
  constructor(private http: HttpClient, private _http: Http) { }
  params:any; 
  // Todo: delete these later when library works
  // create(user: User) {
  //   return this.http.post(environment.apiUrl+'/register', user);
  // }
  // getFiles(file: FileLibrary) {
  //   return this.http.get(environment.apiUrl+'/register', file);
  // }
  updateFile(file){
    return this.http.put(environment.apiUrl+'/file_library/'+file.id, file);
  }
  addFile(file,opts){
    if(typeof opts == 'undefined') opts={};
    let formData=new FormData();
    Object.keys(file).forEach(function(key){
      formData.append(key,(typeof file[key]=='boolean') ? (file[key] ? 1 : 0) : file[key]);
    });
    return this.http.post<any>(environment.apiUrl+'/file_library', formData,opts);
  }
  addFiles(files,opts){
    if(typeof opts == 'undefined') opts={};
    let formData=new FormData();
    let index=0;
    files.forEach(file=>{
      Object.keys(file).forEach(function(key){
        formData.append('files['+index+']['+key+']',(typeof file[key]=='boolean') ? (file[key] ? 1 : 0) : file[key]);
      });
      index++;
    });
    return this.http.post<any>(environment.apiUrl+'/file_library/bulk_upload', formData,opts);
  }
  moveToFolder(iDs,folderId, keepCopy){
    return this.http.post(environment.apiUrl+'/file_library/move_to_folder', {id: iDs,folder_id: folderId,keep_copy: keepCopy});
  }
  getFiles(dtParams:any, currentFolderId, showFileType,fileType?:any){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : '',
        parent_id: currentFolderId,
        privacy: showFileType,
        file_type: fileType ? fileType.join(',') : ''
      }
    });
    return this.http.get<any>(environment.apiUrl+'/file_library/files',{params: opts})
            .map((res) => {
              res.data = res.data.map((file: FileLibrary) => new FileLibrary().deserialize(file)); 
              return res;
            });
  }
  getSpaceUsage(){
    return this.http.get<any>(environment.apiUrl+'/file_library/space');
  }
  getAllSpaceUsage(){
    return this.http.get<any>(environment.apiUrl+'/file_library/space_all');
  }

  getTopFiveSpaceUsage(){
    return this.http.get<any>(environment.apiUrl+'/file_library/space_top_five');
  }

  saveStorage(teacherId,space){
    return this.http.post<any>(environment.apiUrl+'/file_library/space_allocate',{id: teacherId,space: space});
  }
  deleteFiles(ids){
    return this.http.post<any>(environment.apiUrl+'/file_library/delete_files',{id: ids});
  }
  getFolders(){
    function recursiveAssign(files){
      return files.map((file) => {
        file.children=[];
        file.state={opened: true};
        file.text=file.name;
        if(file.folders.length){
          file.children=recursiveAssign(file.folders);
        }
        delete file.folders;
        return file;
      }); 
    }
    return this.http.get<any>(environment.apiUrl+'/file_library/folders')
            .map((res) => {
              res.data = recursiveAssign(res.data); 
              return res;
            });
  }

  getEmbeds(dtParams:any, currentFolderId, showFileType){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : '',
      }
    });
    return this.http.get<any>(environment.apiUrl+'/file_library/embeds',{params: opts})
            .map((res) => {
              res.data = res.data.map((file: FileLibrary) => new FileLibrary().deserialize(file)); 
              return res;
            });
  }
  getMyRecordings(dtParams:any){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : '',
      }
    });
    return this.http.get<any>(environment.apiUrl+'/file_library/my_recordings',{params: opts})
            .map((res) => {
              res.data = res.data.map((file: FileLibrary) => new FileLibrary().deserialize(file)); 
              return res;
            });
  }
  getRecordings(dtParams:any, currentFolderId, showFileType){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : '',
      }
    });
    return this.http.get<any>(environment.apiUrl+'/file_library/recordings',{params: opts})
            .map((res) => {
              res.data = res.data.map((file: FileLibrary) => new FileLibrary().deserialize(file)); 
              return res;
            });
  }
  getTemplates(dtParams:any, currentFolderId, showFileType){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : '',
      }
    });
    return this.http.get<any>(environment.apiUrl+'/file_library/templates',{params: opts})
            .map((res) => {
              res.data = res.data.map((file: FileLibrary) => new FileLibrary().deserialize(file)); 
              return res;
            });
  }

  downloadFile(recording_url){
    //console.log('sss');
    //return this._http.get('http://localhost/test/test.php', {responseType: ResponseContentType.Blob});
    let opts = {
      'recording_url' : recording_url
    };
    return this.http.get<any>(environment.apiUrl+'/file_library/getVideo',{params: opts});
  }

  getCertificates(){
    return this.http.get<any>(environment.apiUrl+'/getCertificates');
  }

  saveLogoSettings(logoType,logo:File){
      let formData=new FormData();
      formData.append('logoType',logoType);
      formData.append('logo', logo,logo.name);
      return this.http.post<any>(environment.apiUrl+'/saveInstituteCertificate', formData);
    }

}
