import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams} from '@angular/common/http';
import { RequestOptions, Response} from '@angular/http';
import { Group } from '../_models/group';
import {environment} from '../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class GroupService { 
  constructor(private http: HttpClient) { }
  params:any; 
  updateGroup(group){
    return this.http.put(environment.apiUrl+'/groups/'+group.id, group);
  }
  addGroup(group){
    return this.http.post(environment.apiUrl+'/groups', group);
  }
  assignToGroup(groupId,userIds){
    return this.http.post(environment.apiUrl+'/groups/assign_users', {id: groupId, users: userIds});
  }
  assignCoursesToGroup(groupId,courseIds){
    return this.http.post(environment.apiUrl+'/groups/assign_courses', {id: groupId, courses: courseIds});
  }
  assignClassesToGroup(groupId,classIds){
    return this.http.post(environment.apiUrl+'/groups/assign_classes', {id: groupId, classes: classIds});
  }
  
  getDetail(groupId){
    return this.http.get<any>(environment.apiUrl+'/groups/'+groupId)
      .map((res) => {
        res.data.group = new Group().deserialize(res.data.group); 
        return res;
      });
  }
  getGroups(dtParams:any){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : ''
      }
    });
    return this.http.get<any>(environment.apiUrl+'/groups',{params: opts})
            .map((res) => {
              res.data = res.data.map((group: Group) => new Group().deserialize(group)); 
              return res;
            });
  }
  getUsers(dtParams:any, groupId){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : ''
      }
    });
    return this.http.get<any>(environment.apiUrl+'/groups/'+groupId+'/users',{params: opts});  
  }
  getCourses(dtParams:any, groupId){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : ''
      }
    });
    return this.http.get<any>(environment.apiUrl+'/groups/'+groupId+'/courses',{params: opts});
  }
  getClasses(dtParams:any, groupId){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : ''
      }
    });
    return this.http.get<any>(environment.apiUrl+'/groups/'+groupId+'/classes',{params: opts});
  }
  deleteGroup(id){
    return this.http.delete<any>(environment.apiUrl+'/groups/'+id);
  }
  deleteUser(assignedUserId,groupId){
    return this.http.delete<any>(environment.apiUrl+'/groups/delete_user/'+assignedUserId);
  }
  deleteClass(assignedClassId,groupId){
    return this.http.delete<any>(environment.apiUrl+'/groups/delete_class/'+assignedClassId);
  }
  deleteCourse(assignedCourseId,groupId){
    return this.http.delete<any>(environment.apiUrl+'/groups/delete_course/'+assignedCourseId);
  }
  
}
