import { TestBed } from '@angular/core/testing';

import { LiveClassService } from './live-class.service';

describe('LiveClassService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LiveClassService = TestBed.get(LiveClassService);
    expect(service).toBeTruthy();
  });
});
