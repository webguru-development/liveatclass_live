﻿import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams} from '@angular/common/http';
import { RequestOptions} from '@angular/http';
import { User } from '../_models/user';
import {Course} from '../_models/Courseintro';
import {environment} from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class UserService { 
	
private courseSource = new BehaviorSubject('');
currentCourse = this.courseSource.asObservable();

private siteSettingsSrc = new BehaviorSubject('');
siteSetting = this.siteSettingsSrc.asObservable();

private currentUserSrc = new BehaviorSubject('');
currentUser = this.currentUserSrc.asObservable();

constructor(private http: HttpClient) { }
params:any;	
	changeCourseTitle(courseTitle: any) {
		this.courseSource.next(courseTitle)
	 }
	updateSiteSettings(setting: any) {
		this.siteSettingsSrc.next(setting)
	}
	updateCurrentUser(user: any) {
		this.currentUserSrc.next(user)
	}
	getAll() { 
		return this.http.get<User[]>(environment.apiUrl+'/userData');
	}
	plans(){
		return this.http.get(environment.apiUrl+'/plans/all');
	}
	countries(){
		return this.http.get(environment.apiUrl+'/countries');
	}
	exportAll(type:string,token:string){
		return environment.apiUrl+'/users/export/'+type+'?token='+token;
	}
	languageRequest(language: string) {
		return this.http.post(environment.apiUrl+'/language',{language : language});
		//return this.http.post('http://localhost/liveatclass.dev/public_html/newapi/public/api'+'/test',{language : language});
	}

	create(user: User) {
		 
		return this.http.post(environment.apiUrl+'/register', user);
	}
	
	
	updateProfile(pic_selected:any,model:any) {
	 
			if(pic_selected){
			let formData=new FormData();
			formData.append('profile_image', model.profile_image,model.profile_image.name);
			formData.append('name',model.name);
			formData.append('description',model.description);
			formData.append('skills',model.skills);
			formData.append('profession',model.profession);
			formData.append('experience',model.experience);
			formData.append('phone_number',model.phone_number);
			formData.append('address',model.address);
			formData.append('user_id',model.user_id);
			formData.append('fb',model.fb);
			formData.append('tw',model.tw);
			formData.append('youtube',model.youtube);
			this.params=formData;
		}else{
			model.profile_image = null;
			this.params=model;
		}
		return this.http.post(environment.apiUrl+'/updateProfile',this.params );
	}

	update(user: User) {
		return this.http.put('/api/users/' + user.id, user);
	}

	delete(id: number) {
		return this.http.delete('/api/users/' + id);
	}
	
	login(email: string, password: string) {
		 
		return this.http.post<any>(environment.apiUrl+'/authenticate', { email: email, password: password });
	}
	
	appStudentLogin(email: string, password: string){
		
		return this.http.post<any>(environment.apiUrl+'/appStudentLogin', { email: email, password: password});
	}
	
	activateAccount(token: string) {
		return this.http.post<any>(environment.apiUrl+'/confirmation', { token: token});
	}
	getEventsForDate(date){
		return this.http.get<any>(environment.apiUrl+'/dashboard/events?date='+date);
	}
	siteStats() {
		return this.http.get<any>(environment.apiUrl+'/dashboard/site_stats');
	}
	
	getEnrollStats() {
		return this.http.get<any>(environment.apiUrl+'/dashboard/getEnrollStats');
	}
	
	getCompletedClassHoursStats() {
		return this.http.get<any>(environment.apiUrl+'/dashboard/getCompletedClassHoursStats');
	}
	
	getTotalCoursesStats() {
		return this.http.get<any>(environment.apiUrl+'/dashboard/getTotalCoursesStats');
	}
	
	getNumberOfLiveClassStats() {
		return this.http.get<any>(environment.apiUrl+'/dashboard/getNumberOfLiveClassStats');
	}
	
	getTopRankedCourses() {
		return this.http.get<any>(environment.apiUrl+'/dashboard/getTopRankedCourses');
	}
	
	getTeachersStats() {
		return this.http.get<any>(environment.apiUrl+'/dashboard/getTeachersStats');
	}
	
	getCoursesStats() {
		return this.http.get<any>(environment.apiUrl+'/teacher_dashboard/getCoursesStats');
	}
	
	getCoursesandTestStats() {
		return this.http.get<any>(environment.apiUrl+'/teacher_dashboard/getCoursesandTestStats');
	}
	
	getLangList() {
		 
		return this.http.get<any>(environment.apiUrl+'/getLangList');
	}
	
	getcategories() {
		 
		return this.http.get<any>(environment.apiUrl+'/getcategories');
	}
	getCounts() {
		 
		return this.http.get<any>(environment.apiUrl+'/getCounts');
	}
	getCmsPages(page:number) {
		 
		return this.http.get<any>(environment.apiUrl+'/getCmsPages?page='+page);
	}
	
	addCms(name:string, metatitle:string, content:string, seourl:string ,metadesc:string ,metakeyword:string ,activeLanguage:any){
		return this.http.post<any>(environment.apiUrl+'/addCms', { name:name, metatitle:metatitle, content:content, seourl:seourl, metadesc:metadesc, metakeyword:metakeyword, activeLanguage:activeLanguage});
	}
	
	getPackageList(activeLanguage:any) {
	
		 
		return this.http.post<any>(environment.apiUrl+'/getPackageList', {activeLanguage:activeLanguage});
	}
	
	changePassword(oldpassword:string, password: string, id:number) {
		 
		return this.http.post<any>(environment.apiUrl+'/changePassword', {oldpassword: oldpassword, password: password, id:id  });
	}
	
	forgotPassword(email: string) {
		 
		return this.http.post<any>(environment.apiUrl+'/forgotPassword', {email: email});
	}
	
	setPassword(password: string, email: string, token:string) {
		 
		return this.http.post<any>(environment.apiUrl+'/setPassword', {password: password,email: email, token:token});
	}
	
	buyPackage(id: number, userID: number) {
		return this.http.post<any>(environment.apiUrl+'/buyPackage', {id: id, userID:userID });
	}

	
	getCourses(userType:string,teacherId:number){
		 
		return this.http.get<any>(environment.apiUrl+'/getCourses?userType='+userType+'&teacherId='+teacherId);
	}
	
	courseIntro(courseID: number, course_name: string, price:number, category_id:number ) {
		 
		return this.http.post<any>(environment.apiUrl+'/courseIntro', {courseID:courseID, course_name:course_name, price:price, category_id:category_id });
	}
	getcourseIntro(courseID: number) {
		 
		return this.http.post<any>(environment.apiUrl+'/getcourseIntro', {courseID:courseID});
	}
	courseSummary(courseID: number, course_summary: string) {
		 
		return this.http.post<any>(environment.apiUrl+'/courseSummary', {courseID:courseID, course_summary:course_summary});
	}
	getCourseSummary(courseID: number) {
		 
		return this.http.post<any>(environment.apiUrl+'/getCourseSummary', {courseID:courseID});
	}
	getcourseImage(courseID: number) {
		 
		return this.http.post<any>(environment.apiUrl+'/getcourseImage', {courseID:courseID});
	}
	saveBatches(courseID: number, enrollmentId:number,arr: Array<{start: Date, end: Date}>) {
		 
		return this.http.post<any>(environment.apiUrl+'/saveBatches', {courseID:courseID, arr:arr,enrollmentId:enrollmentId });
	}
	getBatches(courseID: number){
		 
		return this.http.post<any>(environment.apiUrl+'/getBatches', {courseID:courseID});
	}
	
	addSyllabus(syllabus: any,courseID: any){
		 
		return this.http.post<any>(environment.apiUrl+'/addSyllabus',{ syllabus: syllabus, courseID:courseID });
	}
	
	courseImage(courseID: any ,course_image:File) {
		 
		let formData=new FormData();
		formData.append('course_image',course_image,course_image.name);
		formData.append('courseID',courseID);
		this.params=formData; 
	
		return this.http.post<any>(environment.apiUrl+'/courseImage',this.params );
	}
	 deleteMyCourse(id: number) {
		  
		return this.http.post<any>(environment.apiUrl+'/deleteMyCourse',{id:id});
	}
	 
	 //~ getTeacherList(page:number, limit:number,sort:any,searchKey:string) {
		  //~ 
		//~ return this.http.get<any>(environment.apiUrl+'/getTeacherList?page='+page+'&limit='+limit+'&sort='+sort+'&searchKey='+searchKey);
	//~ }
	getTeacherList() {
		  
		return this.http.get<any>(environment.apiUrl+'/getTeacherList');
	}

	getTeacherListByCourse(courseid) {
		  
		return this.http.get<any>(environment.apiUrl+'/getTeacherListByCourse/'+courseid);
	}

	loginAs(userID: number){
		return this.http.get<any>(environment.apiUrl+'/loginAs/'+userID);
	}
	 userProfile(userID: any) {
		  
		return this.http.post<any>(environment.apiUrl+'/userProfile',{userID:userID});
	}
	
	 contactUs(name: string, email:string, address:string, message:string) {
		  
		return this.http.post<any>(environment.apiUrl+'/contactUs',{name:name, email:email, address:address, message:message  });
	}
	
	 getContactPage(activeLanguage:any) {
		  
		return this.http.post<any>(environment.apiUrl+'/getContactPage',{activeLanguage:activeLanguage});
	}
	 getStaticPage(activeLanguage:any,slug:string) {
		  
		return this.http.post<any>(environment.apiUrl+'/getStaticPage',{activeLanguage:activeLanguage, slug:slug});
	}
	 getslug(activeLanguage:any) {
		  
		return this.http.post<any>(environment.apiUrl+'/getslug',{activeLanguage:activeLanguage});
	}
	
	 lessonContent(lessonID:string,content:File) {
		 
		
		let formData=new FormData();
		formData.append('lessonID',lessonID);
		formData.append('content', content,content.name);
		
		return this.http.post<any>(environment.apiUrl+'/lessonContent',formData);
	}
	
	lessonContentArticle(lessonID:string,lesson_content:string){
		 
		return this.http.post<any>(environment.apiUrl+'/lessonContentArticle',{ lessonID: lessonID, lesson_content:lesson_content });
	}

	saveModule(moduleId:number, module_name:string){
		 
		return this.http.post<any>(environment.apiUrl+'/saveModule',{moduleId:moduleId, module_name:module_name});
	}
	saveLesson(lessonId:number, lesson_name:string){
		 
		return this.http.post<any>(environment.apiUrl+'/saveLesson',{lessonId:lessonId, lesson_name:lesson_name});
	}
	
	addModule(courseID:number){
		 
		return this.http.post<any>(environment.apiUrl+'/addModule',{courseID:courseID});
	}
	
	
	addLesson(moduleID:number){
		 
		return this.http.post<any>(environment.apiUrl+'/addLesson',{moduleID:moduleID});

	}
	deleteLesson(lessonId:number){
		 
		return this.http.post<any>(environment.apiUrl+'/deleteLesson',{lessonId:lessonId});

	}
	deleteModule(moduleId:number){
		 
		return this.http.post<any>(environment.apiUrl+'/deleteModule',{moduleId:moduleId});

	}
	deleteQuiz(quizId:number){	 
		return this.http.post<any>(environment.apiUrl+'/deleteQuiz',{quizId:quizId});
	}
	
	deleteContent(lessonId:number){
		 
		return this.http.post<any>(environment.apiUrl+'/deleteContent',{lessonId:lessonId});
	}
	deleteDocument(lessonId:number){
		 
		return this.http.post<any>(environment.apiUrl+'/deleteDocument',{lessonId:lessonId});
	}
	getModuleContent(courseID:number){
	
		return this.http.get<any>(environment.apiUrl+'/getModuleContent?courseID='+courseID);
	}
	
	getAllCoursesList(page:number, limit:number,sort:string,searchKey:string){
		 
		return this.http.get<any>(environment.apiUrl+'/getAllCourses?page='+page+'&limit='+limit+'&sort='+sort+'&searchKey='+searchKey);
	
	}
	getLessonPreviewContent(lessonID:number){
		return this.http.get<any>(environment.apiUrl+'/getLessonPreviewContent?lessonID='+lessonID);
	}
	
	addQuiz(lessonID:number,moduleID:number){
		 
		return this.http.post<any>(environment.apiUrl+'/addQuiz', {lessonID:lessonID, moduleID:moduleID});
	}
	
	saveQuiz(lessonID:number,title:string){
		 
		return this.http.post<any>(environment.apiUrl+'/saveQuiz', {lessonID:lessonID, title:title});
	}
	addQuestions(quizId:number, options:any){
		 
		return this.http.post<any>(environment.apiUrl+'/addQuestions', {quizId:quizId, options:options });
	}
	addQuestionBank(options:any){
		 
		return this.http.post<any>(environment.apiUrl+'/addQuestionBank', {options:options });
	}
	getOptions(quizId:number){
		 
		return this.http.get<any>(environment.apiUrl+'/getOptions?quizId='+quizId);
	}
	getMyQuestionBank(){
		 
		return this.http.get<any>(environment.apiUrl+'/getMyQuestionBank');
	}
	getQuestionBank(){
		 
		return this.http.get<any>(environment.apiUrl+'/getQuestionBank');
	}
	editOption(questionId:any){
		 
		return this.http.post<any>(environment.apiUrl+'/editOption', {questionId:questionId});
	}
	previewQuestionBank(questionId:any){
		 
		return this.http.post<any>(environment.apiUrl+'/previewQuestionBank', {questionId:questionId});
	}
	deleteQuestion(questionId:number){
		 
		return this.http.post<any>(environment.apiUrl+'/deleteQuestion', {questionId:questionId});
	}
	editQuestion(questionId:number, options:any){
		 
		return this.http.post<any>(environment.apiUrl+'/editQuestion', {questionId:questionId,options:options });
	}
	quizQuestionBank(quizId:number, val:any){
		 
		return this.http.post<any>(environment.apiUrl+'/quizQuestionBank', {quizId:quizId,val:val });
	}
	assignCourses(teacherId:number, val:any){
		 
		return this.http.post<any>(environment.apiUrl+'/assignCourses', val+'&teacherId='+teacherId,{headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
	}
	
	getpreviewLessonContent(lessonId:number){
		 
		return this.http.get<any>(environment.apiUrl+'/getpreviewLessonContent?lessonId='+lessonId);
	}
	getPreviewQuizContent(quizId:number){
		 
		return this.http.get<any>(environment.apiUrl+'/getPreviewQuizContent?quizId='+quizId);
	}

	previewStartQuiz(quizId:number){
		 
		return this.http.get<any>(environment.apiUrl+'/previewStartQuiz?quizId='+quizId);
	}
	getRandomQuestion(quizId:number,quizUserID:number,qID:number,ans:string, essayAns:string){
		 
		return this.http.get<any>(environment.apiUrl+'/getRandomQuestion?quizId='+quizId+'&quizUserID='+quizUserID+'&qId='+qID+'&answer='+ans+'&essayAns='+essayAns);
	}
	Result(quizUserID:number, quizId:number){
		 
		return this.http.get<any>(environment.apiUrl+'/Result?quizUserID='+quizUserID+'&quizId='+quizId);
	}
	QuizResult(studentId:number, quizId:number){
		 
		return this.http.get<any>(environment.apiUrl+'/QuizResult?studentId='+studentId+'&quizId='+quizId);
	}
	QuizReport(studentId:number, courseId:number){
		 
		return this.http.get<any>(environment.apiUrl+'/QuizReport?studentId='+studentId+'&courseId='+courseId);
	}
	uploadAssignment(assignmentId:string,assignmentContent:string,files:File){
		 
		let formData=new FormData();
		if(files != null){
		formData.append('file', files,files.name);
	}
		formData.append('assignmentId', assignmentId);
		formData.append('assignmentContent', assignmentContent);
		return this.http.post<any>(environment.apiUrl+'/uploadAssignment',formData);
	}
	edituploaddoc(assignmentId:string,files:File){
		 
		let formData=new FormData();
		formData.append('file', files,files.name);
		formData.append('assignmentId', assignmentId);
		return this.http.post<any>(environment.apiUrl+'/edituploaddoc',formData);

	}
	getAssignments(courseID:number){
		 
		return this.http.get<any>(environment.apiUrl+'/getAssignments?courseID='+courseID);
	}
	saveAssigmentName(courseID:number,assignmentName:string){
		 
		return this.http.post<any>(environment.apiUrl+'/saveAssigmentName',{courseID:courseID,assignmentName:assignmentName});
	}
	getAssignmentsQuestions(assignmentId:number){
		 
		return this.http.get<any>(environment.apiUrl+'/getAssignmentsQuestions?assignmentId='+assignmentId);
	}
	editQassignment(assignmentId:number, Qcontent:string){
		 
		return this.http.post<any>(environment.apiUrl+'/editQassignment',{assignmentId:assignmentId,Qcontent:Qcontent});
	}
	deleteQassignment(assignmentId:number){
		 
		return this.http.post<any>(environment.apiUrl+'/deleteQassignment',{assignmentId:assignmentId});
	}
	deleteAssignment(assignmentId:number){
		 
		return this.http.post<any>(environment.apiUrl+'/deleteAssignment',{assignmentId:assignmentId});	
	}
	editAssignment(assignmentId:number,Qcontent:	string){
		 
		return this.http.post<any>(environment.apiUrl+'/editAssignment',{assignmentId:assignmentId,Qcontent:Qcontent});	
	}
	savePromo(courseID:string,file:File){
		 
	
		let formData=new FormData();
		if(file != null){
			formData.append('file', file,file.name);
		}
		formData.append('courseID', courseID);
		return this.http.post<any>(environment.apiUrl+'/savePromo',formData);
	}
	getpromo(courseID:string){
		 
		return this.http.get<any>(environment.apiUrl+'/getpromo?courseID='+courseID);
	}
	
	checkValidDomain(){	 
		return this.http.get<any>(environment.apiUrl+'/checkValidDomain');
	}
	
	getUserDetail(){	 
		return this.http.get<any>(environment.apiUrl+'/getUserDetail');
	}
	saveCategory(name:string){
			return this.http.post<any>(environment.apiUrl+'/saveCategory',{name:name});
	}
	getInstituteUser(type:string, page:number){
			return this.http.get<any>(environment.apiUrl+'/getInstituteUser?type='+type+'&page='+page);
	}
	saveInstituteUser(userId:number,name:any){
			return this.http.post<any>(environment.apiUrl+'/saveInstituteUser', {userId:userId, name:name});
	}
	getCourseDetail(id:number){
			return this.http.get<any>(environment.apiUrl+'/getCourseDetail?id='+id);
	}
	deleteInstituteUser(id:number){
			return this.http.post<any>(environment.apiUrl+'/deleteInstituteUser', {id:id});
	}
	getCategory(page:number){
			return this.http.get<any>(environment.apiUrl+'/getCategory?page='+page);
	}
	saveEditCategory(id:number, name:string){
			return this.http.post<any>(environment.apiUrl+'/saveEditCategory', {id:id, name:name});
	}
	deleteCategory(id:number){
			return this.http.post<any>(environment.apiUrl+'/deleteCategory', {id:id});
	}
	deleteQuestionBankList(id:number){
		return this.http.post<any>(environment.apiUrl+'/deleteQuestionBankList', {id:id});
	}
	deletePromovideo(courseId:string){
		return this.http.post<any>(environment.apiUrl+'/deletePromovideo', {courseId:courseId});
	}
	
	addInstituteUser(firstname:string, lastname:string, email:string, password:string,instituteId:string, userType:string){
		return this.http.post<any>(environment.apiUrl+'/addInstituteUser', {firstname:firstname, lastname:lastname, email:email, password: password, instituteId:instituteId, userType:userType});
	}
	
	editInstituteUser(userId:number){
		return this.http.post<any>(environment.apiUrl+'/editInstituteUser', {userId:userId});
	}
	
	editCms(id:number){
		return this.http.get<any>(environment.apiUrl+'/editCms?id='+id);
	}
	
	updateCmsPage(id:number, name:string, metatitle:string, content:string, seourl:string ,metadesc:string ,metakeyword:string ,activeLanguage:any){
		return this.http.post<any>(environment.apiUrl+'/updateCmsPage', { id:id,name:name, metatitle:metatitle, content:content, seourl:seourl, metadesc:metadesc, metakeyword:metakeyword, activeLanguage:activeLanguage});
	}
	
	deleteCmsPage(id:number){
		return this.http.post<any>(environment.apiUrl+'/deleteCmsPage',{id:id});
	}
	
	getAllEnrolledCourses(){
		return this.http.get<any>(environment.apiUrl+'/getAllEnrolledCourses');
	}
	
	getEnrolled(id:number){
		return this.http.get<any>(environment.apiUrl+'/getEnrolled?id='+id);
	}
	SaveToEnroll(studentId:number,activeLanguage:number, val:any){
		return this.http.post<any>(environment.apiUrl+'/SaveToEnroll', val+'&studentId='+studentId+'&activeLanguage='+activeLanguage,{headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
	}
	
	studentList(courseId:number,classId:number){
		return this.http.get<any>(environment.apiUrl+'/studentList?courseId='+courseId+'&classId='+classId);
	}
	//~ assignLiveClassSchedule(duration:number, startDate:string, courseId:number,val:any){
		//~ return this.http.post<any>(environment.apiUrl+'/assignLiveClassSchedule', val+'&duration='+duration+'&startDate='+startDate+'&courseId='+courseId,{headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
	//~ }
	assignLiveClassSchedule(duration:number, startDate:string, courseId:number){
		return this.http.post<any>(environment.apiUrl+'/assignLiveClassSchedule', {duration:duration, startDate:startDate, courseId:courseId});
	}
	assignStudentToclass(classId:number, val:any){

		return this.http.post<any>(environment.apiUrl+'/assignStudentToclass', val+'&classId='+classId,{headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
	}
	
	
	getManageClasses(classId:number){
		return this.http.get<any>(environment.apiUrl+'/getManageClasses?classId='+classId);
	}
	updateClass(classId:number,duration:number, startDate:string){
		return this.http.post<any>(environment.apiUrl+'/updateClass', {classId:classId, duration:duration, startDate:startDate});
	}
	
	getClasseScheduleList(courseId:number,page:number){
		return this.http.get<any>(environment.apiUrl+'/getClasseScheduleList?courseId='+courseId+'&page='+page);
	}
	//~ getStudentList(courseId:number,classId:number){
		//~ return this.http.get<any>(environment.apiUrl+'/studentList?courseId='+courseId+'&classId='+classId);
	//~ }
	
	deleteClassManageList(id:number){
			return this.http.post<any>(environment.apiUrl+'/deleteClassManageList', {id:id});
	}
	
	getUsersList(){
		return this.http.get<any>(environment.apiUrl+'/getUsersList');
	}
	
	
	sendMessageToReceiver(receiverId:number,conversation:any){
		return this.http.post<any>(environment.apiUrl+'/sendMessageToReceiver', {receiverId:receiverId, conversation:conversation});
	}
	
	getallConversation(receiverId:number){
		return this.http.get<any>(environment.apiUrl+'/getallConversation?receiverId='+receiverId);	
	}
	getAssignmentList(courseId:number){
	return this.http.get<any>(environment.apiUrl+'/getAssignmentList?courseId='+courseId);	
	}
	getAssignmentDetail(assignmentId:number){
	return this.http.get<any>(environment.apiUrl+'/getAssignmentDetail?assignmentId='+assignmentId);	
	}
	submitAssignment(assignmentId:string,files:File){
		let formData=new FormData();
		formData.append('file', files,files.name);
		formData.append('assignmentId', assignmentId);
		return this.http.post<any>(environment.apiUrl+'/submitAssignment', formData);	
	}
	getSubmitAssignments(courseId:number){
		return this.http.get<any>(environment.apiUrl+'/getSubmitAssignments?courseId='+courseId);	
	}
	
	AssignmentsResult(submitAssignmentId:number,marks:number){
		return this.http.post<any>(environment.apiUrl+'/AssignmentsResult',{submitAssignmentId:submitAssignmentId, marks:marks});
	}
	getquestionBanksList(page:number){
		return this.http.get<any>(environment.apiUrl+'/getquestionBanksList?page='+page);
	}
	getquestionBankOption(questionId:number){
		return this.http.get<any>(environment.apiUrl+'/getquestionBankOption?questionId='+questionId);
	}
	editQuestionBank(questionId:any, options:any){	 
		return this.http.post<any>(environment.apiUrl+'/editQuestionBank', {questionId:questionId, options:options});
	}
	getStudentsList(courseId:number,includeTeachers:boolean){
		if(typeof includeTeachers=='undefined') includeTeachers=false;
		return this.http.get<any>(environment.apiUrl+'/getStudentsList?includeTeachers='+(includeTeachers? 1: 0)+'&courseId='+courseId);
	}
	getPlanInfo(){
		return this.http.get<any>(environment.apiUrl+'/getPlanInfo');
	}
	StudentEnrolltoCourse(courseId:number, activeLanguage:number, val:any){
		return this.http.post<any>(environment.apiUrl+'/StudentEnrolltoCourse', val+'&courseId='+courseId+'&activeLanguage='+activeLanguage,{headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
	}
	saveGroup(userId:number, groupName:string){
	return this.http.post<any>(environment.apiUrl+'/saveGroup', {userId:userId, groupName:groupName});
	}
	getGroups(page:number){
		return this.http.get<any>(environment.apiUrl+'/getGroups?page='+page);
	}
	getAllStudents(groupId:number){
		return this.http.get<any>(environment.apiUrl+'/getAllStudents?&groupId='+groupId);
	}
	studentGroup(groupId:number, val:any){
		return this.http.post<any>(environment.apiUrl+'/studentGroup', val+'&groupId='+groupId,{headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
	}
	deleteGroup(id:number){
			return this.http.post<any>(environment.apiUrl+'/deleteGroup', {id:id});
	}
	saveEditGroup(id:number, name:any){
			return this.http.post<any>(environment.apiUrl+'/saveEditGroup', {id:id, name:name});
	}
	getGroupList(courseId:number){
		return this.http.get<any>(environment.apiUrl+'/getGroupList?&courseId='+courseId);
	}
	assignCourseToGroup(courseId:number,activeLanguage:number, val:any ){
		return this.http.post<any>(environment.apiUrl+'/assignCourseToGroup', val+'&courseId='+courseId+'&activeLanguage='+activeLanguage,{headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
	}
	
	//Worked for new layout "ranjana or rajat"//
	
	addChapter(courseId:number, courseChapters:any){
		 
		return this.http.post<any>(environment.apiUrl+'/addChapter', {courseId:courseId, courseChapters:courseChapters});
	
	}
	
	addChapterUnit(courseId:number, courseChapterId:number , courseChapterUnits:any){
		 
		return this.http.post<any>(environment.apiUrl+'/addChapterUnit', {courseId:courseId,courseChapterId:courseChapterId, courseChapterUnits:courseChapterUnits});
	
	}
	
	addUnitResource(courseId:number, courseChapterId:number ,courseChapterUnitId:number , courseChapterUnitResources:any,fileId:any,classId:any){
		 
		return this.http.post<any>(environment.apiUrl+'/addUnitResource', {courseId:courseId,courseChapterId:courseChapterId,courseChapterUnitId:courseChapterUnitId, courseChapterUnitResources:courseChapterUnitResources,fileId:fileId,classId:classId});
	
	}
	
	
	saveAssignCourses(userId:number, enrollCourses:string ){
		 
		return this.http.post<any>(environment.apiUrl+'/saveAssignCourses',  {userId:userId,enrollCourses:enrollCourses });
	}
	
	saveEnrollCourses(userId:number, enrollCourses:string ){
		 
		return this.http.post<any>(environment.apiUrl+'/saveEnrollCourses',  {userId:userId,enrollCourses:enrollCourses });
	}
	
	saveAssignGroups(userId:number, assignGroups:string ){
		 
		return this.http.post<any>(environment.apiUrl+'/saveAssignGroups',  {userId:userId,assignGroups:assignGroups });
	}
	
	
	getAllCourses(page:number, limit:number,searchKey:string, year: string = 'all'){
		 
		return this.http.get<any>(environment.apiUrl+'/getAllCourses?page='+page+'&limit='+limit+'&searchKey='+searchKey+'&year='+year);
	
	}
	
	
	getAllUsers(dtParams:any){
		let opts=new HttpParams({
			fromObject: {
				limit: dtParams.length,
				offset: dtParams.start,
				page: ((dtParams.start/dtParams.length) + 1).toString(),
				search: dtParams.search.value,
				order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].data : '',
				dir: dtParams.order.length ? dtParams.order[0].dir : '',
			}
		});
		return this.http.get<any>(environment.apiUrl+'/getAllUsers',{params: opts}).map((res) => {
      if(typeof res.data == 'undefined'){
      	res.data=res.user_data.data;
      	res.total=res.user_data.total;
      } 
      return res;
    });
	}
	
	getAllGroups(dtParams:any){
		let opts=new HttpParams({
			fromObject: {
				limit: dtParams.length,
				offset: dtParams.start,
				page: ((dtParams.start/dtParams.length) + 1).toString(),
				search: dtParams.search.value,
				order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].data : '',
				dir: dtParams.order.length ? dtParams.order[0].dir : '',
			}
		});
		return this.http.get<any>(environment.apiUrl+'/getAllGroups',{params: opts});
	}
	
	getMyClassList(dtParams:any){
		let opts=new HttpParams({
			fromObject: {
				limit: dtParams.length,
				offset: dtParams.start,
				page: ((dtParams.start/dtParams.length) + 1).toString(),
				search: dtParams.search.value,
				order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
				dir: dtParams.order.length ? dtParams.order[0].dir : '',
			}
		});
		return this.http.get<any>(environment.apiUrl+'/getMyClassList',{params: opts});
	}

	getMyCourseList(dtParams:any,isActive?:number){
		let params = {
				limit: dtParams.length,
				offset: dtParams.start,
				page: ((dtParams.start/dtParams.length) + 1).toString(),
				search: dtParams.search.value,
				order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].data : '',
				dir: dtParams.order.length ? dtParams.order[0].dir : ''
			}
		if(isActive) params['is_active']=isActive;
		let opts=new HttpParams({
			fromObject: params
		});
		return this.http.get<any>(environment.apiUrl+'/getMyCourseList',{params: opts});
	}

	getMyCourseListByUser(dtParams:any,userId:any,isActive?:number){
		let params = {
				limit: dtParams.length,
				offset: dtParams.start,
				page: ((dtParams.start/dtParams.length) + 1).toString(),
				search: dtParams.search.value,
				order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].data : '',
				dir: dtParams.order.length ? dtParams.order[0].dir : '',
				user_id : userId
			}
		if(isActive) params['is_active']=isActive;
		let opts=new HttpParams({
			fromObject: params
		});
		return this.http.get<any>(environment.apiUrl+'/getMyCourseListByUser',{params: opts});
	}
	
	getMyEnrolledCourses(dtParams:any){
		let opts=new HttpParams({
			fromObject: {
				limit: dtParams.length,
				offset: dtParams.start,
				page: ((dtParams.start/dtParams.length) + 1).toString(),
				search: dtParams.search.value,
				order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].data : '',
				dir: dtParams.order.length ? dtParams.order[0].dir : '',
			}
		});
		console.log('opts');
		return this.http.get<any>(environment.apiUrl+'/getMyEnrolledCourses',{params: opts});
	}
	
	
	getAssignedGroups(dtParams:any,userId:any){
		let opts=new HttpParams({
			fromObject: {
				limit: dtParams.length,
				offset: dtParams.start,
				page: ((dtParams.start/dtParams.length) + 1).toString(),
				search: dtParams.search.value,
				order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].data : '',
				dir: dtParams.order.length ? dtParams.order[0].dir : '',
				userId : userId
			}
		});
		return this.http.get<any>(environment.apiUrl+'/getAssignedGroups',{params: opts});
	}
	
	getAssignedCourses(dtParams:any,userId:any){
		let opts=new HttpParams({
			fromObject: {
				limit: dtParams.length,
				offset: dtParams.start,
				page: ((dtParams.start/dtParams.length) + 1).toString(),
				search: dtParams.search.value,
				order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].data : '',
				dir: dtParams.order.length ? dtParams.order[0].dir : '',
				userId : userId
			}
		});
		return this.http.get<any>(environment.apiUrl+'/getAssignedCourses',{params: opts});
	}

	getEnrolledCoursesFromAdmin(dtParams:any,userId:any){
		let opts=new HttpParams({
			fromObject: {
				limit: dtParams.length,
				offset: dtParams.start,
				page: ((dtParams.start/dtParams.length) + 1).toString(),
				search: dtParams.search.value,
				order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].data : '',
				dir: dtParams.order.length ? dtParams.order[0].dir : '',
				userId : userId
			}
		});
		return this.http.get<any>(environment.apiUrl+'/getEnrolledCoursesFromAdmin',{params: opts});	
	}
	
	getEnrolledCourses(dtParams:any,userId:any){
		let opts=new HttpParams({
			fromObject: {
				limit: dtParams.length,
				offset: dtParams.start,
				page: ((dtParams.start/dtParams.length) + 1).toString(),
				search: dtParams.search.value,
				order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].data : '',
				dir: dtParams.order.length ? dtParams.order[0].dir : '',
				userId : userId
			}
		});
		return this.http.get<any>(environment.apiUrl+'/getEnrolledCourses',{params: opts});
	}
	
	getAssignedLiveClasses(dtParams:any,userId:any){
		let opts=new HttpParams({
			fromObject: {
				limit: dtParams.length,
				offset: dtParams.start,
				page: ((dtParams.start/dtParams.length) + 1).toString(),
				search: dtParams.search.value,
				order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].data : '',
				dir: dtParams.order.length ? dtParams.order[0].dir : '',
				userId : userId
			}
		});
		return this.http.get<any>(environment.apiUrl+'/getAssignedLiveClasses',{params: opts});
	}
	
	
	getCourseDetails(courseId:string){
		return this.http.get<any>(environment.apiUrl+'/getCourseDetails?courseId='+courseId);
	}
	
	getResources(resourceId:string){
		return this.http.get<any>(environment.apiUrl+'/courses/getResources?id='+resourceId);
	}
	
	getCourseList(){
		return this.http.get<any>(environment.apiUrl+'/getCourseList');
	}

	getCourseYears(){
		return this.http.get<any>(environment.apiUrl+'/getCourseYears');
	}
	
	removeSelectedCourses(selectedCourse:string){
		return this.http.post<any>(environment.apiUrl+'/removeSelectedCourses', {selectedCourse:selectedCourse});
	}
	
	removeEnrolledCourse(assignedCourse:string){
		return this.http.post<any>(environment.apiUrl+'/removeEnrolledCourse', {assignedCourse:assignedCourse});
	}
	
	removeAssignedCourse(assignedCourse:string){
		return this.http.post<any>(environment.apiUrl+'/removeAssignedCourse', {assignedCourse:assignedCourse});
	}
	
	removeAssignedGroup(assignedGroup:string){
		return this.http.post<any>(environment.apiUrl+'/removeAssignedGroup', {assignedGroup:assignedGroup});
	}
	
	removeChapter(chapterId:number){
		return this.http.post<any>(environment.apiUrl+'/removeChapter', {chapterId:chapterId});
	}
	
	removeUnit(unitId:number){
		return this.http.post<any>(environment.apiUrl+'/removeUnit', {unitId:unitId});
	}
	editResource(resource:any){
		return this.http.post<any>(environment.apiUrl+'/editResource', resource);
	}
	removeResource(resourceId:number){
		return this.http.post<any>(environment.apiUrl+'/removeResource', {resourceId:resourceId});
	}
	
	changeStatus(status:any, userId:number){
		return this.http.post<any>(environment.apiUrl+'/changeStatus', {status:status, userId:userId});
	}
	deleteUser(userId:number){
		return this.http.post<any>(environment.apiUrl+'/deleteUser', {userId:userId});	/* delete the data from user list */
	}
	getUserType(){
		return this.http.get<any>(environment.apiUrl+'/getUserType'); /* get the type of users */
	}
	//~ savePersonalInstituteUser(userId:string,firstname:string,lastname:string,city:string, phone_number:string,pin_Code:string,user_type:number,country:string, time_zone:string,description:any,courses:any,specialization:any){
		//~ return this.http.post<any>(environment.apiUrl+'/savePersonalInstituteUser',{userId:userId,firstname:firstname,lastname:lastname, city:city,phone_number:phone_number,pin_Code:pin_Code,user_type:user_type, country:country,time_zone:time_zone,description:description,courses:courses,specialization:specialization });
	//~ }
	savePersonalInstituteUser(personalData:any){
		return this.http.post<any>(environment.apiUrl+'/savePersonalInstituteUser',{ personalData:personalData });
	}
	resetPassword(old_password:string,password:string){
		return this.http.post<any>(environment.apiUrl+'/resetPassword',{ password: password,old_password: old_password });
	}
	sendResetInstruction(user){
		return this.http.post<any>(environment.apiUrl+'/sendResetInstruction',{ userId: user.id });
	}
	setProfilePic(profilePic: File, userId?: number){
		let formData=new FormData();
		if(userId != undefined) formData.append('user_id', userId.toString());
		formData.append('profile_image', profilePic,profilePic.name);
		return this.http.post<any>(environment.apiUrl+'/set_profile_image', formData);	
	}
	submitCSV(files:File){
		let formData=new FormData();
		formData.append('file', files,files.name);
		return this.http.post<any>(environment.apiUrl+'/submitCSV', formData);	
	}
	saveSettings(settings){
		return this.http.post<any>(environment.apiUrl+'/saveSettings', settings);	
	}
	resetLoginSettings(type:any){
		return this.http.get<any>(environment.apiUrl+'/resetSettings/'+type);
	}
	saveLoginSettings(userId:any, bannerImage:File, leftTilte:any, rightTitle:any, leftDescription:any, rightDescription:any, copyrightTitle:any,instituteId:any){
			let formData=new FormData();
			formData.append('userId',userId);
			formData.append('bannerImage', bannerImage);
			formData.append('leftTilte', leftTilte);
			formData.append('rightTitle', rightTitle);
			formData.append('leftDescription', leftDescription);
			formData.append('rightDescription', rightDescription);
			formData.append('copyrightTitle', copyrightTitle);
			formData.append('instituteId', instituteId);
			return this.http.post<any>(environment.apiUrl+'/saveLoginSettings', formData);
		}

	saveLogoSettings(logoType,logo:File,opts){
			if(typeof opts =='undefined') opts={};
			let formData=new FormData();
			formData.append('logoType',logoType);
			formData.append('logo', logo,logo.name);
			return this.http.post<any>(environment.apiUrl+'/saveLogoSettings', formData, opts);
		}
	
	getLoginSettings(){
		return this.http.get<any>(environment.apiUrl+'/getLoginSettings');
	}
	setInstitute(fields){
		return this.http.post<any>(environment.apiUrl+'/setInstitute', fields);
	}
	
	getInstitute(){
		return this.http.get<any>(environment.apiUrl+'/getInstitute');
	}
	
	addBasicInformation(courseId:string,userId:any, selectedTeachers:string ,courseTitle:string, courseSubtitle:string, courseDetail:string, courseImage:any, instructionalLevel:string, courseLanguage:string, courseStartDate:Date, courseEndDate:Date){
		let formData=new FormData();
			formData.append('coursesBasicInfo[user_id]',userId);
			formData.append('coursesBasicInfo[assigned_teacher]',selectedTeachers);
			formData.append('coursesBasicInfo[course_title]',courseTitle);
			formData.append('coursesBasicInfo[sub_title]',courseSubtitle);
			formData.append('coursesBasicInfo[course_detail]',courseDetail);
			if(courseImage)formData.append('coursesBasicInfo[course_image]', courseImage);
			formData.append('coursesBasicInfo[instructional_level]', instructionalLevel);
			formData.append('coursesBasicInfo[course_language]', courseLanguage);
			formData.append('coursesBasicInfo[start_date]',(typeof courseStartDate=='undefined' || courseStartDate==null ? '' : courseStartDate.toString()));
			formData.append('coursesBasicInfo[end_date]', (typeof courseEndDate=='undefined' || courseEndDate==null ? '' : courseEndDate.toString()));
		return this.http.post<any>(environment.apiUrl+'/addCourse?courseId='+courseId, formData);
	}
	
	addAdvancedInformation(courseId:string,courseCompletionRule:string, coursePromoVideo:any, courseHighlights:string, courseKeywords:string){
		let formData=new FormData();
			formData.append('coursesAdvancedInfo[course_completion_rule]',courseCompletionRule);
			if(coursePromoVideo)formData.append('coursesAdvancedInfo[promotion_video]', coursePromoVideo);
			formData.append('coursesAdvancedInfo[course_highlights]',courseHighlights);
			formData.append('coursesAdvancedInfo[keywords]',courseKeywords);
		return this.http.post<any>(environment.apiUrl+'/addCourse?courseId='+courseId, formData);
	}
	
	getClasses(dtParams:any, courseId){
		let opts=new HttpParams({
		  fromObject: {
			limit: dtParams.length,
			offset: dtParams.start,
			page: ((dtParams.start/dtParams.length) + 1).toString(),
			search: dtParams.search.value,
			order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
			dir: dtParams.order.length ? dtParams.order[0].dir : ''
		  }
		});
		return this.http.get<any>(environment.apiUrl+'/courses/'+courseId+'/classes',{params: opts});
	}
	
	assignClassesToCourse(courseId,classIds){
		return this.http.post(environment.apiUrl+'/courses/assign_classes', {id: courseId, classes: classIds});
    }
	
	addReview(reviewRating:any,courseId){
		return this.http.post<any>(environment.apiUrl+'/courses/addReview', {reviewRating: reviewRating,courseId:courseId}); 
	}
	
	deleteReview(reviewId){
		return this.http.post<any>(environment.apiUrl+'/courses/deleteReview', {reviewId:reviewId}); 
	}
	
	coursePublish(courseId){
		return this.http.post<any>(environment.apiUrl+'/courses/coursePublish', {courseId:courseId}); 
	}
	
	endCourse(courseId){
		return this.http.post<any>(environment.apiUrl+'/courses/endCourse', {courseId:courseId}); 
	}
	
	courseClone(courseId){
		return this.http.post<any>(environment.apiUrl+'/courses/courseClone', {courseId:courseId}); 
	}
	
	completedResource(resourceId,markedLearn){
		return this.http.post<any>(environment.apiUrl+'/courses/completedResource', {resourceId:resourceId,markedLearn:markedLearn});
	}
	
	courseEnrolledRequest(enrollCourse,courseId){
		return this.http.post<any>(environment.apiUrl+'/courseEnrolledRequest', {enrollCourse:enrollCourse,courseId:courseId});
	}
	enquirePlanRequest(details,planId){
		return this.http.post<any>(environment.apiUrl+'/planEnrolledRequest', {user: details,planId: planId});
	}
	
	
	}
	

