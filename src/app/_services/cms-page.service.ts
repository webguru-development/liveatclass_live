import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams} from '@angular/common/http';
import { RequestOptions, Response} from '@angular/http';
import { CmsPage } from '../_models/cms_page';
import {environment} from '../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class CmsPageService {

  constructor(private http: HttpClient) { }

  savePage(page){
    if(page.id){
      return this.http.put<any>(environment.apiUrl+'/cms_pages/'+page.id, page);
    }else{
      return this.http.post<any>(environment.apiUrl+'/cms_pages', page);
    }
  }
  getPageBySlug(slug){
    return this.http.get<any>(environment.apiUrl+'/cms_pages/slug/'+slug).map((res) => {
            res.data = new CmsPage().deserialize(res.data); 
            return res;
          });
  }
  getPage(pageId){
    return this.http.get<any>(environment.apiUrl+'/cms_pages/'+pageId).map((res) => {
            res.data = new CmsPage().deserialize(res.data); 
            return res;
          });
  }
  getAllPages(dtParams:any){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : ''
      }
    });
    return this.http.get<any>(environment.apiUrl+'/cms_pages',{params: opts})
          .map((res) => {
            res.data = res.data.map((cmsPage: CmsPage) => new CmsPage().deserialize(cmsPage)); 
            return res;
          });
  }
  sortPages(oldIndex,newIndex){
    return this.http.post<any>(environment.apiUrl+'/cms_pages/reorder', {old: oldIndex,new: newIndex});
  }
  changeStatus(pageId,active){
    return this.http.put<any>(environment.apiUrl+'/cms_pages/'+pageId, {active: active});
  }
  deleteCmsPage(pageId){
    return this.http.delete<any>(environment.apiUrl+'/cms_pages/'+pageId);
  }
}
