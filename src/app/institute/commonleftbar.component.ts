import { Component, OnInit, DoCheck, Input} from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router, ActivatedRoute} from '@angular/router';

@Component({
	selector: 'dashboardLeftBar',
    templateUrl: 'commonleftbar.component.html'
})

export class DashboardLeftBarComponent implements OnInit, DoCheck {
	data:any;
	model: any = {};
    CourseID:number;
    currentUser:any;
    permissions:any;
	 @Input() sidebarOpen: boolean;
    constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) {}

	ngOnInit() {
       		this.data=JSON.parse(localStorage.getItem("language"));
       		this.currentUser= JSON.parse(localStorage.getItem("user_data"));
       		this.permissions= JSON.parse(localStorage.getItem("permissions"));
       		this.route.params.subscribe(params => {this.CourseID=params['id']});
    }
  
    ngDoCheck(){
    if(this.data!=JSON.parse(localStorage.getItem("language"))){
		this.data=JSON.parse(localStorage.getItem("language")); 
		}	
	}
	
  

}
