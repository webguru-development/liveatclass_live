import { Component, OnInit, DoCheck, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import {UserService} from '../_services/user.service';
import {FileLibraryService} from '../_services/file-library.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {User} from '../_models/user';
import {Params, Router,ActivatedRoute} from '@angular/router';
import * as $ from "jquery";
import 'jqueryui';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet,MultiDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { AppComponent } from '../app.component';
import {Calendar, Organizer} from '../../assets/js/calendarorganizer.min.js';
import * as moment from 'moment';

@Component({
    templateUrl: 'student_dashboard.component.html'
})

export class StudentDashoardComponent implements OnInit, DoCheck {
@ViewChild('sortable_container', { static: false }) sortable_container: ElementRef;
language:number;
items:any;
currentUser:any;
data:any;
logo:any;
CourseList = [];
teacherId:number;
instituteUsers = [];
permissions:any;
currentTab="student";
loadingComplete = false;
type:string;
id:number;
index:number;
confirmBox = false;
currentType:string;
courses=[];
per_page = 1;
current_page =1;
total =1;
studentId:number;
userType:string;
currentDate=moment();
organizer;
constructor(private FileLibrary: FileLibraryService, private UserService: UserService, private router: Router, private route: ActivatedRoute,private appComponent: AppComponent , public sanitizer: DomSanitizer){
  }
getWidthPercent(percent){
  return this.sanitizer.bypassSecurityTrustStyle('width: '+percent+'%');
}

ngAfterViewInit(){
  let that=this;
  this.loadWidgetConfig();
  //@ts-ignore
  $(this.sortable_container.nativeElement).sortable({
    revert: true,
    stop: function( event, ui ) {
      that.savePositions();
    }
  });
  let calendar = new Calendar("calendarContainer", "small", [ "Monday", 1 ], [ "#74cacb", "#1c7b8f", "#ffffff", "#ffffff" ]);
  this.organizer = new Organizer("organizerContainer", calendar);
  this.organizer.setOnClickListener('day-slider', function () { 
    that.currentDate=moment(that.currentDate).subtract(1,'days');
    that.loadEvents();
  }, function () {
    that.currentDate=moment(that.currentDate).add(1,'days');
    that.loadEvents();
  });
  this.organizer.setOnClickListener('days-blocks', function (date) {
    that.currentDate=moment(date);
    that.loadEvents();
  }, null);
  this.organizer.setOnClickListener('month-slider', function () {
    that.currentDate=moment(that.currentDate).subtract(1,'month');
    that.loadEvents();
  }, function () {
    that.currentDate=moment(that.currentDate).add(1,'month');
    that.loadEvents();
  });
  this.organizer.setOnClickListener('year-slider', function () {
    that.currentDate=moment(that.currentDate).subtract(1,'year');
    that.loadEvents();
  }, function () {
    that.currentDate=moment(that.currentDate).add(1,'year');
    that.loadEvents();
  });
  this.loadEvents();
}
ngOnInit() {

  this.data=JSON.parse(localStorage.getItem("language"));
  this.language = JSON.parse(localStorage.getItem("activeLanguage"));
  this.currentUser= JSON.parse(localStorage.getItem("user_data"));
  let config=localStorage.getItem('teacherdashboard-widget-config');
  if(config){
    this.widgetConfig=JSON.parse(config);
  }
  this.getCoursesStats();
  this.getCoursesandTestStats();
}

ngDoCheck(){
  if(JSON.parse(localStorage.getItem("activeLanguage")) != this.language){
   this.language=JSON.parse(localStorage.getItem("activeLanguage"));
  }
}

//Widgets Chart Groups Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: { position: 'bottom',
        display: true,
        labels: {
          boxWidth: 8,
          usePointStyle: true,
  
        },
      },
  title:{
      display: true,
      //text: 'Total Enrollment',
      fontColor:'#49B1B4',
      position:'top',
      fontSize:18,
      },
    
  };
  
//Chart Course Data

  public numberOfCourseLabels: Label[] = ["Completed", "In Progress"];
  public numberOfCourseData: SingleDataSet = [0,0];
  public numberOfCourseType: ChartType = 'pie';
  public numberOfCourseLegend = true;
  public numberOfCoursePlugins = [];
  public numberOfCourseColor= [
  {
    backgroundColor: ["#639CD3", "#F390A4"],
  },
  ];
 
public currentWidget={name: '',position: 0};
public widgetConfig={
  widgets: [
    {show: true,deleted: false},
    {show: false,deleted: true},
    {show: false,deleted: true},
    {show: true,deleted: false},
    {show: false,deleted: true},
  ],
  positions: [0,1,2,3,4]
};
refreshWidgets(){
  this.widgetConfig.positions=[0,1,2,3,4];
  this.widgetConfig.positions.forEach((position)=>{
    // ToDo: For now no need for these widgets, Remove this line after it's active
    if([1,2,4].indexOf(position) != -1) return;
    
    this.widgetConfig.widgets[position]={show: true,deleted: false};
  });
  this.saveConfig();
  this.loadWidgetConfig();
}
loadWidgetConfig(){
  this.widgetConfig.positions.forEach((position, index)=>{
    $('[data-position="'+position.toString()+'"]').insertAfter($(this.sortable_container.nativeElement).children(':nth-child('+(index+1).toString()+')'))
  });
}
saveConfig(){
  localStorage.setItem('teacherdashboard-widget-config', JSON.stringify(this.widgetConfig));
}
toggleWidget(position){
  this.widgetConfig.widgets[position].show=!this.widgetConfig.widgets[position].show;
  this.saveConfig();
}
deleteWidget(widget){
  this.widgetConfig.widgets[widget.position].deleted=true;
  this.saveConfig();
}
savePositions(){
  let positions=[];
  $('[data-position]').each(function(){
    positions.push(parseInt($(this).data('position')));
  });
  this.widgetConfig.positions=positions;
  this.saveConfig();
}
loadEvents(){
  this.UserService.getEventsForDate(moment(this.currentDate).format('YYYY-MM-DD'))
  .subscribe(
    data => { 
      this.organizer.list(data); 
    },
    error => {

    }
  );
}

getCoursesStats(){
     this.UserService.getCoursesStats()
      .subscribe(
        resp => { 
          this.numberOfCourseData = [resp.completedCourses,resp.inProgressCourses];
        },
        error => {

        }
      ); 
     
}

getCoursesandTestStats(){
     this.UserService.getCoursesandTestStats()
      .subscribe(
        resp => { 
          this.courses=resp;
        },
        error => {

        }
      ); 
     
}

}
