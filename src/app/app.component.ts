import { Component, OnInit} from '@angular/core';
import {UserService} from './_services/user.service';
import {CmsPageService} from './_services/cms-page.service';
import {User} from './_models/user';
import {Router, RoutesRecognized, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
	title = 'app';
	page = '';
	theme = undefined; 
  site_settings={fav_icon:''};
	currentUser: any;
	course={};
  pages: [];
	public isLoading=false;
	public appUrl='';
	public sidebarOpen = false;
  isDashboard=false;
	constructor(private CmsPageService: CmsPageService, private userService: UserService, private user:User, private router: Router, private route: ActivatedRoute) {
    this.router.events.subscribe(event=>{
      if(event instanceof RoutesRecognized){
      	let currentRoute = event.state.root.firstChild;
        this.isDashboard=(['/dashboard','/student_dashboard','/teacher_dashboard'].indexOf(event.url) !== -1);
        if(!this.currentUser && currentRoute.data.theme=='backend' ){
          this.theme='frontend';
        }else{
          this.theme=currentRoute.data.theme || 'backend';  
        }
        if(this.theme=='frontend-page'){
          document.getElementsByTagName('body')[0].setAttribute('style','background-color: transparent;');
        }else{
          document.getElementsByTagName('body')[0].setAttribute('style','');
        }
      }
    })
	}
	ngOnInit(){
    this.userService.siteSetting.subscribe(setting => {
      this.setSiteSettings(setting);
    });
    this.userService.currentUser.subscribe(user => {
      this.setCurrentUser(user);
    });
		this.checkValidDomain();
    this.CmsPageService.getAllPages({length: 10,start: '',search: '', order: []}).subscribe(resp=>{
      this.pages=resp.data;
    });
    this.userService.getLoginSettings()
    .subscribe(
      data => { 
        this.setSiteSettings(data['getLoginSetting']);
      },
      error => {

      }
    );
		if(JSON.parse(localStorage.getItem("user_data")) != null){
      this.currentUser= JSON.parse(localStorage.getItem("user_data"));
    }
	}
  setCurrentUser(user){
    if(user){
      if(JSON.parse(localStorage.getItem("user_data")) != null){
        this.currentUser= JSON.parse(localStorage.getItem("user_data"));
        this.currentUser.profile_image=user.profile_image;
        this.currentUser.firstname=user.firstname;
        this.currentUser.lastname=user.lastname;
        localStorage.setItem("user_data",JSON.stringify(this.currentUser));
      }
    }
  }
  setSiteSettings(settings){
    localStorage.setItem('site_settings',JSON.stringify(settings));
    this.site_settings=settings;
    if(this.site_settings.fav_icon) document.getElementById('appFavicon').setAttribute('href',this.site_settings.fav_icon);
  }
	toggleSideBar(){
		this.sidebarOpen = !this.sidebarOpen;
	}
	gotCourse(course){
		this.course=course;
	}
	checkValidDomain(){
	  this.isLoading = true;
			this.userService.checkValidDomain()
			.subscribe(
				data => {
					this.user.logo = data['logo'];
					this.isLoading = false;	
					this.appUrl=data['url'];	
				},
				error => {
					if(error.name=='ResponseError'){
						window.location.href = error.data.url;
					}
					this.isLoading=false;
				}
			); 
	}

  
}


